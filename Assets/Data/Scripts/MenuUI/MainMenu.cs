﻿//Creer par Valentin. 
//Date de création [2019-04-08].
//Modification Valentin le [2019-04-09]
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    const string MAIN_PANEL = "MainPanel";
    const string LEVEL_SELECT = "LevelSelect";    
    const string SETTINGS_PANEL = "Settings";
    const string LOAD_PANEL = "Load";

    static GameObject mainMenu, levelSelect, settings, load;
    public Panel mainMenuPanel;

    public GameObject current, last;
    private GameObject thisPanel;  
    public Panel lastPanel, currentPanel;          

    public int currentIndex, maxIndex;
    private GameObject[] mainMenuPanels = new GameObject[] { mainMenu, load, settings };    

    private void Start()
    {
        mainMenu = transform.Find("MainPanel").gameObject;
        load = transform.Find("Load").gameObject;
        levelSelect = transform.Find("LevelSelect").gameObject;      
        settings = transform.Find("Settings").gameObject;     
                
        mainMenuPanel = mainMenu.GetComponentInChildren<Panel>();      
        
        current = mainMenu;
        last = mainMenu;

        currentIndex = 0;
        maxIndex = 3;       
    }

    #region toggleMainMenuPanel Functions
    public void ToggleMainPanel()
    {
        if (mainMenuPanel.selected == false)
            AssignCurrent(mainMenu);
        mainMenu.SetActive(!mainMenu.activeInHierarchy);
    }
    public void ToggleLoadPanel()
    {
        AssignCurrent(load);
        load.SetActive(!load.activeInHierarchy);
        ToggleMainPanel();
    }
    public void ToggleLevelSelectPanel()
    {
        AssignCurrent(levelSelect);        
        levelSelect.SetActive(!levelSelect.activeInHierarchy);              
        ToggleMainPanel();
    }

    public void ToggleSettingsPanel()
    {
        AssignCurrent(settings);
        settings.SetActive(!settings.activeInHierarchy);             
        ToggleMainPanel();
    }

    public void TogglePanel(GameObject panelToToggle)
    {
        AssignCurrent(panelToToggle);        
        panelToToggle.SetActive(!panelToToggle.activeInHierarchy);             
    }       

    public void AssignCurrent(GameObject o)
    {
        if (last != current)
        {
            last = current;
            lastPanel = last.GetComponent<Panel>();
            lastPanel.selected = false;
            last.SetActive(false);
        }
        current = o;
    }

    public void BackToLast()
    {
        if (last != null && last != current)
        {
            TogglePanel(last);

            currentPanel = current.GetComponent<Panel>();
            currentPanel.UnSelectPanel();
            TogglePanel(current);
        }
    }
    #endregion

    public void StartGame()
    {
        GameManager.instance.LoadLevel(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
