﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMECerfPursuit : NMEPursuit
{
    public override void Update()
    {
        base.Update();

        if (Vector3.Distance(master.transform.position, GameManager.instance.currentAvatar.tr.position) <= master.attack1Range)
            master.ChangeState(NMEStateMachine.State.Attack1);
    }
}
