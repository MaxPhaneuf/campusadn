﻿// Création du script : David Babin
// Date : 03-04-2019 11:30
// Modification : Benjamin Chouinard , David Babin
// Date de modification : 10-04-2019 11:00
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterState
{
    public const string ANIM_PARAM_JUMP = "jump";
    public const string ANIM_PARAM_FALL = "isFalling";
    public const string ANIM_PARAM_WALK = "isWalking";
    public const string ANIM_PARAM_RUN = "isRunning";
    public const string ANIM_PARAM_DOUBLEJUMP = "doubleJump";
    public const string ANIM_PARAM_DASH = "isDashing";
    public const string ANIM_PARAM_HIT = "hit";
    public const string ANIM_PARAM_MOVESPEED = "moveSpeed";
    public const string ANIM_PARAM_ROLL = "isRolling";
    public const string ANIM_PARAM_DEATH = "death";
    public const string ANIM_PARAM_FLINCH = "flinch";
    public const string ANIM_PARAM_KNOCKBACK = "knockBack";
    public const string ANIM_PARAM_CHECKPOINT = "checkpoint";
    public const string ANIM_PARAM_CLEANSING = "isCleansing";
    public const string ANIM_PARAM_GRABBING = "isGrabbing";
    public const string ANIM_PARAM_PUSH = "isPushing";
    public const string ANIM_PARAM_PULL = "isPulling";
    public const string ANIM_PARAM_GLIDE = "isGliding";

    public const float MAX_WALK_INPUT = 0.3f;
    public Vector3 direction;
    public Character myMaster;

    public abstract void ToIdle();
    public abstract void ToJump();
    public abstract void ToLocomotion();
    public abstract void ToDoubleJump();
    public abstract void ToFalling();
    public abstract void ToRolling();
    public abstract void ToGlide();
    public abstract void ToDash();
    public abstract void ToPushPull();

    public abstract void RollInput();
    public abstract void DashInput();
    public abstract void CancelButton();
    public abstract void InteractInput();
    public abstract void JumpInput();
    public abstract void MoveInput();

    public CharacterState(Character myMaster)
    {
        this.myMaster = myMaster;
    }

}