﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindPit : Triggerable
{
    // variables pour fonctionement
    public enum Orientation { Plancher, Plafond};
    public Orientation Direction;
    private float force = 9f;
    private float forceGlide = 1.7f;
    private Transform tr;
    Vector3 currentPos;
    private bool activated; // si un objets passe devant pour le fermer
    public bool open; // si il est actif au depart ou non
    Vector3 baseMotion;
    AudioSource sound;

    // variable pour le deplacer

    Vector3 direction;
    private float distance;
    [HideInInspector]
    public GameObject idleFxGO;
    public GameObject idleFxGOH;
    public Vector3 offset;
    bool fxInit;
    void Start()
    {
        activated = true;
        tr = transform;
        //this.GetComponent<CapsuleCollider>().enabled = false;
        offset = new Vector3(0, 0.9f, 0);
        sound = GetComponentInChildren<AudioSource>();


    }


    private void Update()
    {
        if (!fxInit)
        {
            if (PoolingManager.instance)
            {
                idleFxGO = PoolingManager.instance.GetObject("FX_Prop_PuitVent_5m_01", tr.position + offset);
                idleFxGOH = PoolingManager.instance.GetObject("FX_Prop_PuitVent_10m_01", tr.position + offset);
                if (open)
                    idleFxGO.SetActive(true);
                else
                    idleFxGO.SetActive(false);
                fxInit = true;
                idleFxGOH.SetActive(false);
            }
        }
        if (GameManager.instance.currentAvatar.isRolling)
        {
            this.GetComponent<CapsuleCollider>().enabled = true;

        }
        else
        {
            this.GetComponent<CapsuleCollider>().enabled = false;
        }
        if (open)
        {
            idleFxGO.SetActive(true);
        }
        else
        {
            idleFxGO.SetActive(false);
            idleFxGOH.SetActive(false);
        }

    }

    private void LateUpdate()
    {
        idleFxGO.transform.position = tr.position;
        idleFxGOH.transform.position = tr.position;
    }

    public override void Activate()
    {
        open = true;
    }

    public override void Deactivate()
    {
        open = false;
    }

    public override void Toggle()
    {
        open = !open;
    }



    private void OnTriggerEnter(Collider other)
    {
        switch (Direction)
        {
            case Orientation.Plancher:

                currentPos = GameManager.instance.transform.position;
                break;

                    case Orientation.Plafond:
                break;
        }
        if (other.tag == "Player")
        {
            baseMotion = GameManager.instance.currentAvatar.velocity * Time.deltaTime;
            GameManager.instance.currentAvatar.velocityZ = 0;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if( open == true)
        {
            switch (Direction)
            {
                case Orientation.Plancher:

                    if (other.transform.tag == "Platform")
                    {
                        activated = false;
                        return;
                    }

                    if (other.transform.tag == "Player" && activated)
                    {
                        if (GameManager.instance.currentAvatar.isRolling == true)
                        {
                            //this.GetComponent<CapsuleCollider>().enabled = true;
                            GameManager.instance.currentAvatar.velocityY = force;
                            idleFxGOH.SetActive(true);
                            idleFxGO.SetActive(false);
                        }
                        else
                        {
                            idleFxGO.SetActive(true);
                            idleFxGOH.SetActive(false);
                        }


                        if (GameManager.instance.currentAvatar.isGliding == true)
                        {
                            this.GetComponent<CapsuleCollider>().enabled = true;
                        }

                        if(GameManager.instance.currentAvatar.isGliding)
                            GameManager.instance.currentAvatar.velocityY = forceGlide;
                        else
                            GameManager.instance.currentAvatar.velocityY = force;
                    }

                    /*if (GameManager.instance.currentAvatar.onGround == true)
                    {
                        this.GetComponent<CapsuleCollider>().enabled = false;
                    }
                    */
                    break;

                case Orientation.Plafond:

                    if (other.transform.tag == "Platform")
                    {
                        activated = false;
                        return;
                    }

                    if (other.transform.tag == "Player" && activated)
                    {

                        if (GameManager.instance.currentAvatar.isGliding == true)
                        {
                            GameManager.instance.currentAvatar.controller.Move(tr.up * force * Time.deltaTime);
                        }

                        if (GameManager.instance.currentAvatar.isGliding)
                            GameManager.instance.currentAvatar.velocityY = -forceGlide;
                        else
                            GameManager.instance.currentAvatar.velocityY = -force;

                        if (GameManager.instance.currentAvatar.isRolling == true)
                        {
                            idleFxGOH.SetActive(true);
                            idleFxGO.SetActive(false);
                        }
                        else
                        {
                            idleFxGO.SetActive(true);
                            idleFxGOH.SetActive(false);
                        }
                    }

                    break;
            }
        }

        if (!sound.isPlaying)
        {
            sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_WindPit"));
        }
    }
        
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Platform")
        {
            activated = true;
            return;

        }
        if(GameManager.instance.currentAvatar.onGround == true)
        {
            this.GetComponent<BoxCollider>().enabled = true;
           // this.GetComponent<CapsuleCollider>().enabled = false;
        }
        if(other.tag == "Player")
            GameManager.instance.currentAvatar.velocity = baseMotion;
    }

}
