﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class NMEFollow : PathFollow
{
    NMEStateMachine machine;
    float distThreshold = 3;
    public bool isFollowing = false;

    private void Start()
    {
        if (path != null)
        {
            if (startPoint > path.wayPoints.Count)
                startPoint = path.wayPoints.Count;
            if (path.wayPoints[startPoint - 1])
                Init();
        }
        machine = GetComponent<NMEStateMachine>();
    }

    private void Update()
    {
        if (isFollowing && Vector3.Distance(next.transform.position, transform.position) <= distThreshold)
        {
            GoToNext();
            machine.agent.SetDestination(next.transform.position);
        }
    }

    protected override void Init()
    {
        current = path.wayPoints[startPoint - 1];
        if (path)
            transform.position = path.wayPoints[startPoint - 1].transform.position;
        InitDirection();
    }
}
