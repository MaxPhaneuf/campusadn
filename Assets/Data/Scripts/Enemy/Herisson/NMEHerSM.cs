﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class NMEHerSM : NMEStateMachine
{
    public GameObject basicModel, ballModel;
    public UnityEvent OnBallHit = new UnityEvent();
             
    public void Awake()
    {
        Init();
    }

    private void Update()
    {
        current?.Update();
    }
        
    protected override void InitStates()
    {
        base.InitStates();
        InitState(State.Pursuit, new NMEHerRoll());
        InitState(State.Attack1, new NMEHerCannon());
    }
}
