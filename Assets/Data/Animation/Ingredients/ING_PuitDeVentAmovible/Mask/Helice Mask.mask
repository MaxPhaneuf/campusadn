%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Helice Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Helice_PuitDeVentMobile_Vegetale_01_LOD0
    m_Weight: 0
  - m_Path: PuitDeVentMobile_Vegetale_01_LOD0
    m_Weight: 0
  - m_Path: PuitDeVentMobile_Vegetale_01_LOD0/FeuilleA_PuitDeVentMobile_Vegetale_01_LOD0
    m_Weight: 0
  - m_Path: PuitDeVentMobile_Vegetale_01_LOD0/FeuilleB_PuitDeVentMobile_Vegetale_01_LOD003
    m_Weight: 0
  - m_Path: PuitDeVentMobile_Vegetale_01_LOD0/FeuilleC_PuitDeVentMobile_Vegetale_01_LOD002
    m_Weight: 0
  - m_Path: PuitDeVentMobile_Vegetale_01_LOD0/FeuilleD_PuitDeVentMobile_Vegetale_01_LOD001
    m_Weight: 0
  - m_Path: RootMotion001
    m_Weight: 0
  - m_Path: RootMotion001/root
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleARotate
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleARotate/B_FeuilleA_00
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleARotate/B_FeuilleA_00/B_FeuilleA_01
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleARotate/B_FeuilleA_00/B_FeuilleA_01/B_FeuilleA_02
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleARotate/B_FeuilleA_00/B_FeuilleA_01/B_FeuilleA_02/B_FeuilleA_03
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleARotate/B_FeuilleA_00/B_FeuilleA_01/B_FeuilleA_02/B_FeuilleA_03/B_FeuilleA_04
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleBRotate
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleBRotate/B_FeuilleB_00
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleBRotate/B_FeuilleB_00/B_FeuilleB_01
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleBRotate/B_FeuilleB_00/B_FeuilleB_01/B_FeuilleB_02
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleBRotate/B_FeuilleB_00/B_FeuilleB_01/B_FeuilleB_02/B_FeuilleB_03
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleBRotate/B_FeuilleB_00/B_FeuilleB_01/B_FeuilleB_02/B_FeuilleB_03/B_FeuilleB_04
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleCRotate
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleCRotate/B_FeuilleC_00
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleCRotate/B_FeuilleC_00/B_FeuilleC_01
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleCRotate/B_FeuilleC_00/B_FeuilleC_01/B_FeuilleC_02
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleCRotate/B_FeuilleC_00/B_FeuilleC_01/B_FeuilleC_02/B_FeuilleC_03
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleCRotate/B_FeuilleC_00/B_FeuilleC_01/B_FeuilleC_02/B_FeuilleC_03/B_FeuilleC_04
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleDRotate
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleDRotate/B_FeuilleD_00
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleDRotate/B_FeuilleD_00/B_FeuilleD_01
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleDRotate/B_FeuilleD_00/B_FeuilleD_01/B_FeuilleD_02
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleDRotate/B_FeuilleD_00/B_FeuilleD_01/B_FeuilleD_02/B_FeuilleD_03
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_FeuilleDRotate/B_FeuilleD_00/B_FeuilleD_01/B_FeuilleD_02/B_FeuilleD_03/B_FeuilleD_04
    m_Weight: 0
  - m_Path: RootMotion001/root/B_BasePlant/B_TigeHelice
    m_Weight: 1
  - m_Path: RootMotion001/root/B_BasePlant/B_TigeHelice/B_L_HeliceFeuille
    m_Weight: 1
  - m_Path: RootMotion001/root/B_BasePlant/B_TigeHelice/B_R_HeliceFeuille
    m_Weight: 1
