﻿// Création du script : David Babin
// Date : 10-04-2019 11:00
// Modification : Felix Desrosiers
// Date de modification : 10-04-2019 15:15

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using System;

public class InputManager : MonoBehaviour
{
    public static InputManager instance;
    #region Available inputs value  
    // Main
    [HideInInspector]
    public bool start { get; private set; }
    [HideInInspector]
    public bool bButton { get; private set; }
    [HideInInspector]
    public bool rightBumper { get; set; }
    [HideInInspector]
    public bool leftBumper { get; set; }
    [HideInInspector]
    public bool leftStickButton { get; set; }
    [HideInInspector]
    public bool select { get; set; }

    // Character movements
    [HideInInspector]
    public Vector3 leftStick { get; set; }
    [HideInInspector]
    public bool jumping { get; set; }
    [HideInInspector]
    public bool moving { get; set; }
    [HideInInspector]
    public bool doubleJumping { get; set; }
    [HideInInspector]
    public bool dashing { get; set; }
    [HideInInspector]
    public bool interaction { get; set; }
    [HideInInspector]
    public bool cancelInteract { get; set; }
    [HideInInspector]
    public bool rolling { get; set; }
    [HideInInspector]
    public bool aspiring { get; set; }
    [HideInInspector]
    public bool resetCam { get; set; }

    // Camera
    [HideInInspector]
    public Vector3 rightStick { get; private set; }

    // Ghost move movements
    [HideInInspector]
    public bool ghostUp { get; set; }
    [HideInInspector]
    public bool ghostDown { get; set; }
    [HideInInspector]
    public bool ghostRight { get; set; }
    [HideInInspector]
    public bool ghostLeft { get; set; }

    [HideInInspector]
    public Vector3 leftStickGhost { get; private set; }
    [HideInInspector]
    public float triggerLeftGhost { get; private set; }
    [HideInInspector]
    public float triggerRightGhost { get; private set; }
    #endregion

    #region Input manager internal workings
    // Rewired 
    private Player player;
    private int playerId = 0;
    public MainMenu mainMenu;
    bool isInMainMenu = false;
    bool isInUI = false;
    bool isGamePaused = false;
    bool cheatModeActivated = false;
    public bool inCheatMode = false;

    // If we ever need to get them directly. Leave a note here if you use those constants. If no notes are left we will remove this later on.
    [HideInInspector]
    public const string START = "start";
    [HideInInspector]
    public const string SELECT = "select";
    [HideInInspector]
    public const string BBUTTON = "bButton";
    [HideInInspector]
    public const string HORIZONTAL = "horizontal";
    [HideInInspector]
    public const string VERTICAL = "vertical";
    [HideInInspector]
    public const string CAM_YAW = "camYaw";
    [HideInInspector]
    public const string CAM_PITCH = "camPitch";
    [HideInInspector]
    public const string ACTION_BUTTON = "aButton";
    [HideInInspector]
    public const string TRIGGER_LEFT = "bumperLeft";
    [HideInInspector]
    public const string RIGHT_TRIGGER = "rightTrigger";
    [HideInInspector]
    public const string LEFT_TRIGGER = "leftTrigger";
    [HideInInspector]
    public const string XBUTTON = "xButton";
    [HideInInspector]
    public const string YBUTTON = "yButton";
    [HideInInspector]
    public const string RIGHT_BUTTON = "rightButton";
    [HideInInspector]
    public const string LEFT_BUTTON = "leftButton";
    [HideInInspector]
    public const string LEFT_STICK_BUT = "leftStickBut";
    [HideInInspector]
    public const string RIGHT_STICK_BUT = "rightStickBut";
    [HideInInspector]
    public const string D_PAD_UP = "dPadUp";
    [HideInInspector]
    public const string D_PAD_DOWN = "dPadDown";
    [HideInInspector]
    public const string D_PAD_RIGHT = "dPadRight";
    [HideInInspector]
    public const string D_PAD_LEFT = "dPadLeft";

    // Inputs pipeline
    private Vector3 leftStickRaw;
    private Vector3 rightStickRaw;
    private Vector3 leftStickGhostRaw;
    private float triggerRightRaw;
    private float triggerLeftRaw;
    private float genericDeadZone = 0.2f;
    [Range(0, 1)] public float rightStickDeadZone = 0.25f;
    [Range(0, 1)] public float triggerDeadZone = 0.1f;
    private bool deadX;
    private bool deadY;
    private bool deadZ;
    private bool deadLeft;
    private bool deadRight;
    public bool ghostMode = false;
    public bool ghostOnOff = false;
    [HideInInspector]
    public bool gameplayInputsAreDisabled;
    #endregion

    private void Awake()
    {
        SingletonSetup();
        player = ReInput.players.GetPlayer(playerId);
    }

    private void SingletonSetup()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
        DontDestroyOnLoad(gameObject);
    }
    
    private void Update()
    {
        InGameInput();          
        if (isInMainMenu)
            MainMenuInput();
        if (isInUI)
            UIMenuInput();
    }     

    void InGameInput()
    {
        start = player.GetButtonDown(START);
        bButton = player.GetButtonDown(BBUTTON);
        leftStick = GetMovementsInputs();
        moving = leftStick.normalized.magnitude != 0;
        jumping = player.GetButtonDown(ACTION_BUTTON);
        rolling = player.GetButton(RIGHT_TRIGGER);
        rightBumper = player.GetButtonDown(RIGHT_BUTTON);
        leftBumper = player.GetButtonDown(LEFT_BUTTON);
        aspiring = player.GetButton(LEFT_TRIGGER);
        resetCam = player.GetButtonDown(RIGHT_STICK_BUT);
        leftStickButton = player.GetButtonDown(LEFT_STICK_BUT);
        cheatModeActivated = player.GetButtonDown(SELECT);
        //interaction = player.GetButtonDown(YBUTTON);

        if (cheatModeActivated)
            inCheatMode = !inCheatMode;
        if(inCheatMode)
        {
                ghostUp = player.GetButtonDown(D_PAD_UP);
                ghostDown = player.GetButtonDown(D_PAD_DOWN);
                ghostRight = player.GetButtonDown(D_PAD_RIGHT);
                ghostLeft = player.GetButtonDown(D_PAD_LEFT);
        }
        if (GameManager.instance.currentAvatar != null)
            dashing = player.GetButtonDown(XBUTTON);
   
        if (GameManager.instance.currentAvatar != null && !GameManager.instance.currentAvatar.onGround )
        {
            doubleJumping = player.GetButtonDown(ACTION_BUTTON);
            dashing = player.GetButtonDown(XBUTTON);
        }

        if(GameManager.instance.currentAvatar != null )
            interaction = player.GetButtonDown(YBUTTON);
        else
            cancelInteract = player.GetButtonDown(XBUTTON);

        rightStick = GetCamInputs();
        if (ghostMode)
            leftStickGhost = GetGhostInputs();
        if (start && !isInMainMenu)
            SetPause();
        if (Input.GetKeyDown(KeyCode.R) || leftStickButton)
            GameManager.instance.DeactivateRD.Invoke();
        if (resetCam)
            PlayerCamera.instance.stateMachine.ChangeState(PlayerCamera.instance.playerCamResetState);
    }

    #region MAIN MENU FUNCTIONS

    void MainMenuInput()
    {
        if (bButton)
            BackToLastPanelMainMenu();
    }
    void BackToLastPanelMainMenu()
    {
        mainMenu.BackToLast();
    }

    #endregion

    #region UI FUNCTIONS

    void UIMenuInput()
    {
        if (bButton)
            BackToLastPanelUI();
    }
    void SetPause()
    {
        if (isGamePaused)
        {
            GameManager.instance.uiManager.Resume();
            isGamePaused = false;
        }
        else
        {
            GameManager.instance.uiManager.Paused();
            isGamePaused = true;
        }

    }

    void BackToLastPanelUI()
    {
        GameManager.instance.uiManager.BackToLast();
    }
    #endregion


    private Vector3 GetMovementsInputs()
    {
        leftStickRaw = new Vector3(player.GetAxisRaw(HORIZONTAL), 0.0f, player.GetAxisRaw(VERTICAL));
        return CheckMoveDeadZone(leftStickRaw);
    }

    private Vector3 GetCamInputs()
    {
        rightStickRaw = new Vector3(player.GetAxisRaw(CAM_YAW), player.GetAxisRaw(CAM_PITCH), 0.0f);
        return CheckCamDeadZone(rightStickRaw);
    }

    private Vector3 GetGhostInputs()
    {
        leftStickGhostRaw = new Vector3(player.GetAxisRaw(HORIZONTAL), 0.0f, player.GetAxisRaw(VERTICAL));
        triggerLeftRaw = player.GetAxisRaw(TRIGGER_LEFT);
        triggerRightRaw = player.GetAxisRaw(RIGHT_TRIGGER);

        CheckTriggersDeadZone(triggerLeftRaw, triggerRightRaw);
        return CheckMoveDeadZone(leftStickGhostRaw);
    }

    private Vector3 CheckMoveDeadZone(Vector3 leftStickRaw)
    {
        deadX = leftStickRaw.x < genericDeadZone && leftStickRaw.x > -genericDeadZone;
        deadZ = leftStickRaw.z < genericDeadZone && leftStickRaw.z > -genericDeadZone;
        if (deadX)
            leftStickRaw.x = 0.0f;
        if (deadZ)
            leftStickRaw.z = 0.0f;
        return leftStickRaw;
    }

    private Vector3 CheckCamDeadZone(Vector3 rightStickRaw)
    {
        deadX = rightStickRaw.x < rightStickDeadZone && rightStickRaw.x > -rightStickDeadZone;
        deadY = rightStickRaw.y < rightStickDeadZone && rightStickRaw.y > -rightStickDeadZone;
        if (deadX)
            rightStickRaw.x = 0.0f;
        if (deadY)
            rightStickRaw.y = 0.0f;
        return rightStickRaw;
    }

    private void CheckTriggersDeadZone(float triggerLeftRaw, float triggerRightRaw)
    {
        deadLeft = triggerLeftRaw < triggerDeadZone;
        deadRight = triggerRightRaw < triggerDeadZone;
        if (deadLeft)
            triggerLeftRaw = 0.0f;
        if (deadRight)
            triggerRightRaw = 0.0f;
        triggerLeftGhost = triggerLeftRaw;
        triggerRightGhost = triggerRightRaw;
    }

    private void OverwriteAllGameplayInputs()
    {
        leftStick = new Vector3(0, 0, 0);
        rightStick = new Vector3(0, 0, 0);
        resetCam = false;
        leftStickButton = false;

        jumping = false;
        doubleJumping = false;
        interaction = false;
        rolling = false;

        bButton = false;
        rightBumper = false;
        leftBumper = false;
        aspiring = false;
    }
}
