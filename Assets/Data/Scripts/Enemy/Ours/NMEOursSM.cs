﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NMEOursSM : NMEStateMachine
{
    [HideInInspector]
    public GameObject currentRock;
    [HideInInspector]
    public Vector3 rockTarget;

    public float rockUpwardForce = 4;
    public float timeOfPoundAnim = 3.9f;
    public GameObject localRock;
    public Transform LookAtObject;
    public Transform throwPoint;

    Vector3 localRockStartPos;

    Vector3 force;
    readonly int isThrow = Animator.StringToHash("isThrow");

    [HideInInspector]
    public bool thrown;

    public void Awake()
    {
        Init();
        localRockStartPos = localRock.transform.localPosition;
    }

    private void Update()
    {
        current?.Update();
    }

    protected override void InitStates()
    {
        base.InitStates();
        InitState(State.Pursuit, new NMEOursPursuit());
        InitState(State.Attack1, new NMEOursPound());
        InitState(State.Attack2, new NMEOursThrow());
    }

    public void ThrowRock()
    {
        localRock.GetComponent<Animator>().SetBool(isThrow, false);
        localRock.SetActive(false);
        currentRock = PoolingManager.instance.GetObject(PoolConst.OBJ_NMEvOur_Rock, throwPoint.position);
        Rigidbody rb = currentRock.GetComponent<Rigidbody>();
        force = rockTarget - transform.position;
        rb.AddForce(force + Vector3.up * rockUpwardForce, ForceMode.Impulse);
        thrown = true;
    }

    public void PickUpRock()
    {
        localRock.transform.localPosition = localRockStartPos; 
        localRock.SetActive(true);
        localRock.GetComponent<Animator>().SetBool(isThrow, true);
    }
}
