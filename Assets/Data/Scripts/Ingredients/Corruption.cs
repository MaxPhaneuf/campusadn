﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corruption : MonoBehaviour
{
    public virtual void Corrupt()
    {
        gameObject.SetActive(false);
    }
    public virtual void Uncorrupt()
    {
        gameObject.SetActive(true);
    }
}
