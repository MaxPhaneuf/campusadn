﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTrigger : Triggerable
{
    PathFollow follow;
    public override void Activate()
    {
        follow.SetActive(true);
    }

    public override void Deactivate()
    {
        follow.SetActive(false);
    }

    public override void Toggle()
    {
        follow.SetActive(!follow.IsActive());
    }

    // Start is called before the first frame update
    void Start()
    {
        follow = GetComponent<PathFollow>();
    }
}
