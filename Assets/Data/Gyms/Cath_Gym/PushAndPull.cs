﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushAndPull : MonoBehaviour
{
    public float speedMod = 10f;
    Transform tr;
    Collider coll;

    // Start is called before the first frame update
    void Start()
    {
        tr = transform;
        coll = GetComponentInChildren<Collider>();
    }

    // Update is called once per frame
    void Update()
    {


    }
    void CheckPosition()
    {

    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && InputManager.instance.interaction)
            tr.Translate(GameManager.instance.currentAvatar.tr.forward * GameManager.instance.currentAvatar.runSpeed * Time.deltaTime * speedMod);
    }
}
