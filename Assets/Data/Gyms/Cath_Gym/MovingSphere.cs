﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingSphere : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Player")
            GameManager.instance.currentAvatar.stockPosition = false;
    }
}
