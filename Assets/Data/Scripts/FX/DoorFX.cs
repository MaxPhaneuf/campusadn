﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorFX : MonoBehaviour
{
    private Transform tr;

    private void Start()
    {
        tr = GetComponentInParent<Transform>();
    }
    public void PlayFX(string name)
    {
        PoolingManager.instance.GetObjectAutoReturn(name, tr.position, tr.rotation.eulerAngles, 1f);
    }
}
