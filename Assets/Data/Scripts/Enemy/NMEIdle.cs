﻿public class NMEIdle : NMEState
{
    public override void Update()
    {
        base.Update();
        if (!IsPlayerInRange())
            master.ChangeState(NMEStateMachine.State.Patrol);
        else
            master.ChangeState(NMEStateMachine.State.Alert);

    }
}
