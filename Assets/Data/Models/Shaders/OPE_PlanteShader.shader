// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "OPE_PlanteShader"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		[Header(Translucency)]
		_Translucency("Strength", Range( 0 , 50)) = 1
		_TransNormalDistortion("Normal Distortion", Range( 0 , 1)) = 0.1
		_TransScattering("Scaterring Falloff", Range( 1 , 50)) = 2
		_TransDirect("Direct", Range( 0 , 1)) = 1
		_TransAmbient("Ambient", Range( 0 , 1)) = 0.2
		_TransShadow("Shadow", Range( 0 , 1)) = 0.9
		_Direction("Direction", Vector) = (0,0,0,0)
		_Metallic("Metallic", 2D) = "white" {}
		_Wind_Speed("Wind_Speed", Range( 0 , 1)) = 0.1315719
		_Albedo("Albedo", 2D) = "white" {}
		_Normal("Normal", 2D) = "white" {}
		_Roughness("Roughness", 2D) = "white" {}
		_Emmissive("Emmissive", 2D) = "white" {}
		_EmmissiveIntensity("EmmissiveIntensity", Range( 0 , 10)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#pragma target 3.0
		#pragma surface surf StandardCustom keepalpha addshadow fullforwardshadows exclude_path:deferred vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
		};

		struct SurfaceOutputStandardCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			half3 Translucency;
		};

		uniform float _Wind_Speed;
		uniform float3 _Direction;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _Emmissive;
		uniform float4 _Emmissive_ST;
		uniform float _EmmissiveIntensity;
		uniform sampler2D _Metallic;
		uniform float4 _Metallic_ST;
		uniform sampler2D _Roughness;
		uniform float4 _Roughness_ST;
		uniform half _Translucency;
		uniform half _TransNormalDistortion;
		uniform half _TransScattering;
		uniform half _TransDirect;
		uniform half _TransAmbient;
		uniform half _TransShadow;
		uniform float _Cutoff = 0.5;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 temp_cast_0 = (0.0).xxx;
			float3 ase_vertex3Pos = v.vertex.xyz;
			float3 Local_To_World10 = ase_vertex3Pos;
			float Wind_Speed58 = ( _Time.y * _Wind_Speed );
			float simplePerlin2D16 = snoise( ( Local_To_World10 + Wind_Speed58 ).xy );
			float3 Direction19 = ( simplePerlin2D16 * _Direction );
			float3 lerpResult34 = lerp( temp_cast_0 , Direction19 , v.color.r);
			float3 Vertex_Mask38 = lerpResult34;
			v.vertex.xyz += Vertex_Mask38;
		}

		inline half4 LightingStandardCustom(SurfaceOutputStandardCustom s, half3 viewDir, UnityGI gi )
		{
			#if !DIRECTIONAL
			float3 lightAtten = gi.light.color;
			#else
			float3 lightAtten = lerp( _LightColor0.rgb, gi.light.color, _TransShadow );
			#endif
			half3 lightDir = gi.light.dir + s.Normal * _TransNormalDistortion;
			half transVdotL = pow( saturate( dot( viewDir, -lightDir ) ), _TransScattering );
			half3 translucency = lightAtten * (transVdotL * _TransDirect + gi.indirect.diffuse * _TransAmbient) * s.Translucency;
			half4 c = half4( s.Albedo * translucency * _Translucency, 0 );

			SurfaceOutputStandard r;
			r.Albedo = s.Albedo;
			r.Normal = s.Normal;
			r.Emission = s.Emission;
			r.Metallic = s.Metallic;
			r.Smoothness = s.Smoothness;
			r.Occlusion = s.Occlusion;
			r.Alpha = s.Alpha;
			return LightingStandard (r, viewDir, gi) + c;
		}

		inline void LightingStandardCustom_GI(SurfaceOutputStandardCustom s, UnityGIInput data, inout UnityGI gi )
		{
			#if defined(UNITY_PASS_DEFERRED) && UNITY_ENABLE_REFLECTION_BUFFERS
				gi = UnityGlobalIllumination(data, s.Occlusion, s.Normal);
			#else
				UNITY_GLOSSY_ENV_FROM_SURFACE( g, s, data );
				gi = UnityGlobalIllumination( data, s.Occlusion, s.Normal, g );
			#endif
		}

		void surf( Input i , inout SurfaceOutputStandardCustom o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float4 Normal41 = tex2D( _Normal, uv_Normal );
			o.Normal = Normal41.rgb;
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode35 = tex2D( _Albedo, uv_Albedo );
			float4 Albedo39 = tex2DNode35;
			o.Albedo = Albedo39.rgb;
			float2 uv_Emmissive = i.uv_texcoord * _Emmissive_ST.xy + _Emmissive_ST.zw;
			float4 Emissive72 = ( tex2D( _Emmissive, uv_Emmissive ) * _EmmissiveIntensity );
			o.Emission = Emissive72.rgb;
			float2 uv_Metallic = i.uv_texcoord * _Metallic_ST.xy + _Metallic_ST.zw;
			float4 Metallic75 = tex2D( _Metallic, uv_Metallic );
			o.Metallic = Metallic75.r;
			float2 uv_Roughness = i.uv_texcoord * _Roughness_ST.xy + _Roughness_ST.zw;
			float4 Roughness36 = ( 1.0 - tex2D( _Roughness, uv_Roughness ) );
			o.Smoothness = Roughness36.r;
			o.Translucency = Albedo39.rgb;
			o.Alpha = 1;
			float Opacity40 = tex2DNode35.a;
			clip( Opacity40 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16301
7;1;1906;1010;3053.289;1283.109;2.054483;True;False
Node;AmplifyShaderEditor.CommentaryNode;1;-3497.354,421.9294;Float;False;856.3502;357.33;Vitesse;4;55;57;58;56;Vitesse;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;57;-3466.593,693.9788;Float;False;Property;_Wind_Speed;Wind_Speed;10;0;Create;True;0;0;False;0;0.1315719;0.2628609;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;55;-3415.517,473.1977;Float;True;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;2;-3475.581,-5.837063;Float;False;814.8336;358.9998;Local_To_World;2;10;4;Local_To_World;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;56;-3149.488,541.4271;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;4;-3422.979,71.74298;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;58;-2895.24,521.2401;Float;True;Wind_Speed;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;10;-2884.647,66.96288;Float;False;Local_To_World;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;9;-2559.056,38.89165;Float;False;1055.332;607.9457;Direction;7;19;17;16;14;13;12;54;Direction;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;12;-2509.989,223.0925;Float;False;58;Wind_Speed;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;13;-2529.89,117.1211;Float;False;10;Local_To_World;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;14;-2242.262,166.5238;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;16;-2109.261,175.1674;Float;True;Simplex2D;1;0;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;54;-2316.74,421.999;Float;False;Property;_Direction;Direction;8;0;Create;True;0;0;False;0;0,0,0;0.3,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-1871.995,327.6492;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;23;-407.2526,-620.2151;Float;False;727.2673;283.7211;Smoothness;3;36;32;27;Smoothness;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;62;356.5908,-631.3708;Float;False;886.3648;398.8815;Comment;4;72;71;70;69;Emissive;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;24;-686.056,21.26763;Float;False;918.9866;607.2115;Vertex_Mask ;5;30;38;34;26;28;Vertex_Mask ;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;19;-1720.703,321.343;Float;True;Direction;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;26;-572.6748,251.1663;Float;False;19;Direction;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.VertexColorNode;28;-490.759,351.4513;Float;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;69;422.0183,-553.2676;Float;True;Property;_Emmissive;Emmissive;14;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;77;1284.418,-648.4788;Float;False;627;284;Comment;2;74;75;Metallic;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;70;435.9655,-332.1239;Float;False;Property;_EmmissiveIntensity;EmmissiveIntensity;15;0;Create;True;0;0;False;0;0;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;31;-1771.657,-621.4001;Float;False;643.4322;280;Albedo;3;40;39;35;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;27;-357.2526,-568.276;Float;True;Property;_Roughness;Roughness;13;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;29;-1113.355,-622.8148;Float;False;682.6995;282.2639;Normal;2;41;33;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-471.4,160.2678;Float;False;Constant;_Black;Black;5;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;71;774.039,-447.0313;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;74;1334.418,-594.4788;Float;True;Property;_Metallic;Metallic;9;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;32;-65.58835,-561.903;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;35;-1721.657,-571.4001;Float;True;Property;_Albedo;Albedo;11;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;34;-175.9843,216.542;Float;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;33;-1063.355,-570.5509;Float;True;Property;_Normal;Normal;12;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;38;7.529217,225.521;Float;True;Vertex_Mask;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;72;1015.719,-437.4363;Float;False;Emissive;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;36;102.0451,-572.2151;Float;False;Roughness;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;39;-1376.225,-569.9474;Float;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;41;-673.6554,-572.8148;Float;False;Normal;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;18;-1468.736,157.1744;Float;False;738.4441;370.9536;World_To_Object;4;25;22;21;20;World_To_Object;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;75;1668.418,-598.4788;Float;False;Metallic;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;40;-1372.03,-462.7515;Float;False;Opacity;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;20;-1408.736,227.1746;Float;False;19;Direction;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldToObjectTransfNode;21;-1416.325,326.1282;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;25;-975.2904,261.7912;Float;True;World_To_Object;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-1195.874,266.1704;Float;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;76;512.8558,300.0253;Float;False;75;Metallic;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;73;509.7985,224.7603;Float;False;72;Emissive;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;45;501.7222,364.6311;Float;False;36;Roughness;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;46;513.9351,512.3456;Float;False;40;Opacity;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;48;512.437,73.87109;Float;False;39;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;51;513.2435,441.6962;Float;False;39;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;43;512.4521,150.4858;Float;False;41;Normal;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;47;506.785,587.9722;Float;False;38;Vertex_Mask;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;841.323,192.7784;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;OPE_PlanteShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;AlphaTest;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;56;0;55;0
WireConnection;56;1;57;0
WireConnection;58;0;56;0
WireConnection;10;0;4;0
WireConnection;14;0;13;0
WireConnection;14;1;12;0
WireConnection;16;0;14;0
WireConnection;17;0;16;0
WireConnection;17;1;54;0
WireConnection;19;0;17;0
WireConnection;71;0;69;0
WireConnection;71;1;70;0
WireConnection;32;0;27;0
WireConnection;34;0;30;0
WireConnection;34;1;26;0
WireConnection;34;2;28;1
WireConnection;38;0;34;0
WireConnection;72;0;71;0
WireConnection;36;0;32;0
WireConnection;39;0;35;0
WireConnection;41;0;33;0
WireConnection;75;0;74;0
WireConnection;40;0;35;4
WireConnection;25;0;22;0
WireConnection;22;0;20;0
WireConnection;22;1;21;0
WireConnection;0;0;48;0
WireConnection;0;1;43;0
WireConnection;0;2;73;0
WireConnection;0;3;76;0
WireConnection;0;4;45;0
WireConnection;0;7;51;0
WireConnection;0;10;46;0
WireConnection;0;11;47;0
ASEEND*/
//CHKSM=51538C31BA3DEEAA402890094CA3EA8C16B6CBE0