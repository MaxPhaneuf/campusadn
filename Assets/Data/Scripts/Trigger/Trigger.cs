﻿/*Script cree le : 05-04-2019
 * Derniere modification:22-04-2019
 * Par:Steven Judge
 * Description: Affecte un objet lorsqu'il est active/desactive
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Trigger : MonoBehaviour
{
    public enum TriggerActivation { OnEnter, OnExit, OnStay }
    public TriggerActivation activateur;

    public enum TriggerFunction { Activate, Deactivate, Toggle }
    public TriggerFunction function;

    public bool hasTimer;
    public float timer;
    float lastTimeToggle;
    public bool isColliderTrigger = true;
    public Triggerable triggerable;
    delegate void Function();
    Function currentFunction;
    [HideInInspector] public bool active = false;
    [HideInInspector] public float dot;// positif si le trigger est activer par derriere, et negatif pas devant----se base sur le sens du trigger
    [HideInInspector] public bool isUse = false;

    private void Start()
    {
        TriggerEvent();
        triggerable.myTriggers.Add(this);
    }

    protected void TriggerEvent()
    {
        if (function == TriggerFunction.Activate)
            currentFunction = triggerable.Activate;
        else if (function == TriggerFunction.Deactivate)
            currentFunction = triggerable.Deactivate;
        else if (function == TriggerFunction.Toggle)
            currentFunction = triggerable.Toggle;
    }

    private void Update()
    {
        if (hasTimer && active && Time.time - lastTimeToggle >= timer)
        {
            active = false;
            ToggleAnim(false);
            isUse = false;
            triggerable.Deactivate();
        }
            
    }

    void OnEnter()
    {
        if (currentFunction == triggerable.Toggle)
            isUse = false;

        if (isUse == false)
        {

            if (activateur == TriggerActivation.OnEnter)
            {
                isUse = true;
                currentFunction();
                active = !active;
                ToggleAnim(true);
                lastTimeToggle = Time.time;
            }
        }
        
    }

    void OnExit()
    {
        if (currentFunction == triggerable.Toggle)
            isUse = false;

        if (isUse == false)
        {
            if (activateur == TriggerActivation.OnExit)
            {
                isUse = true;
                currentFunction();
                active = !active;
                ToggleAnim(false);
            }
        }
        
    }

    void OnStay()
    {
        if (currentFunction == triggerable.Toggle)
            isUse = false;

        if (isUse == false)
        {
            if (activateur == TriggerActivation.OnStay)
            {
                ToggleAnim(true);
                isUse = true;
                currentFunction();
                active = !active;
            }

        }
    }

    public virtual void ToggleAnim(bool toggle)
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isColliderTrigger)
            return;
        if(other.transform.tag == GameManager.PLAYER_TAG)
            OnEnter();
    }

    private void OnTriggerExit(Collider other)
    {
        if (!isColliderTrigger || hasTimer)
            return;
        if (other.transform.tag == GameManager.PLAYER_TAG)
            OnExit();
    }

    private void OnTriggerStay(Collider other)
    {
        if (!isColliderTrigger)
            return;
        if (other.transform.tag == GameManager.PLAYER_TAG)
            OnStay();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isColliderTrigger)
            return;
        if (collision.transform.tag == GameManager.PLAYER_TAG)
            OnEnter();
    }

    private void OnCollisionExit(Collision collision)
    {
        if (isColliderTrigger || hasTimer)
            return;
        if (collision.transform.tag == GameManager.PLAYER_TAG)
            OnExit();
    }

    private void OnCollisionStay(Collision collision)
    {
        if (isColliderTrigger)
            return;
        if (collision.transform.tag == GameManager.PLAYER_TAG)
            OnStay();
    }

    public void CompareWithAvatar(Vector3 avatarDirection)
    {
        dot = Vector3.Dot(transform.forward, avatarDirection - transform.position);
    }
}
