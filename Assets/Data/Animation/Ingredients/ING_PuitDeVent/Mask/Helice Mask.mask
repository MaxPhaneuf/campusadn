%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Helice Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: B_BasePlant
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleA_00
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleA_00/B_PetaleA_01
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleA_00/B_PetaleA_01/B_PetaleA_02
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleA_00/B_PetaleA_01/B_PetaleA_02/B_PetaleA_03
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleB_00
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleB_00/B_PetaleB_01
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleB_00/B_PetaleB_01/B_PetaleB_02
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleB_00/B_PetaleB_01/B_PetaleB_02/B_PetaleB_03
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleC_00
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleC_00/B_PetaleC_01
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleC_00/B_PetaleC_01/B_PetaleC_02
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleC_00/B_PetaleC_01/B_PetaleC_02/B_PetaleC_03
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleD_00
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleD_00/B_PetaleD_01
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleD_00/B_PetaleD_01/B_PetaleD_02
    m_Weight: 0
  - m_Path: B_BasePlant/B_PetaleD_00/B_PetaleD_01/B_PetaleD_02/B_PetaleD_03
    m_Weight: 0
  - m_Path: B_BasePlant/B_TigeHelice
    m_Weight: 1
  - m_Path: ctrl_PuitDeVent
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_Helice
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleA_01
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleA_01/ctrl_PetaleA_02
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleA_01/ctrl_PetaleA_02/ctrl_PetaleA_03
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleB_01
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleB_01/ctrl_PetaleB_02
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleB_01/ctrl_PetaleB_02/ctrl_PetaleB_03
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleC_01
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleC_01/ctrl_PetaleC_02
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleC_01/ctrl_PetaleC_02/ctrl_PetaleC_03
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleD_01
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleD_01/ctrl_PetaleD_02
    m_Weight: 0
  - m_Path: ctrl_PuitDeVent/ctrl_PetaleD_01/ctrl_PetaleD_02/ctrl_PetaleD_03
    m_Weight: 0
  - m_Path: PuitDeVent_Vegetal_01_LOD0
    m_Weight: 0
  - m_Path: PuitDeVent_Vegetal_01_LOD0/Helice_PuitDeVentMobile_Vegetale_01_LOD0
    m_Weight: 0
