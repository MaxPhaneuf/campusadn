﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateDoubleJump : CharacterState
{
    public override void ToIdle(){
        myMaster.ChangeStates(Character.StateName.idle);
    }
    public override void ToLocomotion()
    {
        myMaster.ChangeStates(Character.StateName.move);
    }
    public override void ToJump() { }
    public override void ToFalling()
    {
        myMaster.ChangeStates(Character.StateName.falling);
    }
    public override void ToDoubleJump(){}
    public override void ToGlide() { }
    public override void ToDash()
    {
        myMaster.ChangeStates(Character.StateName.dash);
    }
    public override void ToPushPull(){}
    public override void ToRolling()
    {
        myMaster.ChangeStates(Character.StateName.roll);
    }

    public override void RollInput()
    {
        myMaster.isCleansing = false;
        myMaster.layerCleanse = myMaster.animator.GetLayerIndex("Cleanse");
        myMaster.animator.SetLayerWeight(myMaster.layerCleanse, 0f);
        myMaster.isRolling = true;
        myMaster.animator.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.controller.height = 1;
        myMaster.controller.center = myMaster.cutCenter;
        myMaster.collider.height = 1;
        myMaster.collider.center = myMaster.cutCenter;
        myMaster.ApplyRotation();
        ToRolling();
    }
    public override void DashInput()
    {
        if (!myMaster.lockDash && myMaster.isAbleToDash && myMaster.canDash)
        {
            myMaster.lastTimeDash = Time.frameCount;
            myMaster.animator.SetTrigger(ANIM_PARAM_DASH);
            myMaster.isAbleToDash = false;
            myMaster.isDashing = true;
        }

        if (Time.frameCount - myMaster.lastTimeDash >= ANIM_PARAM_DASH.Length + myMaster.timeDashDrop)
        {
            myMaster.isDashing = false;
            if (myMaster.onGround)
                myMaster.isAbleToDash = true;
        }
        ToDash();
    }
    public override void CancelButton(){}
    public override void InteractInput(){}
    public override void JumpInput(){}
    public override void MoveInput()
    {
        if (!myMaster.onGround)
        {
            myMaster.velocityZ = myMaster.impulsionForwardJump;
            myMaster.ApplyRotation();
        }
        else
        {
            myMaster.ApplyRotation();
            myMaster.animator.SetBool(ANIM_PARAM_RUN, true);
            ToLocomotion();
        }
    }
    public StateDoubleJump(Character myMaster) : base(myMaster) { }
}
