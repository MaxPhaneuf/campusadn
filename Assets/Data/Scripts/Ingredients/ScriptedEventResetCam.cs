﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptedEventResetCam : MonoBehaviour
{
    Transform tr;

    private void Start()
    {
        tr = transform;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Triggered");

            GameManager.instance.currentAvatar.tr.position = tr.position + tr.forward;
            GameManager.instance.currentAvatar.tr.rotation = tr.rotation;
            PlayerCamera.instance.stateMachine.ChangeState(PlayerCamera.instance.playerCamResetState);
        }
    }
}
