﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NmeStatePatrol : NmeStateMachine
{

    public override void Idle()
    {
        myMaster.anim.SetBool("move", false);
        myMaster.ChangeState(NmeController.NmeStates.IDLE);
    }
    public override void Chase()
    {
        myMaster.anim.SetBool("detecRange", true);
        myMaster.anim.SetBool("moveRange", true);
        myMaster.ChangeState(NmeController.NmeStates.CHASE);
    }
    public override void Search()
    {
        myMaster.ChangeState(NmeController.NmeStates.SEARCH);
    }
    public override void Defend()
    {
        myMaster.ChangeState(NmeController.NmeStates.DEFEND);
    }
    public override void Patrol()
    {
       
    }
    public override void Alert()
    {
        
        myMaster.ChangeState(NmeController.NmeStates.ALERT);
    }
    public override void Hurt()
    {
        myMaster.ChangeState(NmeController.NmeStates.HURT);
        myMaster.anim.SetBool("hit", true);
    }



    public override void UpdateState()
    {
        
        //Debug.Log("Patrol");
        float distance = Vector3.Distance(GameManager.instance.currentAvatar.tr.position, myMaster.transform.position);

       if (distance <= myMaster.lookRadius)
        {
            Chase();
        }
        myMaster.Patrol();
    }

    public NmeStatePatrol(NmeController myMaster) : base(myMaster) { }
}