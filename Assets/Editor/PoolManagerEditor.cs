﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PoolingManager))]
public class PoolManagerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PoolingManager poolManager = (PoolingManager)target;
        if (GUILayout.Button("Generate Const File"))
        {
            poolManager.GenerateConstFile();
        }
    }
}


