﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CamState

{

    public CameraManager myMaster;
    public abstract void GameCam();
    public abstract void FixedCam();
    public abstract void TowerCam();
    public abstract void EventCam();
    public abstract void Update();

    public CamState(CameraManager myMaster)
    {
        this.myMaster = myMaster;
    }

}
