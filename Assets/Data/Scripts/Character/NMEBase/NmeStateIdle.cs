﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NmeStateIdle : NmeStateMachine
{

    public override void Idle()
    {
        
    }
    public override void Chase()
    {

        myMaster.ChangeState(NmeController.NmeStates.CHASE);
    }
    public override void Search()
    {
        myMaster.ChangeState(NmeController.NmeStates.SEARCH);
    }
    public override void Defend()
    {
        myMaster.ChangeState(NmeController.NmeStates.DEFEND);
    }
    public override void Patrol()
    {
        myMaster.anim.SetBool("detecRange", false);
        myMaster.anim.SetBool("moveRange", false);
        myMaster.anim.SetBool("move", true);
        myMaster.ChangeState(NmeController.NmeStates.PATROL);
    }
    public override void Alert()
    {
        throw new System.NotImplementedException();
    }
    public override void Hurt()
    {
        myMaster.ChangeState(NmeController.NmeStates.HURT);
        myMaster.anim.SetBool("hit", true);
    }




    public override void UpdateState()
    {

        float distance = Vector3.Distance(GameManager.instance.currentAvatar.tr.position, myMaster.transform.position);
        Debug.Log("Idle");

        if (distance >= myMaster.lookRadius)
        {
            Patrol();
        }

    }

    public NmeStateIdle(NmeController myMaster) : base(myMaster) { }
}
