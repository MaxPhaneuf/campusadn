﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlideTest : MonoBehaviour
{
    //private CharacterController controller;

    public float baseSpeed = 1.0f;
    public float rotSpeedX = 3.0f;
    [HideInInspector]
    public bool allowGlide = false;
    [HideInInspector]
    public static GlideTest instance;
    public float gravity = -0.25f;
    private Vector3 vecGravity;


    private void Start()
    {
        //GameManager.instance.currentAvatar.controller = GetComponent<CharacterController>();
        //colision = GetComponentInChildren<Collider>();
        //Character.instanceGlide = this;
    }

    private void Update()
    {
        if (allowGlide)
        {
            Debug.Log("je vole !!!!");
            Vector3 moveVector = GameManager.instance.currentAvatar.transform.forward * baseSpeed;

            Vector3 inputs = InputManager.instance.leftStick;

            Vector3 yaw = inputs.x * GameManager.instance.currentAvatar.transform.right * rotSpeedX * Time.deltaTime;
            Vector3 pitch = Time.deltaTime * -Mathf.Abs(gravity) * Vector3.up;
            Vector3 dir = yaw + pitch;

            float maxX = GameManager.instance.currentAvatar.angle; 
            if (maxX < 90 && maxX > 70 || maxX > 270 && maxX < 290)
            { }
            else
                moveVector += dir;

            GameManager.instance.currentAvatar.controller.Move(moveVector * Time.deltaTime);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log("touche moi");
        allowGlide = true;
        //if (other.CompareTag("Player") && GameManager.instance.inputManager.dashing && GameManager.instance.currentAvatar.hitDashableIngredient)

    }


}
