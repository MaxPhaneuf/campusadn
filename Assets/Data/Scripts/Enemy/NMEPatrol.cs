﻿using UnityEngine;

public class NMEPatrol : NMEState
{
    public override void Start()
    {
        base.Start();
        master.follow.isFollowing = true;
        master.agent.enabled = true;
        master.agent.SetDestination(master.follow.next.transform.position);
    }

    public override void Update()
    {
        base.Update();
        master.SetAgentSpeedWithAnim();
        if (isPlayerInRange)
            master.ChangeState(NMEStateMachine.State.Alert);
    }

    public override void End()
    {
        base.End();
        master.follow.isFollowing = false;
        master.agent.enabled = false;
    }
}
