﻿using UnityEngine;

public class NMEStateListener : MonoBehaviour
{
    protected Animator Anim;
    protected NMEStateMachine machine;
    protected int isTargetInRange = Animator.StringToHash("isTargetInRange");
    protected int isRunningAtTarget = Animator.StringToHash("isRunningAtTarget");

    readonly int isDizzy = Animator.StringToHash("isDizzy");
    readonly int triggerHurt = Animator.StringToHash("triggerHurt");
    readonly int triggerWakeUp = Animator.StringToHash("triggerWakeUp");
    readonly int triggerCleansingComplete = Animator.StringToHash("triggerWakeUp");
    readonly int triggerCleansing = Animator.StringToHash("triggerCleansing");
    readonly int isMoving = Animator.StringToHash("isMoving");
   
    public virtual void Init(NMEStateMachine machine)
    {
        this.machine = machine;
        Anim = GetComponent<Animator>();
        InitStates();
    }

    protected virtual void InitStates()
    {
        InitIdle(Anim);
        InitHurt(Anim);
        InitPatrol(Anim);
        InitPursuit(Anim);
        
    }

    protected void InitIdle(Animator anim)
    {
        NMEIdle idle = (NMEIdle)machine.GetState(NMEStateMachine.State.Idle);
        idle.IsInRange.AddListener((isInRange) => anim.SetBool(isTargetInRange, isInRange));
    }

    protected void InitHurt(Animator anim)
    {
        NMEHurt hurt = (NMEHurt)machine.GetState(NMEStateMachine.State.Hurt);

        hurt.OnDizzy.AddListener(() => anim.SetTrigger(isDizzy));
        machine.OnHurt.AddListener(() => anim.SetTrigger(triggerHurt));
        machine.OnWakeUp.AddListener(() => anim.SetTrigger(triggerWakeUp));
    }

    protected void InitCleansing(Animator anim)
    {
        NMECleansing cleansing = (NMECleansing)machine.GetState(NMEStateMachine.State.Cleansing);
        cleansing.OnCleasingComplete.AddListener(() => anim.SetTrigger(triggerCleansingComplete));

        machine.OnCleansingStart.AddListener(() => anim.SetTrigger(triggerCleansing));
    }

    protected void InitPatrol(Animator anim)
    {
        NMEPatrol patrol = (NMEPatrol)machine.GetState(NMEStateMachine.State.Patrol);
        patrol.OnStart.AddListener(() => anim.SetBool(isMoving, true));
        patrol.OnEnd.AddListener(() => anim.SetBool(isMoving, false));
        patrol.IsInRange.AddListener((isInRange) => anim.SetBool(isTargetInRange, isInRange));
    }

    protected virtual void InitPursuit(Animator anim)
    {
        NMEPursuit pursuit = (NMEPursuit)machine.GetState(NMEStateMachine.State.Pursuit);
        pursuit.OnStart.AddListener(() => anim.SetBool(isRunningAtTarget, true));
        pursuit.OnEnd.AddListener(() => anim.SetBool(isRunningAtTarget, false));
        pursuit.IsInRange.AddListener((isInRange) => anim.SetBool(isTargetInRange, isInRange));
    }



}
