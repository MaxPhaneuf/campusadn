﻿// Création du script : Benjamin Chouinard
// Date : 02-04-2019 16:15
// Modification : /////////
// Date de modification : ////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public GameManager myManager;

    public enum StateName { gameCam, fixedCam, towerCam, eventCam };

    private CamState currentState;
    private GameCamState gameCam;
    private FixedCamState fixedCam;
    private TowerCamState towerCam;
    private EventCamState eventCam;

    private void Start()
    {
        gameCam = new GameCamState(this);
        fixedCam = new FixedCamState(this);
        towerCam = new TowerCamState(this);
        eventCam = new EventCamState(this);

        currentState = gameCam;
    }

    private void LateUpdate()
    {
        currentState.Update();
    }

    public void ChangeState(StateName newState)
    {
        switch (newState)
        {
            case StateName.gameCam:
                currentState = gameCam;
                break;
            case StateName.fixedCam:
                currentState = fixedCam;
                break;
            case StateName.towerCam:
                currentState = towerCam;
                break;
            case StateName.eventCam:
                currentState = eventCam;
                break;
        }
    }

}
