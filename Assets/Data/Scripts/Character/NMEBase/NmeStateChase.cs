﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NmeStateChase : NmeStateMachine
{
    public override void Idle()
    {
        myMaster.anim.SetBool("move", false);
        myMaster.ChangeState(NmeController.NmeStates.IDLE);
    }
    public override void Chase()
    {
        
    }
    public override void Search()
    {
        myMaster.ChangeState(NmeController.NmeStates.SEARCH);
    }
    public override void Defend()
    {
        myMaster.ChangeState(NmeController.NmeStates.DEFEND);
    }
    public override void Patrol()
    {
        myMaster.anim.SetBool("detecRange", false);
        myMaster.anim.SetBool("moveRange", false);
        myMaster.anim.SetBool("move", true);
        myMaster.ChangeState(NmeController.NmeStates.PATROL);
    }
    public override void Alert()
    {
        myMaster.ChangeState(NmeController.NmeStates.ALERT);
    }
    public override void Hurt()
    {
        myMaster.ChangeState(NmeController.NmeStates.HURT);
        myMaster.anim.SetBool("hit", true);
    }

    public override void UpdateState()
    {

        //Debug.Log("Chase");

        float distance = Vector3.Distance(GameManager.instance.currentAvatar.tr.position, myMaster.transform.position);
        if (distance >= myMaster.lookRadius)
        {
            Patrol();
        }


        if (distance < myMaster.lookRadius - 4 && myMaster.type == NmeController.Type.Cerf)
        {
            myMaster.cerf.NmeCerfBehavior();
        }

        if (distance < myMaster.lookRadius && myMaster.type == NmeController.Type.Ours)
        {
            myMaster.ours.NmeOursBehavior();
        }

        myMaster.Chase();

    }

    public NmeStateChase(NmeController myMaster) : base(myMaster) { }
}
