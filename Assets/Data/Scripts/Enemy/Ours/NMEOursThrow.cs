﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NMEOursThrow : NMEState
{
    NMEOursSM sm;
    Vector3 offSet;
    Quaternion lookAtRot;
    float lookAtThreshold = .5f;

    public override void Start()
    {
        sm = (NMEOursSM)master;
        sm.thrown = false;
        offSet = new Vector3(GameManager.instance.currentAvatar.tr.position.x, master.transform.position.y, GameManager.instance.currentAvatar.tr.position.z);
        sm.LookAtObject.LookAt(offSet);
        lookAtRot = Quaternion.LookRotation(sm.LookAtObject.forward);
        sm.rockTarget = GameManager.instance.currentAvatar.tr.position;
        base.Start();
    }

    public override void Update()
    {
        base.Update();
        if (sm.currentRock && !sm.currentRock.activeInHierarchy && sm.thrown)
            master.ChangeState(NMEStateMachine.State.Alert);
        else
            master.transform.rotation = Quaternion.RotateTowards(master.transform.rotation, lookAtRot, 5);
    }

}
