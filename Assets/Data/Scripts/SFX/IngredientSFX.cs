﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class IngredientSFX : MonoBehaviour
{
    AudioSource sound;


    void Start()
    {
        sound = GetComponentInChildren<AudioSource>();
    }

    public void PlaySFX(string Name)
    {
        sound.PlayOneShot(GameManager.instance.SoundManager.GetClip(Name));
    }


    public void StopSFX()
    {
        sound.Stop();
    }
}
