﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HedgehogTeleport : MonoBehaviour
{
    public HedgehogTeleport entranceBuddy;
    [HideInInspector]
    public Transform tr;
    [HideInInspector]
    public Collider collider;
    public float transitTime = 1f;

    private void Start()
    {
        tr = transform;
        collider = GetComponentInChildren<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && GameManager.instance.currentAvatar.isRolling)
            Teleport();
    }

    void Teleport()
    {
        if (!GameManager.instance.currentAvatar.isTeleporting)
        {
            Tunnel(transitTime);
            GameManager.instance.currentAvatar.isTeleporting = true;
            GameManager.instance.currentAvatar.gameObject.SetActive(false);
            GameManager.instance.currentAvatar.tr.position = entranceBuddy.tr.position + entranceBuddy.tr.forward;
            GameManager.instance.currentAvatar.tr.rotation = entranceBuddy.tr.rotation;
            GameManager.instance.currentAvatar.gameObject.SetActive(true);
            if(GameManager.instance.currentAvatar.isRolling)
                GameManager.instance.currentAvatar.currentState.RollInput();
            PlayerCamera.instance.stateMachine.ChangeState(PlayerCamera.instance.playerCamResetState); 
            Invoke("ResetTeleport", 1);
        }
    }
    void ResetTeleport()
    {
        GameManager.instance.currentAvatar.isTeleporting = false;
    }

    public IEnumerator Tunnel(float waitTime)
    {
        Color c = new Color(1,1,1,1);
        Color n = new Color(1, 1, 1, 0);
        StartCoroutine(GameManager.instance.FadeEffect(c,waitTime, GameManager.instance.fadePanel));
        while (GameManager.instance.busy)
            yield return new WaitForEndOfFrame();
        yield return new WaitForSecondsRealtime(waitTime);
        StartCoroutine(GameManager.instance.FadeEffect(n, waitTime, GameManager.instance.fadePanel));
    }
}

