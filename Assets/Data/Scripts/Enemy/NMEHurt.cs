﻿using UnityEngine.Events;

public class NMEHurt : NMEState
{
    float timeToDizzy = 1;
    bool isDizzy;
    public UnityEvent OnDizzy = new UnityEvent();

    public override void Start()
    {
        base.Start();
        master.isHurt = true;
    }

    public override void Update()
    {
        base.Update();
        if (timeInState >= timeToDizzy && !isDizzy)
        {
            OnDizzy.Invoke();
            isDizzy = true;
        }
   
        if (timeInState >= NMEStateMachine.TIME_HURT)
        {
            master.OnWakeUp.Invoke();
            master.ChangeState(NMEStateMachine.State.Idle);
        }
    }

    public override void End()
    {
        base.End();
        master.isHurt = false;

    }
}
