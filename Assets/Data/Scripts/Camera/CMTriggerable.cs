﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;

public class CMTriggerable : Triggerable
{
    Camera cmCamera;
    public CameraTriggerManager.ActiveCam SwitchTo, Default;  

    private void Start()
    {        
        cmCamera = GetComponent<Camera>();        
    }

    public override void Activate()
    {        
        cmCamera.enabled = true; 
        PlayerCamera.instance.GetComponent<Camera>().enabled = false;
        GameManager.instance.GetComponent<CameraTriggerManager>().ChangeCurrent(SwitchTo);
        //CameraEvent.Invoke();
        Debug.Log("Cinemachine Cam view");
    }

    public override void Deactivate()
    {
        PlayerCamera.instance.GetComponent<Camera>().enabled = true;
        cmCamera.enabled = false;
        GameManager.instance.GetComponent<CameraTriggerManager>().ChangeCurrent(Default);
        Debug.Log("player Cam view");
    }

    public override void Toggle()
    {
        
    }
}
