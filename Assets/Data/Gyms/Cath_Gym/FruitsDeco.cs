﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitsDeco : MonoBehaviour
{
    public Transform tr;
    public Rigidbody rb;
    public float mass = 0f;
    public bool isDropped = false;
    private Animator animator;
    public bool takenByAvatar;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
        tr = transform;
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        //rb.mass = mass;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if(GameManager.instance.currentAvatar.health < 3)
                GameManager.instance.currentAvatar.health++;

            gameObject.SetActive(false);
        } 
    }


}
