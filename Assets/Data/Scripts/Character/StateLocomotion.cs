﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateLocomotion : CharacterState
{
    public override void ToIdle()
    {
        myMaster.animatorRoll.SetBool(ANIM_PARAM_RUN, false);
        myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
        myMaster.ChangeStates(Character.StateName.idle);
    }
    public override void ToLocomotion(){}
    public override void ToJump()
    {
        myMaster.ChangeStates(Character.StateName.jump);
    }
    public override void ToFalling()
    {
        myMaster.animator.SetBool(ANIM_PARAM_RUN, true);
        myMaster.ChangeStates(Character.StateName.falling);
    }
    public override void ToDoubleJump()
    {
        myMaster.ChangeStates(Character.StateName.doubleJump);
    }
    public override void ToGlide()
    {
        myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_RUN, false);
        myMaster.isGliding = true;
        myMaster.animator.SetBool("isGliding", true);
        myMaster.startGlideFXGO = PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_PollenStart_01, myMaster.tr.position, 1);
        myMaster.ChangeStates(Character.StateName.glide);
    }
    public override void ToDash()
    {
        myMaster.ChangeStates(Character.StateName.dash);
    }
    public override void ToPushPull(){}
    public override void ToRolling()
    {
        myMaster.ChangeStates(Character.StateName.roll);
    }

    public override void RollInput()
    {
        myMaster.isCleansing = false;
        myMaster.layerCleanse = myMaster.animator.GetLayerIndex("Cleanse");
        myMaster.animator.SetLayerWeight(myMaster.layerCleanse, 0f);
        myMaster.isRolling = true;
        myMaster.animator.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.controller.height = 1;
        myMaster.controller.center = myMaster.cutCenter;
        myMaster.collider.height = 1;
        myMaster.collider.center = myMaster.cutCenter;
        myMaster.ApplyRotation();
        ToRolling();
    }
    public override void DashInput()
    {
        if (!myMaster.lockDash && !myMaster.isDashing && myMaster.canDash)
        {
            myMaster.lastTimeDash = Time.frameCount;
            myMaster.animator.SetTrigger(ANIM_PARAM_DASH);
            myMaster.isDashing = true;
            ToDash();
        }
    }
    public override void CancelButton(){}
    public override void InteractInput(){}
    public override void JumpInput()
    {
        if (myMaster.onGround)
        {
            if (InputManager.instance.leftStick != Vector3.zero)
            {
                myMaster.isJumping = true;
                myMaster.onGround = false;
                myMaster.animator.SetTrigger(ANIM_PARAM_JUMP);
                myMaster.velocityY = myMaster.impulsionJump;
                myMaster.velocityZ = myMaster.impulsionForwardJump;
                ToJump();
            }
            else
            {
                myMaster.isJumping = true;
                myMaster.onGround = false;
                myMaster.animator.SetTrigger(ANIM_PARAM_JUMP);
                myMaster.velocityY = myMaster.impulsionJump;
                myMaster.velocityZ = 0;
                ToJump();
            }
        }
        else
        {
            myMaster.animator.SetTrigger(ANIM_PARAM_DOUBLEJUMP);
            myMaster.velocityY = 0;
            myMaster.velocityY = myMaster.impulsionDoubleJump;
            if (InputManager.instance.moving)
                myMaster.velocityZ = myMaster.impulsionForwardJump / 1.3f;
            myMaster.canDoubleJump = false;
            ToDoubleJump();
        }
    }
    public override void MoveInput()
    {
        if (InputManager.instance.leftStick != Vector3.zero)
        {
            myMaster.animator.SetBool(ANIM_PARAM_RUN, true);

            if (myMaster.onGround && !myMaster.isJumping)
            {
                //Reset gravite
                myMaster.currentSpeed = 0f;
                myMaster.velocityY = 0f;
                myMaster.velocityZ = 0f;
                myMaster.drag = 0f;

                if (InputManager.instance.leftStick != Vector3.zero && myMaster.isAbleToDash)
                {
                    myMaster.stockPosition = true;
                    // Get direction en fonction des inputs et de la cam
                    myMaster.targetRotation = Mathf.Atan2(InputManager.instance.leftStick.x, InputManager.instance.leftStick.z) * Mathf.Rad2Deg + myMaster.currentCameraTr.eulerAngles.y;
                    myMaster.newAngle = Mathf.Atan2(InputManager.instance.leftStick.x, InputManager.instance.leftStick.z) * Mathf.Rad2Deg;

                    myMaster.angle = Mathf.DeltaAngle(myMaster.oldAngle, myMaster.newAngle);
                    myMaster.oldAngle = myMaster.newAngle;

                    myMaster.angleFor180 = myMaster.GetNormal(myMaster.currentCameraTr.forward, myMaster.tr.forward, myMaster.tr.up);

                    // Compare la nouvelle direction avec la vieille et applique la bonne anim
                    myMaster.animator.SetFloat(myMaster.angleAnimator, myMaster.angle);

                    //Corrige la rotation de l'avatar en fonction de la cam
                    if (myMaster.tr.position == myMaster.oldPosition)
                        myMaster.tr.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(myMaster.targetRotation, myMaster.targetRotation, ref myMaster.turnSmoothVelocity, myMaster.mouvementSmoothTime);
                    else
                        myMaster.tr.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(myMaster.tr.eulerAngles.y, myMaster.targetRotation, ref myMaster.turnSmoothVelocity, myMaster.turnSmoothTime);
                    myMaster.ApplyRotation();
                }
                else
                {
                    if (myMaster.stockPosition)
                        myMaster.oldPosition = myMaster.tr.position;
                }
                myMaster.ApplyRotation();
            }
        }
        else 
        {
            if (myMaster.hasStoppedMoving)
            {
                myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
                ToIdle();
            }
        }

        if (InputManager.instance.rolling)
            RollInput();
    }
    public StateLocomotion(Character myMaster) : base(myMaster) { }
}
