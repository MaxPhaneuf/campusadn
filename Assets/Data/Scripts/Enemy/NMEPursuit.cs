﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMEPursuit : NMEState
{
    public override void Start()
    {
        base.Start();
        master.agent.enabled = true;
        master.agent.SetDestination(GameManager.instance.currentAvatar.tr.position);
    }

    public override void Update()
    {
        base.Update();
        master.SetAgentSpeedWithAnim();
        if (!isPlayerInRange)
            master.ChangeState(NMEStateMachine.State.Alert);
        else
            master.agent.SetDestination(GameManager.instance.currentAvatar.tr.position);
    }

    public override void End()
    {
        base.End();
        master.agent.enabled = false;
    }
}
