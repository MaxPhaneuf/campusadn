﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatMenu : MonoBehaviour
{
    #region Variable
    [Header("Drag the GameManager Here")]
    public GameManager manager;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 turnDirection = Vector3.zero;
    private Vector3 direction;
    private float speed = 1.0f;
    private modulesLD rendererLD;
    private bool rendering = false;
    #endregion
    public static CheatMenu instance;
    public int levelToLoad; 

    private void Awake()
    {
        SingletonSetup();
    }

    void Update()
    {
        //cheats
        if (InputManager.instance.inCheatMode)
        {
            if (InputManager.instance.ghostUp)
            {
                InputManager.instance.ghostOnOff = !InputManager.instance.ghostOnOff;
            }

            if (InputManager.instance.ghostOnOff)
            {
                GameManager.instance.currentAvatar.DisableInput(true);
                GameManager.instance.currentAvatar.onGround = false;
                GameManager.instance.currentAvatar.controller.enabled = false;
                GameManager.instance.currentAvatar.indestructible = true;
                moveDirection = new Vector3(Input.GetAxis("Horizontal") + GameManager.instance.currentAvatar.currentCameraTr.eulerAngles.y , InputManager.instance.rightStick.y, Input.GetAxis("Vertical"));
                turnDirection = new Vector3(0, InputManager.instance.rightStick.x, 0);
                GameManager.instance.currentAvatar.tr.Rotate(turnDirection * 1.3f);
                GameManager.instance.currentAvatar.tr.Translate(moveDirection * speed);
            }
            if (!InputManager.instance.ghostOnOff)
            {
                GameManager.instance.currentAvatar.DisableInput(false);
                GameManager.instance.currentAvatar.controller.enabled = true;
                GameManager.instance.currentAvatar.indestructible = false;
            }

            if (InputManager.instance.ghostRight)
            {
                levelToLoad = (levelToLoad < 10 ? levelToLoad + 1 : 1);
                GameManager.instance.LoadLevel(levelToLoad);
            }

            if (InputManager.instance.ghostLeft)
            {
                levelToLoad = (levelToLoad > 1 ? levelToLoad - 1 : 10);
                GameManager.instance.LoadLevel(levelToLoad);
            }


        }

        if(!InputManager.instance.inCheatMode && InputManager.instance.ghostOnOff)
        {
            GameManager.instance.currentAvatar.DisableInput(true);
            GameManager.instance.currentAvatar.onGround = false;
            GameManager.instance.currentAvatar.controller.enabled = false;
            GameManager.instance.currentAvatar.indestructible = true;
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), InputManager.instance.rightStick.y, Input.GetAxis("Vertical"));
            turnDirection = new Vector3(0, InputManager.instance.rightStick.x, 0);
            GameManager.instance.currentAvatar.tr.Rotate(turnDirection * 1.3f);
            GameManager.instance.currentAvatar.tr.Translate(moveDirection * speed);
        }
        //rendering
        if (InputManager.instance.ghostDown)
        {
            rendering = !rendering;

            if (rendering)
            {
                GameManager.instance.DeactivateRD.Invoke();
            }
        }
    }

    private void OnGUI()
    {
        if (InputManager.instance.inCheatMode)
        {
            GUI.Label(new Rect(40, 40, 100, 40), "DPad up : Cheats");
            GUI.Label(new Rect(40, 100, 100, 40), "DPad down : Renderer");
            GUI.Label(new Rect(100,70, 100, 40), "DPad right :\n Next lvl");
            GUI.Label(new Rect(10, 70, 100, 40), "DPad left :\n Last lvl");
            if (InputManager.instance.ghostOnOff)
            {
                GUI.Label(new Rect(10, 10, 100, 30), "ON");
            }
            else
            {
                GUI.Label(new Rect(10, 10, 100, 30), "OFF");
            }
            if(rendering)
                GUI.Label(new Rect(40, 10, 100, 30), "Rendering");
        }
        if(InputManager.instance.ghostOnOff)
        {
            GUI.Label(new Rect(10, 10, 100, 30), "ON");
        }
        if (rendering)
            GUI.Label(new Rect(40, 10, 100, 30), "Rendering");
    }

    #region Singleton
    private void SingletonSetup()
    {
        if (instance == null) instance = this;
        else Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    #endregion
}
