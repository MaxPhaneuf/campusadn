﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class NMECerfSM : NMEStateMachine
{
    [HideInInspector]
    public UnityEvent OnDashNoHit = new UnityEvent();
    public UnityEvent OnDashHit = new UnityEvent();
    public void Start() { Init(); }

    private void Update() { current?.Update(); }

    protected override void InitStates()
    {
        base.InitStates();
        InitState(State.Pursuit, new NMECerfPursuit());
        InitState(State.Attack1, new NMECerfDash());
    }

    public void DashNoHit()
    {
        OnDashNoHit.Invoke();
        ChangeState(State.Alert);
    }
        
}

