﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.Rendering.PostProcessing;

public class PlayerCamera : MonoBehaviour
{
    public static PlayerCamera instance;
    [HideInInspector] public Transform tr;
    [HideInInspector] public Camera myself;
    public PlayerCameraTarget playerCamTarget;
    public PlayerCameraTarget camTarget;
    public LayerMask noPlayerMask;

    [HideInInspector] public bool stateMachineIsLocked;
    [HideInInspector] public StateMachine stateMachine;
    [HideInInspector] public PlayerCamDefault playerCamDefaultState;
    [HideInInspector] public PlayerCamCollision playerCamCollisionState;
    [HideInInspector] public PlayerCamReset playerCamResetState;

    [Range(40.0f, 115.0f)]
    public float camFOV = 60.0f;
    [HideInInspector] public float yaw, pitch;
    private float camYawSensitivity = 10.0f, camPitchSensitivity = 70.0f;
    private Vector2 pitchMinMax = new Vector2(-12, 60);

    private Vector3 currentRotation, desiredRotation;
    private Vector3 rotationSV;
    private float a1, b1, a2, b2;

    [Header("Collision management")]
    private Vector3 dirToCamera;
    private Collider lastCollFound;

    [Header("Camera dolly parameters")]
    [HideInInspector] public float desiredDollyDst;
    [HideInInspector] public float camDollyMinDist = 2.4f;
    [HideInInspector] public float camDollyMaxDist = 4.5f;

    [Header("Camera yOffset parameters")]
    private float desiredYOffset;
    private float targetMinYOffset = 0.0f;
    private float targetMaxYOffset = 0.10f;

    [HideInInspector] public RaycastHit hit;
    [HideInInspector] public bool isColliding;
    [HideInInspector] public bool camAxisInUse;

    [HideInInspector] public int timerTest;

    private bool getAvatarPos = true;

    private void SingletonSetup()
    {
        if (instance == null) instance = this;
        else Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }
    public void Awake()
    {
        SingletonSetup();
        tr = transform;
        myself = GetComponent<Camera>();
        myself.fieldOfView = camFOV;
        stateMachine = GetComponent<StateMachine>();
        playerCamDefaultState = new PlayerCamDefault(this);
        playerCamCollisionState = new PlayerCamCollision(this);
        playerCamResetState = new PlayerCamReset(this);

        SetCamBehaviours();
        noPlayerMask = LayerMask.GetMask("Obstacles");
        if (!camTarget)
        {
            camTarget = Instantiate(playerCamTarget);
            DontDestroyOnLoad(camTarget);
        }
        camTarget.playerCamera = this;
        stateMachine.ChangeState(playerCamDefaultState);
    }

    private void LateUpdate()
    {
        if (GameManager.instance.currentAvatar.enabled && getAvatarPos)
        {
            tr.eulerAngles = GameManager.instance.currentAvatar.transform.rotation.eulerAngles;
            getAvatarPos = false;
            camTarget.tr.position = GameManager.instance.currentAvatar.transform.position;
        }
        if (camTarget && !getAvatarPos)
        {
            stateMachine.CurrentStateUpdate();
            timerTest++;
        }
    }

    public void UpdateState()
    {
        if (InputManager.instance)
        {
            CameraGetInputs();
            CameraVerticalDolly();
            CameraOrientation();
            CameraHorizontalDolly();
            IsCameraBeingUsed();
            IsCameraColliding();
        }
    }

    private void CameraGetInputs()
    {
        /// Setup yaw. Will use movement axis if camera not being used, else only use camera axis.
        if (InputManager.instance.rightStick.x > 0)
        {
            yaw += Mathf.Pow(InputManager.instance.rightStick.x * camYawSensitivity, 2.1f) * Time.deltaTime;
        }
        if (InputManager.instance.rightStick.x < 0)
        {
            yaw += -(Mathf.Pow(Mathf.Abs(InputManager.instance.rightStick.x) * camYawSensitivity, 2.1f)) * Time.deltaTime;
        }
        /// Left stick that influence camera, check condition
        if (InputManager.instance.rightStick.x == 0 && InputManager.instance.leftStick.x > 0.25)
        {
            yaw += Mathf.Pow(InputManager.instance.leftStick.x * 2.5f, 4.6f) * Time.deltaTime;
        }
        if (InputManager.instance.rightStick.x == 0 && InputManager.instance.leftStick.x < -0.25)
        { // TOBRAIN. Why is -n exp 2.1 is error? But not -n exp 2.0????
            yaw += -(Mathf.Pow(Mathf.Abs(InputManager.instance.leftStick.x) * 2.5f, 4.6f)) * Time.deltaTime;
        }
        /// Clamp the yaw to [0, 360]. Anti-gimbal-lock. Works pretty well!!!
        if (yaw > 360.0f)
        {
            yaw -= 360.0f;
            currentRotation = new Vector3(currentRotation.x, currentRotation.y - 360, currentRotation.z);
        }
        if (yaw < 0.0f)
        {
            yaw += 360.0f;
            currentRotation = new Vector3(currentRotation.x, currentRotation.y + 360, currentRotation.z);
        }
        /// Setup pitch and clamp it
        pitch -= InputManager.instance.rightStick.y * camPitchSensitivity * Time.deltaTime;
        pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);
    }

    /// <summary> Dolly the camera upward or downward depending on the pitch. Linear function start dollying below 0 </summary>
    private void CameraVerticalDolly()
    {
        desiredYOffset = a2 * pitch + b2;
        camTarget.verticalDolly = Mathf.Clamp(desiredYOffset, targetMinYOffset, targetMaxYOffset);
    }

    ///<summary> Set up the camera orientation </summary>
    private void CameraOrientation()
    {
        desiredRotation = new Vector3(pitch, yaw, 0.0f);
        currentRotation = Vector3.SmoothDamp(currentRotation, desiredRotation, ref rotationSV, 0.10f);
        tr.eulerAngles = currentRotation;
    }

    /// <summary> Dolly the camera forward or backward depending on the pitch. Linear function start dollying below 0 </summary>
    private void CameraHorizontalDolly()
    {
        desiredDollyDst = a1 * pitch + b1;
        desiredDollyDst = Mathf.Clamp(desiredDollyDst, camDollyMinDist, camDollyMaxDist);
    }

    /// <summary> Verify if the player is actively manipulating the camera with the right stick axis </summary>
    private void IsCameraBeingUsed()
    {
        if (InputManager.instance.rightStick.x == 0 && InputManager.instance.rightStick.y == 0) camAxisInUse = false;
        else camAxisInUse = true;
    }

    /// <summary> </summary>
    private void IsCameraColliding()
    {
        dirToCamera = tr.position - camTarget.tr.position;
        Debug.DrawRay(camTarget.transform.position, dirToCamera, Color.white);

        if (Physics.Raycast(camTarget.tr.position, dirToCamera, out hit, desiredDollyDst, noPlayerMask))
        {
            float product = Vector3.Dot(hit.normal, Vector3.up);
            if (product <= 0.3 && product >= -0.3)
            {
                isColliding = true;
            }
            else isColliding = false;
        }
        else isColliding = false;
    }

    /// <summary> Determin how the forward/backward dolly and vertical offset will move the camera, depending on the parameters </summary>
    private void SetCamBehaviours()
    {
        // Forward / backward dolly setup
        a1 = (camDollyMinDist - camDollyMaxDist) / pitchMinMax.x;
        b1 = camDollyMaxDist;
        // Vertical offset setup
        a2 = (targetMaxYOffset - targetMinYOffset) / pitchMinMax.x;
        b2 = targetMinYOffset;
    }
}