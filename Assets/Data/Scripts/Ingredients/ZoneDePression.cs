﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneDePression : Trigger
{
    Animator anim;

    public override void ToggleAnim(bool toggle)
    {
        anim.SetBool("isPressing", toggle);
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        TriggerEvent();
        triggerable.myTriggers.Add(this);
    }

}
