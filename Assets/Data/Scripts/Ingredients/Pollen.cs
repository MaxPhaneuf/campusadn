﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pollen : MonoBehaviour
{
    Transform tr;
    private Vector3 offset;
    private Vector3 right;
    private Vector3 forward;
    public GameObject startFxGO;
    public GameObject idleFxGO;
    public GameObject endFxGO;
    // Start is called before the first frame update
    void Start()
    {
        offset.Set(0f, 2f, 0f);
        tr = transform;
        right = new Vector3 (.4f, 0, 0);
        forward = new Vector3(0,0,.4f);
        startFxGO = PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_PollenStart_01, tr.position, 1);
        idleFxGO = PoolingManager.instance.GetObject(PoolConst.FX_Env_PollenCycle_01, tr.position);
        endFxGO = PoolingManager.instance.GetObject(PoolConst.FX_Env_Pollen_Desintegrate_01, tr.position);
        startFxGO.SetActive(true);
        startFxGO.transform.position = tr.position;
        idleFxGO.SetActive(true);
        endFxGO.SetActive(false);
        startFxGO.SetActive(false);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        tr.position = GameManager.instance.currentAvatar.transform.position + offset + right*GameManager.instance.currentAvatar.transform.forward.x + forward * GameManager.instance.currentAvatar.transform.forward.z;
        if (!GameManager.instance.currentAvatar.isGliding)
        {
            endFxGO.transform.position = tr.position;
            idleFxGO.SetActive(false);
            endFxGO.SetActive(true);
           // this.gameObject.SetActive(false);
            Invoke("Disappear", 2f);
        }
        idleFxGO.transform.position = tr.position;
    }

    void Disappear()
    {
        endFxGO.SetActive(false);
       // Destroy(this.gameObject);
    }
}
