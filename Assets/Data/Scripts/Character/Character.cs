﻿// Création du script : David Babin
// Date : 02-04-2019 16:25
// Modification : Catherine
// Date de modification : 7 mai 

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

[RequireComponent(typeof(Animator)), RequireComponent(typeof(CharacterController))]
public class Character : MonoBehaviour
{
    #region VARIABLE DESIGNER
    [Header("Varaible de gravity")]
    public float gravityUpward = 0f;
    public float gravityAtMaxHeight = 0.25f;

    [Header("Variable de mouvement")]
    //modifie le mouvement du character
    public float runSpeed = 2f;
    public float rollSpeed = 8.63f;
    [Range(0.5f,3)]
    public float airControlMod;
    public float impulsionJump = 17.5f;
    public float impulsionDoubleJump = 35f;
    public float varSmoothMod = 0.5f;
    public float timeDashDrop = 20f;

    public bool lockDoubleJump = false;
    public bool lockDash = false;
    public bool lockRoll = false;
    #endregion

    #region VARIABLE SCRIPT    
    [HideInInspector]
    public CharacterController controller;
    [HideInInspector]
    public Animator animator;
    public Animator animatorRoll;
    [HideInInspector]
    public Transform tr { get; private set; }
    [HideInInspector]
    public CapsuleCollider collider;
    private GameManager instance;
    [HideInInspector]
    public Rigidbody rb;
    [HideInInspector]
    public bool disableInput { get; private set; }
    public bool disableInteraction { get;  set; }
    [HideInInspector]
    public bool indestructible = false;
    public CharacterState currentState;
    [HideInInspector]
    public float angle;
    public int angleAnimator;
    public float oldAngle;
    public float newAngle;
    public Vector3 angleFor180;
    [HideInInspector]
    public bool canDoubleJump;

    public enum StateName
    {
        idle, move, jump, doubleJump, falling, dash, roll, glide, pushPull
    }
    private StateIdle characterIdle;
    private StateJump characterJump;
    private StateLocomotion characterLocomotion;
    private StateDoubleJump characterDoubleJump;
    private StateFalling characterFalling;
    private StateDash characterDash;
    private StateRoll characterRoll;
    private StateGlide characterGlide;
    private StatePushPull characterPushPull;

    [HideInInspector]
    public Transform currentCameraTr;

    public PlayerCamera currentCamera;
    [HideInInspector]
    public static Pissenlit instanceGlide;
    [HideInInspector]
    public static SmallMovableRock instanceRock;

    public float velocityX;
    public float velocityY;
    public float velocityZ;
    public float drag = 5f;
    public float impulsionForwardJump;
    [HideInInspector]
    public Vector3 moveDirection;
    public Vector3 velocity;
    private Vector2 avatarLook;
    private Vector3 rayBlot;

    [HideInInspector]
    public RaycastHit hit;
    [HideInInspector]
    public RaycastHit hitForward;
    [HideInInspector]
    public RaycastHit hitGround;
    [HideInInspector]
    public RaycastHit positionBlot;
    private PathFollow currentPlatform;
    public int layerCleanse;
    private float lastTimeCleanse;
    private float lastTimeMove;
    private float delayMoveSpeed = 0.1f;
    public bool hasStoppedMoving = true;
    private float delayCleanseAnim = 100f;
    public float lastTimeDash;
    public Vector3 oldPosition;
    [HideInInspector]
    public bool stockPosition;
    //public GameObject movingSphere;
    //[HideInInspector]
    public int health;
    public Vector3 startingPosition;
    public bool hasRespawn;

    public float rayMaxDist = 0.3f;
    public float rayLanding = 0.5f;
    public float rayDashing = 3f;
    public bool onGround;
    public bool isLanding;
    public bool isJumping = false;
    public bool isDashing;
    public bool isCleansing;
    public bool isGliding = false;
    public bool hasSpawnPollen = false;
    public bool isWindPitting = false;
    public bool isAbleToDash;
    public bool willDash;
    public bool canDash;
    public bool isGrabbing = false;
    public bool hitDashableIngredient = false;
    public bool isRolling = false;
    public bool inTunnel = false;
    public bool isTeleporting = false;
    public float vitesse = 30f;
    public Vector3 startingCenter;// = new Vector3(0f, 1.05f, 0f);
    public Vector3 cutCenter;// = new Vector3(0f, 0.5f, 0f);

    //Variables mouvements avatar mid-air
    public float currentSpeed = 0;
    public float mouvementSmoothTime = 0.5f;

    //Variables rotation avatar mid-air
    public float turnSmoothVelocity;
    public float turnSmoothTime = 0.3f;
    public float targetRotation;
    private float oldRotation = 0;

    Vector3 front;
    Vector3 back;
    Vector3 right;
    Vector3 left;
    private RaycastHit hitFront;
    private RaycastHit hitBackward;
    private RaycastHit hitRight;
    private RaycastHit hitLeft;
    Vector3 pushVector;
    private bool pushedFromLedge = false;

    public GameObject runningParticle;
    private ParticleSystem particleRun;
    private SphereCollider cleansingSphere;
    public GameObject rendAvatar;
    public GameObject rendBoule;

    NMEStateMachine nme;
    CorruptionFlower flower;
    Checkpoint checkpoint;


    //-------------------      Pour FX     --------------------------
    GameObject arrowFx, dblJumpfx;
    public Transform arrowPoint, dblJumpPoint, feetBone;
    //---------------------------------------------------------------
    public GameObject startGlideFXGO;
    public GameObject idleGlideFxGO;

    // Pour le son
    AudioSource sound;

    #endregion

    void Start()
    {
        cutCenter = new Vector3(0f, 0.5f, 0f);
        //initialisation des variables
        tr = transform;
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        angleAnimator = Animator.StringToHash("angle");
        currentCameraTr = Camera.main.transform;
        cleansingSphere = GetComponentInChildren<SphereCollider>();
        collider = GetComponent<CapsuleCollider>();
        rendAvatar = GameObject.Find("avatar");
        rendBoule = GameObject.Find("avatar_RollingBall");
        animatorRoll = rendBoule.GetComponent<Animator>();
        rendBoule.SetActive(false);
        sound = GetComponentInChildren<AudioSource>();

        //initialisation des states
        characterIdle = new StateIdle(this);
        characterJump = new StateJump(this);
        characterLocomotion = new StateLocomotion(this);
        characterDoubleJump = new StateDoubleJump(this);
        characterFalling = new StateFalling(this);
        characterDash = new StateDash(this);
        characterRoll = new StateRoll(this);
        characterPushPull = new StatePushPull(this);
        characterGlide = new StateGlide(this);
        currentState = characterIdle;
        disableInput = false;
        disableInteraction = false;
        stockPosition = true;
        startingCenter =  controller.center;
        startingPosition = tr.position;
        hasRespawn = false;

        front = new Vector3(0, 0.5f, controller.radius);
        back = new Vector3(0, 0.5f, -controller.radius);
        right = new Vector3(controller.radius, 0.5f, 0);
        left = new Vector3(-controller.radius, 0.5f, 0);

        runningParticle = GetComponent<GameObject>();

        //idleGlideFxGO = PoolingManager.instance.GetObject(PoolConst.FX_Env_PollenCycle_01, tr.position);
        //idleGlideFxGO.SetActive(false);
        //rayCenterOfAvatar = new Vector3(animator.rootPosition.x, animator.rootPosition.y, animator.rootPosition.z);
    }

    private void OnEnable()
    {
        health = 3;
        indestructible = true;
        Invoke("NotIndestructible", 2f);
        if (hasRespawn)
        {
            Invoke("ResetHasRespawn", 1f);
        }
        if (isTeleporting)
            animator.SetBool(CharacterState.ANIM_PARAM_ROLL, true);
    }
    void Update()
    {
        if (Time.frameCount - lastTimeDash >= CharacterState.ANIM_PARAM_DASH.Length + timeDashDrop)
        {
            isDashing = false;
        }

        isLanding = Physics.Raycast(tr.position, Vector3.down, out hit,rayLanding);

        Physics.Raycast(tr.position + front, new Vector3(0f, -1f, 0f), out hitFront, rayMaxDist*2);
        Physics.Raycast(tr.position + back, new Vector3(0f, -1f, 0f), out hitBackward, rayMaxDist*2);
        Physics.Raycast(tr.position + right, new Vector3(0f, -1f, 0f), out hitRight, rayMaxDist*2);
        Physics.Raycast(tr.position + left, new Vector3(0f, -1f, 0f), out hitLeft, rayMaxDist*2);
        if (!disableInput)
            moveDirection = new Vector3(InputManager.instance.leftStick.x * currentSpeed, 0f, InputManager.instance.leftStick.z * currentSpeed);
        //Raycast du blot
        Physics.Raycast(tr.position, -Vector3.up, out positionBlot);

        if (!disableInput)
        {
            Physics.Raycast(tr.position, Vector3.down, out hitGround, rayMaxDist);
            willDash = Physics.Raycast(tr.position, tr.forward, out hitForward,rayDashing);

            //falling
            animator.SetBool(CharacterState.ANIM_PARAM_FALL, !onGround && !isGliding);
            animatorRoll.SetBool(CharacterState.ANIM_PARAM_FALL, !onGround && !isGliding);

            if (hitGround.collider != null)
            {
                if ((hitGround.collider.tag == "Ground" || hitGround.collider.tag == "Ronces"))
                {
                    onGround = false;
                    if (!onGround)
                    {
                        onGround = true;

                        if (hitGround.collider.tag == "Ground")
                        {
                            controller.Move(hitGround.point - this.transform.position);

                            onGround = true;
                            canDash = true;
                        }
                    }
                }
                else
                {
                    onGround = false;
                }
            }
            else
            {
                if(!isGliding)
                 currentState.ToFalling();
            }
            //INPUTS HERE
            if (!disableInput)
            {
                if (InputManager.instance.jumping)
                    currentState.JumpInput();

                if (InputManager.instance.leftStick != Vector3.zero)
                {
                    hasStoppedMoving = false;
                    currentState.MoveInput();
                    if (!isGliding)
                        animator.SetFloat(CharacterState.ANIM_PARAM_MOVESPEED, InputManager.instance.leftStick.magnitude);
                }
                else
                    currentState.MoveInput();

                if (onGround && InputManager.instance.leftStick == Vector3.zero && !isGrabbing)
                {
                    hasStoppedMoving = true;
                    if(hasStoppedMoving)
                        Invoke("ResetMoveSpeed", delayMoveSpeed);
                }

                if (InputManager.instance.bButton)
                {
                    currentState.CancelButton();
                }

                if (InputManager.instance.dashing && !lockDash)
                {
                    cleansingSphere.enabled = false;
                    animator.SetBool(CharacterState.ANIM_PARAM_CLEANSING, false);
                    layerCleanse = animator.GetLayerIndex("Cleanse");
                    animator.SetLayerWeight(layerCleanse, 0f);
                    isCleansing = false;
                    currentState.DashInput();
                    canDash = false;
                }

                if (InputManager.instance.interaction)
                {
                    currentState.InteractInput();
                }

                if (!isDashing && InputManager.instance.rolling && !lockRoll)
                {
                    rendAvatar.SetActive(false);
                    rendBoule.SetActive(true);
                    isGliding = false;
                    currentState.RollInput();
                    rendBoule.transform.position = tr.position;
                }
                if (!InputManager.instance.rolling)
                {
                    rendAvatar.SetActive(true);
                    rendBoule.SetActive(false);
                }
            }

            if (isGliding)
            {
                currentState.ToGlide();
                if (!hasSpawnPollen)
                {
                    Invoke("GlideFXidle", 1f);
                    hasSpawnPollen = !hasSpawnPollen;
                }
            }

            if (!isGliding && !onGround && !isDashing)
                ApplyGravity();

            if (isGliding && !onGround && !isDashing)
                ApplyGlide();

            //RAYCAST check debug
            if (!onGround && !isDashing && !isRolling && !isJumping)
            {
                if (hitFront.collider != null || hitBackward.collider != null || hitRight.collider != null || hitLeft.collider != null)
                {
                    if (!pushedFromLedge)
                    {
                        if (hitFront.collider != null)
                            pushVector = tr.position - hitFront.point;
                        if (hitBackward.collider != null)
                            pushVector = tr.position - hitBackward.point;
                        if (hitRight.collider != null)
                            pushVector = tr.position - hitRight.point;
                        if (hitLeft.collider != null)
                            pushVector = tr.position - hitLeft.point;

                        controller.Move(pushVector.normalized * 10 * Time.deltaTime);
                        pushedFromLedge = true;
                    }
                }
                pushedFromLedge = false;
            }
            if(onGround)
            {
                isJumping = false;
            }

            if (hitGround.collider == null)
                onGround = false;

            ForceAngle();
            //Stock la rotation a la fin du frame
            if (angle >= 90 || angle <= -90)
                oldRotation = targetRotation;
            
            if (onGround)
                canDoubleJump = true;
            //aspiration
            if (!disableInput && !isRolling && InputManager.instance.aspiring && !isDashing)
            {
                isCleansing = true;
                lastTimeCleanse = Time.frameCount;
                animator.SetBool(CharacterState.ANIM_PARAM_CLEANSING, true);
                layerCleanse = animator.GetLayerIndex("Cleanse");
                animator.SetLayerWeight(layerCleanse, 1f);
                cleansingSphere.enabled = true;
            }
            else
            {
                cleansingSphere.enabled = false;
                animator.SetBool(CharacterState.ANIM_PARAM_CLEANSING, false);
                if (Time.frameCount - lastTimeCleanse >= delayCleanseAnim)
                {
                    layerCleanse = animator.GetLayerIndex("Cleanse");
                    animator.SetLayerWeight(layerCleanse, 0f);
                    isCleansing = false;
                }
            }
            //
            if (!animator.applyRootMotion)
                animator.applyRootMotion = true;
        }

        if (health < 1)
        {
            animator.SetTrigger(CharacterState.ANIM_PARAM_DEATH);
            Invoke("DeathEvents", 1.35f);
        }
    }

    private void LateUpdate()
    {
        currentPlatform = CheckPlatform(rayMaxDist);
        if (currentPlatform != null)
        {
            controller.Move(currentPlatform.GetTargetDirection());
        }
       // resetCam = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.GetComponent<NMEStateMachine>() != null)
        {
            nme = other.GetComponent<NMEStateMachine>();
            if (other)
                nme.RemoveCorruption(1);
        }
        if (other.GetComponent<CorruptionFlower>() != null)
        {
            flower = other.GetComponent<CorruptionFlower>();
            if(other)
            {
                flower.RemoveCorruption();
            }
        }
        if(other.GetComponent<Checkpoint>() != null)
        {
            checkpoint = other.GetComponent<Checkpoint>();
            if(other)
            {
                checkpoint.ActivateCheckpoint();
            }
        }
    }

    private PathFollow CheckPlatform(float rayDistance)
    {
        RaycastHit hit;
        if (Physics.Raycast(tr.position, Vector3.down, out hit, rayDistance))
        {
            PathFollow platform = hit.transform.GetComponentInParent<PathFollow>();
            if (platform != null)
                return platform;
        }
        return null;
    }

    public void ChangeStates(StateName newState)
    {
        switch (newState)
        {
            case StateName.idle:
                currentState = characterIdle;
                break;
            case StateName.jump:
                currentState = characterJump;
                break;
            case StateName.move:
                currentState = characterLocomotion;
                break;
            case StateName.doubleJump:
                currentState = characterDoubleJump;
                break;
            case StateName.falling:
                currentState = characterFalling;
                break;
            case StateName.dash:
                currentState = characterDash;
                break;
            case StateName.roll:
                currentState = characterRoll;
                break;
            case StateName.glide:
                currentState = characterGlide;
                break;
            case StateName.pushPull:
                currentState = characterPushPull;
                break;
        }
    }

    public bool DisableInput(bool disable)
    {
        if(disable)
            disableInput = true;
        else
            disableInput = false;
        return disableInput;
    }

    public void ApplyGravity()
    {
        //Gravite
            velocityY += -Mathf.Abs(gravityUpward);

            //Drag
            velocityZ += -Mathf.Abs(drag);
            if (velocityZ < 0f)
                velocityZ = 0f;

            //Air control
            Vector3 input = InputManager.instance.leftStick.normalized;

            //Rotation selon les mouvements
            if (!disableInput && input != Vector3.zero && !isDashing && isAbleToDash)
            {
                targetRotation = Mathf.Atan2(input.x, input.z) * Mathf.Rad2Deg + currentCameraTr.eulerAngles.y;
                tr.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(tr.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime * airControlMod);
            }

            velocity = tr.forward * velocityZ + Vector3.up * velocityY;

            //Implementation du mouvement
            controller.Move(velocity * Time.deltaTime);
        
    }

    public void ApplyGlide()
    {
        //Gravite
        velocityY += -Mathf.Abs(0.02f);

        //Drag
        velocityZ += -Mathf.Abs(drag);
        if (velocityZ < 0f)
            velocityZ = 0f;

        //Air control
        Vector3 input = InputManager.instance.leftStick.normalized;

        //Rotation selon les mouvements
        if (input != Vector3.zero && !isDashing && isAbleToDash)
        {
            targetRotation = Mathf.Atan2(input.x, input.z) * Mathf.Rad2Deg + currentCameraTr.eulerAngles.y;
            tr.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(tr.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime * 0.5f);
        }

        velocity = tr.forward * 5 + Vector3.up * velocityY;
        //Implementation du mouvement
        controller.Move(velocity * Time.deltaTime);
    }

    public void ApplyRotation()
    {
        // Get direction en fonction des inputs et de la cam
        targetRotation = Mathf.Atan2(InputManager.instance.leftStick.x, InputManager.instance.leftStick.z) * Mathf.Rad2Deg + currentCameraTr.eulerAngles.y;
        newAngle = Mathf.Atan2(InputManager.instance.leftStick.x, InputManager.instance.leftStick.z) * Mathf.Rad2Deg;

        angle = Mathf.DeltaAngle(oldAngle, newAngle);
        oldAngle = newAngle;

        angleFor180 = GetNormal(currentCameraTr.forward, tr.forward, tr.up);

        // Compare la nouvelle direction avec la vieille et applique la bonne anim
        animator.SetFloat(angleAnimator, angle);

        //Corrige la rotation de l'avatar en fonction de la cam
        tr.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, mouvementSmoothTime);
        //Air control
        Vector3 input = InputManager.instance.leftStick.normalized;

        //Rotation selon les mouvements
        if (input != Vector3.zero)
        {
            targetRotation = Mathf.Atan2(input.x, input.z) * Mathf.Rad2Deg + currentCameraTr.eulerAngles.y;
            tr.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(tr.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime * airControlMod);
        }
    }

    private void DontStockPosition()
    {
        stockPosition = false;
    }

    public Vector3 GetNormal(Vector3 a,Vector3 b,Vector3 c)
    {
        Vector3 side1 = b - a;
        Vector3 side2 = c - a;
        return Vector3.Cross(side1, side2);
    }

    void ForceAngle()
    {
        if (angleFor180.y > 0)
        {
            if (angle == 180)
                angle = 179;
        }
        else
        {
            if (angle == 180)
                angle = -179;
        }
    }
    public void GettingHurt()
    {
        if (!indestructible)
        {
            health--;
            animator.SetTrigger(CharacterState.ANIM_PARAM_FLINCH);
            indestructible = true;
            Invoke("NotIndestructible", 2f);
        }
    }
    public void GettingKnocked()
    {
        if (!indestructible)
        {
            health--;
            animator.SetTrigger(CharacterState.ANIM_PARAM_KNOCKBACK);
            indestructible = true;
            Invoke("NotIndestructible", 2f);
        }
    }
    private void NotIndestructible()
    {
        indestructible = false;

    }
    void DeathEvents()
    {
        if (!hasRespawn)
        {
            hasRespawn = true;
            onGround = false;
        }
    }
    void ResetHasRespawn()
    {
        hasRespawn = false;
    }
     public void ResetMoveSpeed()
    {
        if (hasStoppedMoving && !isGliding && !isGrabbing)
        {
            animatorRoll.SetBool(CharacterState.ANIM_PARAM_RUN, false);
            animator.SetBool(CharacterState.ANIM_PARAM_RUN, false);
            animator.SetFloat(CharacterState.ANIM_PARAM_MOVESPEED, 0f);
            currentState.ToIdle();
        }
    }

    void PlayArrowFX(string name)
    {
        GameObject o = PoolingManager.instance.GetObjectAutoReturn(name, arrowPoint.position, arrowPoint.rotation.eulerAngles, 1);
        o.transform.SetParent(arrowPoint);
    }

    void PlayDashFx(string name)
    {
        float offY = .5f;
        Vector3 offSet = new Vector3(tr.position.x, tr.position.y + offY, tr.position.z);
        PoolingManager.instance.GetObjectAutoReturn(name, offSet, tr.rotation.eulerAngles, 1);
    }
    
    void PlayDblJumpFx(string name)
    {
        float offY = .5f;
        Vector3 offSet = new Vector3(feetBone.position.x, feetBone.position.y + offY, feetBone.position.z);
        GameObject o = PoolingManager.instance.GetObjectAutoReturn(name, offSet, 1);
        o.transform.SetParent(feetBone);
    }

    public void PlayFX(string name)
    {
        switch (name)
        {
            case "FX_Bo_RunDust_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);
                break;

            case "FX_Bo_RunDust_02":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);
                break;

            case "FX_Bo_ArrowGlow_01":
                PlayArrowFX(name);

                break;
            case "FX_Bo_ArrowGlowCheckpoint_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_Dash_01":
                PlayDashFx(name);

                break;
            case "FX_Bo_DashEnd_01":
                PlayDashFx(name);

                break;
            case "FX_Bo_DoubleJump_01":
                PlayDblJumpFx(name);

                break;
            case "FX_Bo_Glide_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_Jump_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_JumpStop":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);
                

                break;
            case "FX_BO_KnockbackRoll_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_OneLife_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_Pull_Cycle_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_Pull_End_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_Pull_Start_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_Roll_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_Roll_Intro_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_Roll_Outro_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);
                

                break;
            case "FX_Bo_RunTrail_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_TrailForest_01":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
            case "FX_Bo_TurnDust":
                PoolingManager.instance.GetObjectAutoReturn(name, tr.position, 1);

                break;
        }
       
    }
    public void GlideFXidle()
    {
        idleGlideFxGO = PoolingManager.instance.GetObject(PoolConst.FX_Env_PollenCycle_01,tr.position);
    }
}
