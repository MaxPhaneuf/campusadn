﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateRoll : CharacterState
{
    public override void ToIdle()
    {
        if (!InputManager.instance.rolling)
        {
            myMaster.animator.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.controller.height = 2;
            myMaster.controller.center = myMaster.startingCenter;
            myMaster.collider.height = 2;
            myMaster.collider.center = myMaster.startingCenter;
            myMaster.isRolling = false;
            myMaster.ChangeStates(Character.StateName.idle);
        }
    }
    public override void ToLocomotion()
    {
        myMaster.animator.SetBool(ANIM_PARAM_ROLL, false);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, false);
        myMaster.controller.height = 2;
        myMaster.controller.center = myMaster.startingCenter;
        myMaster.collider.height = 2;
        myMaster.collider.center = myMaster.startingCenter;
        myMaster.isRolling = false;
        myMaster.ChangeStates(Character.StateName.move);
    }
    public override void ToJump()
    {
        if (!InputManager.instance.rolling)
        {
            myMaster.animator.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.controller.height = 2;
            myMaster.controller.center = myMaster.startingCenter;
            myMaster.collider.height = 2;
            myMaster.collider.center = myMaster.startingCenter;
            myMaster.isRolling = false;
            myMaster.ChangeStates(Character.StateName.jump);
        }
    }
    public override void ToFalling()
    {
        if (!InputManager.instance.rolling)
        {
            myMaster.animator.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.controller.height = 2;
            myMaster.controller.center = myMaster.startingCenter;
            myMaster.collider.height = 2;
            myMaster.collider.center = myMaster.startingCenter;
            myMaster.isRolling = false;
            myMaster.ChangeStates(Character.StateName.falling);
        }
    }
    public override void ToDoubleJump()
    {
        if (!InputManager.instance.rolling)
        {
            myMaster.animator.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.controller.height = 2;
            myMaster.controller.center = myMaster.startingCenter;
            myMaster.collider.height = 2;
            myMaster.collider.center = myMaster.startingCenter;
            myMaster.isRolling = false;
            myMaster.velocityY = myMaster.impulsionDoubleJump;
            if (InputManager.instance.moving)
                myMaster.velocityZ = myMaster.impulsionForwardJump / 1.3f;
            myMaster.ChangeStates(Character.StateName.doubleJump);
        }
    }
    public override void ToGlide(){}
    public override void ToDash()
    {
        myMaster.ChangeStates(Character.StateName.dash);
    }
    public override void ToPushPull(){}
    public override void ToRolling()
    {
        if (!myMaster.isDashing)
        {
            myMaster.isCleansing = false;
            myMaster.layerCleanse = myMaster.animator.GetLayerIndex("Cleanse");
            myMaster.animator.SetLayerWeight(myMaster.layerCleanse, 0f);
            myMaster.isRolling = true;
            myMaster.animator.SetBool(ANIM_PARAM_ROLL, true);
            myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, true);

        }
    }

    public override void RollInput()
    {
        myMaster.isRolling = true;
        myMaster.animator.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.controller.height = 1;
        myMaster.controller.center = myMaster.cutCenter;
        myMaster.collider.height = 1;
        myMaster.collider.center = myMaster.cutCenter;
    }
    public override void DashInput()
    {
        if (!myMaster.lockDash && !myMaster.isDashing && myMaster.canDash)
        {
            myMaster.isRolling = false;
            myMaster.animator.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, false);
            myMaster.controller.height = 2;
            myMaster.controller.center = myMaster.startingCenter;
            myMaster.collider.height = 2;
            myMaster.collider.center = myMaster.startingCenter;
            myMaster.rendAvatar.SetActive(true);
            myMaster.rendBoule.SetActive(false);
            myMaster.lastTimeDash = Time.frameCount;
            myMaster.animator.SetTrigger(ANIM_PARAM_DASH);
            myMaster.isDashing = true;
            ToDash();
        }
    }
    public override void CancelButton(){}
    public override void InteractInput(){}
    public override void JumpInput()
    {
        if (myMaster.onGround)
        {
            if (InputManager.instance.leftStick != Vector3.zero)
            {
                myMaster.isJumping = true;
                myMaster.onGround = false;
                myMaster.velocityY = myMaster.impulsionJump;
                myMaster.velocityZ = myMaster.impulsionForwardJump;
                ToJump();
            }
            if (InputManager.instance.leftStick == Vector3.zero)
            {
                myMaster.isJumping = true;
                myMaster.onGround = false;
                myMaster.velocityZ = 0;
                myMaster.velocityY = myMaster.impulsionJump;
            }
            myMaster.animatorRoll.SetTrigger(ANIM_PARAM_JUMP);
            ToJump();
        }
        else
        {
            if (myMaster.canDoubleJump && !myMaster.lockDoubleJump)
            {
                myMaster.velocityY = myMaster.impulsionDoubleJump;
                if (InputManager.instance.moving)
                    myMaster.velocityZ = myMaster.impulsionForwardJump / 1.3f;
                myMaster.canDoubleJump = false;
            }
            myMaster.animatorRoll.SetTrigger(ANIM_PARAM_JUMP);
            ToDoubleJump();
        }
    }
    public override void MoveInput()
    {
        if (InputManager.instance.rolling && InputManager.instance.leftStick != Vector3.zero)
        {
            myMaster.animatorRoll.SetBool(ANIM_PARAM_RUN, true);
            //Reset gravite
            myMaster.currentSpeed = 0f;
            myMaster.drag = 0f;

            if (InputManager.instance.leftStick != Vector3.zero && myMaster.isAbleToDash)
            {
                myMaster.stockPosition = true;
                // Get direction en fonction des inputs et de la cam
                myMaster.targetRotation = Mathf.Atan2(InputManager.instance.leftStick.x, InputManager.instance.leftStick.z) * Mathf.Rad2Deg + myMaster.currentCameraTr.eulerAngles.y;
                myMaster.newAngle = Mathf.Atan2(InputManager.instance.leftStick.x, InputManager.instance.leftStick.z) * Mathf.Rad2Deg;

                myMaster.angle = Mathf.DeltaAngle(myMaster.oldAngle, myMaster.newAngle);
                myMaster.oldAngle = myMaster.newAngle;

                myMaster.angleFor180 = myMaster.GetNormal(myMaster.currentCameraTr.forward, myMaster.tr.forward, myMaster.tr.up);

                // Compare la nouvelle direction avec la vieille et applique la bonne anim
                myMaster.animator.SetFloat(myMaster.angleAnimator, myMaster.angle);

                //Corrige la rotation de l'avatar en fonction de la cam
                if (myMaster.tr.position == myMaster.oldPosition)
                    myMaster.tr.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(myMaster.targetRotation, myMaster.targetRotation, ref myMaster.turnSmoothVelocity, myMaster.mouvementSmoothTime);
                else
                    myMaster.tr.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(myMaster.tr.eulerAngles.y, myMaster.targetRotation, ref myMaster.turnSmoothVelocity, myMaster.turnSmoothTime);
            }
            myMaster.ApplyRotation();
            myMaster.controller.Move(myMaster.tr.forward.normalized * myMaster.rollSpeed * Time.deltaTime);
        }
        else
        {
            if (!InputManager.instance.rolling && InputManager.instance.leftStick != Vector3.zero)
            {
                myMaster.ApplyRotation();
                myMaster.animator.SetBool(ANIM_PARAM_RUN, true);
                myMaster.animatorRoll.SetBool(ANIM_PARAM_RUN, false);
                myMaster.controller.height = 2;
                myMaster.controller.center = myMaster.startingCenter;
                myMaster.collider.height = 2;
                myMaster.collider.center = myMaster.startingCenter;
                ToLocomotion();
            }
        }
    }
    public StateRoll(Character myMaster) : base(myMaster) { }
}
