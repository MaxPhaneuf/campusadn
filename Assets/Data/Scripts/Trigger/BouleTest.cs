﻿/*Script cree le : 05-04-2019
 * Par: Steven Judge
 * Derniere modification:22-04-2019
 * Par:Steven Judge
 * Description: Changement de couleur a laide d'un trigger
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouleTest : Triggerable
{
    public Color currentColor;
    public Color changeTo;
    private Renderer rend;

    void Start()
    {
        rend = this.GetComponent<Renderer>();
    }

    public override void Activate()
    {
        if (state != 1)
            rend.material.color = changeTo;
        state = 1;
    }

    public override void Deactivate()
    {
        if (state != 2)
            rend.material.color = currentColor;
        state = 2;
    }

    public override void Toggle()
    {
        startActivated = !startActivated;
        if (startActivated) Activate();
        else Deactivate();
    }

}