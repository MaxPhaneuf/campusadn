﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMEHerCannon : NMEState
{
    NMEHerSM sm;
    float cannonSpeed = 10f;
    float acceleration = 100;
    float lastAcceleration;
    float cannonTime = 5f;
    public override void Start()
    {
        base.Start();
        sm = (NMEHerSM)master;
        lastAcceleration = master.agent.acceleration;
        master.agent.acceleration = 100;
        master.agent.speed = cannonSpeed;
        master.agent.enabled = true;
        master.agent.SetDestination(GameManager.instance.currentAvatar.tr.position);

    }

    public override void Update()
    {
        base.Update();
        if (Vector3.Distance(master.transform.position, GameManager.instance.currentAvatar.tr.position) <= 2)
        {
            sm.OnBallHit.Invoke();
            master.ChangeState(NMEStateMachine.State.Pursuit);
        }
        else if (timeInState >= cannonTime)
        {
            sm.OnBallHit.Invoke();
            master.ChangeState(NMEStateMachine.State.Alert);
        }

    }

    public override void End()
    {
        base.End();
        master.agent.acceleration = lastAcceleration;
        sm.basicModel.SetActive(true);
        sm.ballModel.SetActive(false);

    }
}
