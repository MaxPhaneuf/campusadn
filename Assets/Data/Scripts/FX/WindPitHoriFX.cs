﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindPitHoriFX : MonoBehaviour
{
    private WindPitHori parentScriptH;
    private Transform parent;
    private GameObject currentFX;
    private Vector3 offset;
    private Transform tr;
    // Start is called before the first frame update
    void Start()
    {
        parentScriptH = GetComponentInParent<WindPitHori>();
        parent = GetComponentInParent<Transform>();
        tr = transform;
        tr.rotation = parent.rotation;
    }

    private void LateUpdate()
    {
        if (currentFX != null)
        {
            currentFX.transform.position = parent.position + offset;

        }

    }
    public void PlayFX(string name)
    {
        offset = new Vector3(0, 0f, 0);
        GameObject currentFX = PoolingManager.instance.GetObjectAutoReturn(name, transform.position + offset, 5f);
        parentScriptH.idleFxGO.SetActive(false);
        parentScriptH.idleFxGOH.SetActive(false);
        currentFX.transform.SetParent(parent);
    }

    public void ResetIdleFX()
    {
        parentScriptH.idleFxGO.SetActive(true);
        parentScriptH.idleFxGOH.SetActive(true);
    }
}
