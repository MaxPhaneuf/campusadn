﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMEOursSL : NMEStateListener
{
    readonly int triggerAttack1 = Animator.StringToHash("triggerAttack1");
    readonly int triggerAttack2 = Animator.StringToHash("triggerAttack2");

    protected override void InitStates()
    {
        base.InitStates();
        InitThrow();
        InitPound();
        InitPursuit();
    }

    void InitThrow()
    {
        NMEOursThrow thrw = (NMEOursThrow)machine.GetState(NMEStateMachine.State.Attack2);
        thrw.OnStart.AddListener(() => Anim.SetTrigger(triggerAttack2));
    }

    void InitPound()
    {
        NMEOursPound pound = (NMEOursPound)machine.GetState(NMEStateMachine.State.Attack1);
        pound.OnStart.AddListener(() => Anim.SetTrigger(triggerAttack1));
    }

    void InitPursuit()
    {
        NMEOursPursuit pursuit = (NMEOursPursuit)machine.GetState(NMEStateMachine.State.Pursuit);
        pursuit.OnStart.AddListener(() => Anim.SetBool(isRunningAtTarget, true));
        pursuit.OnEnd.AddListener(() => Anim.SetBool(isRunningAtTarget, false));
        pursuit.IsInRange.AddListener((isInRange) => Anim.SetBool(isTargetInRange, isInRange));
    }
       
}
