﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManagerSound : MonoBehaviour
{
    [SerializeField] SoundClip[] poolArray;

    private Dictionary<string, SoundClip> pool = new Dictionary<string, SoundClip>();

    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < poolArray.Length; i++)
        {
            SoundClip obj = poolArray[i];
            pool.Add(obj.name, obj);
        }
    }

    public AudioClip GetClip(string name)
    {
        return pool[name].clip;
    }
                
    [System.Serializable]
    public class SoundClip
    {
        public string name;
        public AudioClip clip;

    }
}


