﻿using UnityEngine;
using UnityEngine.Events;


public class NMECerfDash : NMEState
{
    float dashHitStartTime = 1f;
    float dashDuration = 5f;
    float hitDistance = 2f;
    float hitRadius = 3f;
    NMECerfSM sm;
    public override void Start()
    {
        base.Start();
        master.transform.LookAt(new Vector3(GameManager.instance.currentAvatar.tr.position.x, master.transform.position.y, GameManager.instance.currentAvatar.tr.position.z));
        master.agent.updateRotation = false;
        sm = (NMECerfSM)master;
    }

    public override void Update()
    {
        base.Update();
        //Collider[] colls = Physics.OverlapSphere(master.transform.position, hitRadius);
        //if(colls.Length > 0 && timeInState >= dashHitStartTime)
        //{
        //    foreach (Collider c in colls)
        //    {
        //        if(c.transform.tag == "Player")
        //        {
        //            OnDashHit.Invoke();
        //            GameManager.instance.currentAvatar.GettingKnocked();
        //            master.ChangeState(NMEStateMachine.State.Alert);
        //            return;
        //        }

        //    }
        //}
        if (timeInState >= dashDuration)
            sm.DashNoHit();
   
    }
}
