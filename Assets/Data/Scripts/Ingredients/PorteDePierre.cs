﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PorteDePierre : Triggerable
{
    Animator anim;
    public override void Activate()
    {
        anim.SetBool("isOpening", true);
    }

    public override void Deactivate()
    {
        anim.SetBool("isOpening", false);
    }

    public override void Toggle()
    {
        anim.SetBool("isOpening", !anim.GetBool("isOpening"));
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }
}
