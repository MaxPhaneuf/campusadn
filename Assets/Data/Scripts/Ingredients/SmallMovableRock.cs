﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallMovableRock : MonoBehaviour
{
    Transform tr;
    Vector3 direction;
    public float distance;
    RaycastHit ground;
    RaycastHit front;
    RaycastHit back;
    RaycastHit posdown;
    RaycastHit posup;
    RaycastHit posleft;
    RaycastHit posright;
    public bool grabbed = false;
    public bool release = false;
    public float cast = 2f;
    Collider coli;
    public float pos;
    public static SmallRock instance;
    RaycastHit under;
    public Transform fxOffset;


    void Start()
    {
        tr = transform;
        coli = GetComponentInChildren<Collider>();
        Character.instanceRock = this;
        
    }


    void Update()
    {
        
         Debug.DrawRay(tr.position + new Vector3(0,1,0)+direction, direction, Color.blue); // FRONT

        if (posright.transform == GameManager.instance.currentAvatar.transform)
        {
            pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
            pos = 180;
        }

        if (posleft.transform == GameManager.instance.currentAvatar.transform)
        {
            pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
            pos = 0;
        }

        if (posup.transform == GameManager.instance.currentAvatar.transform)
        {
            pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
            pos = 90;
        }

        if (posdown.transform == GameManager.instance.currentAvatar.transform)
        {
            pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
            pos = 270;
        }



        distance = Vector3.Distance(GameManager.instance.currentAvatar.transform.position, tr.position);
        direction = GameManager.instance.currentAvatar.transform.forward;

        Physics.Raycast(tr.position, direction, out front, 1f);
        Physics.Raycast(tr.position + GameManager.instance.currentAvatar.transform.up, direction + Vector3.down, out ground, cast); //  LA VALEUR DU CAST DOIT ETRE MODIFIER QUAND ON VA AVOIR LE VRAI MODEL , POUR LE MOMENT AVEC LE CUBE ON LA MET A 2



        if (GameManager.instance.currentAvatar.hitForward.collider == coli && distance <= 2f && InputManager.instance.interaction)
        {
            


            grabbed = true;
        }

        if (grabbed == true)
        {
            GameManager.instance.currentAvatar.transform.eulerAngles = new Vector3(0, pos, 0);
            GameManager.instance.currentAvatar.animator.SetBool("isGrabbing", true);

            if (InputManager.instance.leftStick.z == 0)
            {
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
            }

            // pousser

            if (InputManager.instance.leftStick.z > 0 ) 
            {
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", true);

                if (ground.transform && !front.transform)
                {

                    if (ground.transform.tag == "Rock")
                    {
                        ground.collider.enabled = false;
                    }
                    tr.position = GameManager.instance.currentAvatar.transform.position + GameManager.instance.currentAvatar.transform.forward * 2f;  // A MODIFIER LE FLOAT POUR LE OFFSET EN AVANT DE L AVATAR
                }
            }

            // tirer
            if (InputManager.instance.leftStick.z < 0)
            {
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", true);
                tr.position = GameManager.instance.currentAvatar.transform.position + GameManager.instance.currentAvatar.transform.forward * 2f;
            }

            if (grabbed == false)
            {
                under.collider.enabled = true;
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isGrabbing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
            }

            if (InputManager.instance.bButton || InputManager.instance.jumping)
            {
                grabbed = false;
                tr.position = tr.position;
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isGrabbing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
            }
        }
    }
}
