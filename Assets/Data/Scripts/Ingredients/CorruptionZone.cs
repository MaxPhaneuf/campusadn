﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorruptionZone : Corruption
{
    private Collider coll;

    private void Start()
    {
        coll = GetComponentInChildren<Collider>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) GameManager.instance.currentAvatar.GettingHurt();
    }

    public override void Uncorrupt()
    {
        gameObject.SetActive(false);
    }
}
