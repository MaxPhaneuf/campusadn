﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolTester : MonoBehaviour
{
    public string FXName;
    AudioSource source;
    public PoolManagerSound pool;
    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        if (FXName != "")
        {
            source.clip = pool.GetClip(FXName);
        }
        source.Play();
    }

}
