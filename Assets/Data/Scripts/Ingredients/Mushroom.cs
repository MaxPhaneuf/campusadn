﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : Corruption
{

    public bool isCorrupt;
    public float force = 25f;
    private Transform tr;
    private Animator anim;
    Collider collision;
    RaycastHit detect;
    public float range;
    Vector3 direction;
    private bool animStop;
    private bool resetBump = true;
    bool active;
    [HideInInspector]
    public GameObject idleFxGO;
    public Vector3 offset;
    private Renderer rd;
    public Material normalMat;
    public Material corruptedMat;
    AudioSource sound;
    bool fxInit;

    void Start()
    {
        offset = new Vector3(0, 0.9f, 0);
        animStop = false;
        collision = GetComponentInChildren<Collider>();
        anim = GetComponentInChildren<Animator>();
        tr = transform;
        rd = GetComponentInChildren<Renderer>();
        
        sound = GetComponentInChildren<AudioSource>();

    }
    private void LateUpdate()
    {
        if (idleFxGO != null) idleFxGO.transform.position = tr.position + offset;
    }
        
    void Update()
    {
        if (!fxInit)
        {
            if (PoolingManager.instance)
            {
                idleFxGO = PoolingManager.instance.GetObject(PoolConst.FX_BO_JumpShroom_Idle_01, tr.position + offset);
                fxInit = true;
            }

        }
        if (!isCorrupt) { 
        direction = tr.position - GameManager.instance.currentAvatar.transform.position;

        Physics.Raycast(tr.position + new Vector3(0, 1, 0), -direction, out detect, 1.5f);

        if(detect.transform != null)
        {
            if (detect.transform.tag == "Player" && GameManager.instance.currentAvatar.isJumping == true && animStop == false)
            {
               // GameManager.instance.currentAvatar.animator.SetTrigger("shroom");
               // anim.SetTrigger("Bouncing");
                animStop = true;
            }
        }

        if(GameManager.instance.currentAvatar.onGround == true)
        {
            animStop = false;
        }

        if (GameManager.instance.currentAvatar.isDashing)
        {
            GameManager.instance.currentAvatar.velocityY = 0;
        }

        if (GameManager.instance.currentAvatar.hit.collider == collision)
        {
            if (GameManager.instance.currentAvatar.isGliding == true)
            {
                GameManager.instance.currentAvatar.animator.SetTrigger("shroom");
                anim.SetTrigger("Bouncing");
                force = 250;
                    int soundid = Random.Range(0, 3);
                    if (soundid == 0) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_MushroomBounce1"));
                    if (soundid == 1) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_MushroomBounce2"));
                    if (soundid == 2) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_MushroomBounce3"));
                    GameManager.instance.currentAvatar.isGliding = false;
                GameManager.instance.currentAvatar.canDoubleJump = true;
                GameManager.instance.currentAvatar.canDash = true;
                GameManager.instance.currentAvatar.velocityY = force * Time.deltaTime;

            }
            else if(!active)
            {
                GameManager.instance.currentAvatar.canDoubleJump = true;
                GameManager.instance.currentAvatar.canDash = true;
                if (resetBump)
                {
                    GameManager.instance.currentAvatar.animator.SetTrigger("shroom");
                    resetBump = false;
                    Invoke("ResetBomp", 0.5f);
                }
                // Sounds


                anim.SetTrigger("Bouncing");
                active = true;
                    int soundid = Random.Range(0, 3);
                    if (soundid == 0) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_MushroomBounce1"));
                    if (soundid == 1) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_MushroomBounce2"));
                    if (soundid == 2) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_MushroomBounce3"));
                }

            GameManager.instance.currentAvatar.velocityY = force;
        }

        force = 25f;
        } 
    }

    void ResetBomp()
    {
        active = false;
        resetBump = true;
    }
    #region Corruption

    public override void Corrupt()
    {
        isCorrupt = true;
        //rd.material = corruptedMat;
        //idleFxGO.SetActive(false);
    }

    public override void Uncorrupt()
    {
        isCorrupt = false;
        //rd.material = normalMat;
        //idleFxGO.SetActive(true);
    }

    #endregion


       


}
