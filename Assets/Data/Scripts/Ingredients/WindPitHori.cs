using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindPitHori : Triggerable
{
    private float force = 4f;
    private float forceBall = 9f;
    private Transform tr;
    Collider collision;
    RaycastHit block;
    private bool activated;
    public bool open;
    Vector3 baseMotion;
    [HideInInspector]
    public GameObject idleFxGO;
    public GameObject idleFxGOH;
    public Vector3 offset;
    AudioSource sound;

    void Start()
    {
        activated = true;
        //GetComponentInChildren<Collider>();
        tr = transform;
        //this.GetComponent<CapsuleCollider>().enabled = false;
        offset = new Vector3(0, 0f, 0);
        idleFxGO = PoolingManager.instance.GetObject("FX_Prop_PuitVent_5m_01", tr.position + offset, tr.rotation);
        idleFxGOH = PoolingManager.instance.GetObject("FX_Prop_PuitVent_10m_01", tr.position + offset, tr.rotation);
        if (open)
            idleFxGO.SetActive(true);
        else
            idleFxGO.SetActive(false);
        idleFxGOH.SetActive(false);

        sound = GetComponentInChildren<AudioSource>();
    }

    private void Update()
    {
        if (GameManager.instance.currentAvatar.isRolling)
        {
            this.GetComponent<CapsuleCollider>().enabled = true;
        }
        else
        {
            this.GetComponent<CapsuleCollider>().enabled = false;
        }
        if (open)
        {
            idleFxGO.SetActive(true);
        }
        else
        {
            idleFxGO.SetActive(false);
            idleFxGOH.SetActive(false);
        }
    }

    private void LateUpdate()
    {
        idleFxGO.transform.position = tr.position;
        idleFxGOH.transform.position = tr.position;
    }

    public override void Activate()
    {
        open = true;
    }

    public override void Deactivate()
    {
        open = false;
    }

    public override void Toggle()
    {
        open = true;
    }
    /* private void OnTriggerEnter(Collider other)
     {
         if(other.tag =="Player")
             baseMotion = GameManager.instance.currentAvatar.velocity * Time.deltaTime;
     }
     */
    private void OnTriggerStay(Collider other)
    {
        if (open == true)
        {
            if (other.transform.tag == "Platform" || other.transform.tag == "Rock")
            {
                activated = false;
                return;
            }

            /*if (other.transform.tag == "Player")
            {
                activated = true;
            }
            */
            if (other.transform.tag == "Player" && activated)
            {
                if (GameManager.instance.currentAvatar.isRolling)
                {

                    GameManager.instance.currentAvatar.controller.Move(tr.up * forceBall * Time.deltaTime);
                    idleFxGOH.SetActive(true);
                    idleFxGO.SetActive(false);
                }
                else
                {
                    GameManager.instance.currentAvatar.controller.Move(tr.up * force * Time.deltaTime);
                    idleFxGO.SetActive(true);
                    idleFxGOH.SetActive(false);
                }

            }
        }

        if (!sound.isPlaying)
        {
            sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_WindPit"));
        }
    }



    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Platform" || other.transform.tag == "Rock")
        {
            activated = true;
            return;
        }
        if (other.tag == "Player")
            GameManager.instance.currentAvatar.velocity = baseMotion;
    }
}
