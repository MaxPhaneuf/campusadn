﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMECerfHeadTrigger : MonoBehaviour
{
    public NMECerfSM sm;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player" && sm.Current == NMEStateMachine.State.Attack1)
        {
            sm.OnDashHit.Invoke();
            GameManager.instance.currentAvatar.GettingKnocked();
            sm.ChangeState(NMEStateMachine.State.Alert);
        }
    }
}
