// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "OPE_CorruptionShader"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_Corruption_Color("Corruption_Color", Color) = (0.500061,0.2057227,0.6509434,0)
		_Normal("Normal", 2D) = "bump" {}
		_Pattern("Pattern", 2D) = "white" {}
		_ToonRamp("Toon Ramp", 2D) = "white" {}
		_RampTint("RampTint", Color) = (0,0,0,0)
		_Pattern_Tiling("Pattern_Tiling", Range( 0 , 100)) = 0
		[HDR]_RimColor("Rim Color", Color) = (0,1,0.8758622,0)
		_Fresnel_Power("Fresnel_Power", Range( 0 , 20)) = 4.527297
		_RimPower("Rim Power", Range( 0 , 10)) = 0.5
		_Fresnel_Bias("Fresnel_Bias", Range( 0 , 1)) = 1
		_RimOffset("Rim Offset", Float) = 0.24
		_Emmissive_Power("Emmissive_Power", Range( 0 , 10)) = 0
		_Animation_X("Animation_X", Range( -1 , 1)) = 0
		_Animation_Y("Animation_Y", Range( -1 , 1)) = 0
		_Pattern_Speed("Pattern_Speed", Range( 0 , 1)) = 0
		_Corruption_Amount("Corruption_Amount", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _Corruption_Color;
		uniform sampler2D _Pattern;
		uniform float _Pattern_Tiling;
		uniform float _Animation_X;
		uniform float _Animation_Y;
		uniform float _Pattern_Speed;
		uniform float _Corruption_Amount;
		uniform float _Fresnel_Bias;
		uniform float _Fresnel_Power;
		uniform float _Emmissive_Power;
		uniform sampler2D _ToonRamp;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float4 _RampTint;
		uniform float _RimOffset;
		uniform float _RimPower;
		uniform float4 _RimColor;

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 Color39 = _Corruption_Color;
			float4 appendResult9 = (float4(_Pattern_Tiling , _Pattern_Tiling , 0.0 , 0.0));
			float2 appendResult6 = (float2(_Animation_X , _Animation_Y));
			float mulTime4 = _Time.y * _Pattern_Speed;
			float2 uv_TexCoord13 = i.uv_texcoord * appendResult9.xy + ( appendResult6 * mulTime4 );
			float Pattern19 = tex2D( _Pattern, uv_TexCoord13 ).r;
			float Corruption_Amount69 = _Corruption_Amount;
			float4 lerpResult56 = lerp( tex2D( _Albedo, uv_Albedo ) , ( Color39 + Pattern19 ) , Corruption_Amount69);
			float4 Albedo59 = lerpResult56;
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult115 = dot( (WorldNormalVector( i , UnpackNormal( tex2D( _Normal, uv_Normal ) ) )) , ase_worldlightDir );
			float2 temp_cast_4 = (saturate( (dotResult115*0.5 + 0.5) )).xx;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			UnityGI gi87 = gi;
			float3 diffNorm87 = ase_worldNormal;
			gi87 = UnityGI_Base( data, 1, diffNorm87 );
			float3 indirectDiffuse87 = gi87.indirect.diffuse + diffNorm87 * 0.0001;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float dotResult114 = dot( ase_worldNormal , ase_worldViewDir );
			float4 CustomLighting119 = ( ( ( Albedo59 * ( tex2D( _ToonRamp, temp_cast_4 ) * _RampTint ) ) * ( ase_lightColor * float4( ( indirectDiffuse87 + ase_lightAtten ) , 0.0 ) ) ) + ( saturate( ( ( ase_lightAtten * dotResult115 ) * pow( ( 1.0 - saturate( ( dotResult114 + _RimOffset ) ) ) , _RimPower ) ) ) * ( _RimColor * ase_lightColor ) ) );
			c.rgb = CustomLighting119.rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 Color39 = _Corruption_Color;
			float4 appendResult9 = (float4(_Pattern_Tiling , _Pattern_Tiling , 0.0 , 0.0));
			float2 appendResult6 = (float2(_Animation_X , _Animation_Y));
			float mulTime4 = _Time.y * _Pattern_Speed;
			float2 uv_TexCoord13 = i.uv_texcoord * appendResult9.xy + ( appendResult6 * mulTime4 );
			float Pattern19 = tex2D( _Pattern, uv_TexCoord13 ).r;
			float Corruption_Amount69 = _Corruption_Amount;
			float4 lerpResult56 = lerp( tex2D( _Albedo, uv_Albedo ) , ( Color39 + Pattern19 ) , Corruption_Amount69);
			float4 Albedo59 = lerpResult56;
			o.Albedo = Albedo59.rgb;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV23 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode23 = ( _Fresnel_Bias + 1.0 * pow( 1.0 - fresnelNdotV23, (20.0 + (_Fresnel_Power - 0.0) * (0.0 - 20.0) / (20.0 - 0.0)) ) );
			float lerpResult74 = lerp( 0.0 , ( Pattern19 * fresnelNode23 ) , Corruption_Amount69);
			float Opacity32 = lerpResult74;
			float lerpResult63 = lerp( 0.0 , ( Opacity32 * _Emmissive_Power ) , Corruption_Amount69);
			float Emmissive38 = lerpResult63;
			float3 temp_cast_2 = (Emmissive38).xxx;
			o.Emission = temp_cast_2;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16301
7;189;1694;822;1789.004;2168.578;2.070484;True;False
Node;AmplifyShaderEditor.CommentaryNode;1;-2274.589,-1239.015;Float;False;1904.72;702.5156;Comment;11;19;16;13;10;9;6;5;4;3;2;49;Pattern;1,0,0.9476299,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;49;-2143.338,-697.6978;Float;False;Property;_Pattern_Speed;Pattern_Speed;16;0;Create;True;0;0;False;0;0;0.269;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;2;-2151.802,-858.8305;Float;False;Property;_Animation_Y;Animation_Y;15;0;Create;True;0;0;False;0;0;-0.03;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-2151.802,-970.83;Float;False;Property;_Animation_X;Animation_X;14;0;Create;True;0;0;False;0;0;1;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-1891.08,-1083.389;Float;False;Property;_Pattern_Tiling;Pattern_Tiling;7;0;Create;True;0;0;False;0;0;3.5;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;4;-1834.339,-770.6584;Float;True;1;0;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;6;-1809.802,-921.8305;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;9;-1569.51,-1123.588;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-1553.408,-920.9202;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;13;-1379.991,-1116.914;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;78;-2504.936,1736.406;Float;False;434.201;360.7996;Comment;3;114;111;109;World_Normals;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;79;-2705.237,1272.406;Float;False;813.7008;400.4002;Comment;4;110;115;112;113;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;110;-2666.937,1349.406;Float;True;Property;_Normal;Normal;3;0;Create;True;0;0;False;0;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;80;-1754.886,1683.818;Float;False;1510.165;572.144;;13;108;107;106;105;104;103;102;101;100;99;98;97;96;Rim Light;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;31;-207.6122,-1581.815;Float;False;1419.347;822.6175;comment;8;59;56;70;121;57;39;122;34;Albedo;0.008216619,1,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;12;-2278.826,-492.6439;Float;False;2256.871;655.128;comment;10;32;28;24;23;47;20;15;74;75;76;Opacity;0,0.722204,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;72;-1574.674,-1540.455;Float;False;634.2417;166.0228;Comment;2;68;69;Corruption_Amount;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;109;-2488.936,1784.406;Float;False;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;16;-981.5104,-1145.98;Float;True;Property;_Pattern;Pattern;4;0;Create;True;0;0;False;0;6a0170b861f18b84aa3c3f823e74e3fa;6a0170b861f18b84aa3c3f823e74e3fa;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;111;-2477.176,1934.813;Float;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ColorNode;34;-168.1123,-1215.016;Float;False;Property;_Corruption_Color;Corruption_Color;2;0;Create;True;0;0;False;0;0.500061,0.2057227,0.6509434,0;0.5054774,0.0487273,0.6886792,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;19;-618.2372,-1123.68;Float;False;Pattern;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;114;-2219.045,1856.945;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;68;-1524.674,-1490.455;Float;False;Property;_Corruption_Amount;Corruption_Amount;17;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-2241.588,50.30358;Float;False;Property;_Fresnel_Power;Fresnel_Power;9;0;Create;True;0;0;False;0;4.527297;20;0;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;96;-1738.659,1955.817;Float;True;Property;_RimOffset;Rim Offset;12;0;Create;True;0;0;False;0;0.24;-0.09;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;112;-2344.936,1512.406;Float;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;81;-1577.644,802.2318;Float;False;649.599;256;Also know as Lambert Wrap or Half Lambert;3;86;83;82;Diffuse Wrap;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;113;-2296.936,1352.406;Float;False;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;39;126.538,-1213.382;Float;False;Color;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;122;47.3277,-980.1099;Float;True;19;Pattern;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;69;-1219.432,-1489.433;Float;False;Corruption_Amount;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;47;-1985.757,-126.0546;Float;False;Property;_Fresnel_Bias;Fresnel_Bias;11;0;Create;True;0;0;False;0;1;0.676;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;20;-1889.47,-24.38985;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;20;False;3;FLOAT;20;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;97;-1583.659,1840.818;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;82;-1565.644,977.2318;Float;False;Constant;_WrapperValue;Wrapper Value;0;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;115;-2026.936,1424.406;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;24;-1514.403,-350.2239;Float;False;19;Pattern;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;84;-1369.757,1278.324;Float;False;734;262;Comment;5;93;90;89;87;85;Attenuation and Ambient;1,1,1,1;0;0
Node;AmplifyShaderEditor.FresnelNode;23;-1657.723,-141.9921;Float;False;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0.94;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;70;378.1421,-941.7303;Float;False;69;Corruption_Amount;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;121;415.2276,-1209.01;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;57;30.27591,-1523.54;Float;True;Property;_Albedo;Albedo;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScaleAndOffsetNode;83;-1300.242,852.2318;Float;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;98;-1370.658,1843.818;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;86;-1067.045,859.0314;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-1298.539,-344.2399;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;100;-1306.658,1971.817;Float;False;Property;_RimPower;Rim Power;10;0;Create;True;0;0;False;0;0.5;1.65;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;99;-1194.658,1843.818;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;75;-1235.62,17.15359;Float;False;69;Corruption_Amount;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;76;-1225.945,-86.30754;Float;False;Constant;_NormalOpacity;NormalOpacity;12;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;85;-1358.357,1464.324;Float;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;56;714.5102,-1378.219;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;123;-886.5447,762.116;Float;False;906.6274;483.4849;Comment;5;88;116;91;117;92;ToonRamp;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;88;-836.5447,841.9313;Float;True;Property;_ToonRamp;Toon Ramp;5;0;Create;True;0;0;False;0;3096cc9376637d948990735ce30b7836;3096cc9376637d948990735ce30b7836;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;101;-997.3211,1733.151;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;102;-1002.658,1843.818;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;74;-944.0714,-339.9474;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;59;957.1357,-1376.907;Float;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;87;-1169.757,1406.324;Float;False;Tangent;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;116;-749.6443,1043.601;Float;False;Property;_RampTint;RampTint;6;0;Create;True;0;0;False;0;0,0,0,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;117;-343.8613,812.116;Float;False;59;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;77;-1056.103,233.1711;Float;False;1066.253;455.2469;Comment;7;73;30;66;36;71;63;38;Emmissive;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;32;-682.6417,-343.5396;Float;False;Opacity;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;91;-378.9944,920.4099;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightColorNode;103;-730.6583,2131.817;Float;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;90;-929.7557,1438.324;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightColorNode;89;-1313.757,1326.324;Float;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.ColorNode;104;-842.6583,1955.817;Float;False;Property;_RimColor;Rim Color;8;1;[HDR];Create;True;0;0;False;0;0,1,0.8758622,0;0.4386792,1,0.9295598,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;105;-762.6583,1811.818;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;106;-570.6583,1811.818;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-1006.103,492.4643;Float;False;Property;_Emmissive_Power;Emmissive_Power;13;0;Create;True;0;0;False;0;0;0.97;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;73;-897.1639,375.2319;Float;False;32;Opacity;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;107;-538.6583,1939.817;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;93;-769.7557,1326.324;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;92;-148.9173,921.5548;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;71;-733.6614,573.418;Float;False;69;Corruption_Amount;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-714.6042,377.2812;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;108;-378.6583,1811.818;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;94;33.05545,1256.131;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-706.2485,466.1499;Float;False;Constant;_NormalEmmissive;NormalEmmissive;14;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;95;206.4553,1497.931;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;63;-455.1132,358.0169;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;7;-2273.646,281.8623;Float;False;697.1968;367.0809;Comment;3;8;14;11;Time;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;38;-232.8493,353.8865;Float;False;Emmissive;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;119;378.4603,1543.384;Float;False;CustomLighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SinTimeNode;8;-2239.851,399.771;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;14;-1810.861,389.0226;Float;True;Time;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;120;624.6561,-54.39856;Float;False;119;CustomLighting;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;41;632.5852,-369.8728;Float;False;59;Albedo;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;11;-2059.76,394.7817;Float;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;1;False;4;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;44;622.2346,-286.5336;Float;False;38;Emmissive;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;922.1559,-350.4073;Float;False;True;2;Float;ASEMaterialInspector;0;0;CustomLighting;OPE_CorruptionShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Opaque;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;4;0;49;0
WireConnection;6;0;3;0
WireConnection;6;1;2;0
WireConnection;9;0;5;0
WireConnection;9;1;5;0
WireConnection;10;0;6;0
WireConnection;10;1;4;0
WireConnection;13;0;9;0
WireConnection;13;1;10;0
WireConnection;16;1;13;0
WireConnection;19;0;16;1
WireConnection;114;0;109;0
WireConnection;114;1;111;0
WireConnection;113;0;110;0
WireConnection;39;0;34;0
WireConnection;69;0;68;0
WireConnection;20;0;15;0
WireConnection;97;0;114;0
WireConnection;97;1;96;0
WireConnection;115;0;113;0
WireConnection;115;1;112;0
WireConnection;23;1;47;0
WireConnection;23;3;20;0
WireConnection;121;0;39;0
WireConnection;121;1;122;0
WireConnection;83;0;115;0
WireConnection;83;1;82;0
WireConnection;83;2;82;0
WireConnection;98;0;97;0
WireConnection;86;0;83;0
WireConnection;28;0;24;0
WireConnection;28;1;23;0
WireConnection;99;0;98;0
WireConnection;56;0;57;0
WireConnection;56;1;121;0
WireConnection;56;2;70;0
WireConnection;88;1;86;0
WireConnection;101;0;85;0
WireConnection;101;1;115;0
WireConnection;102;0;99;0
WireConnection;102;1;100;0
WireConnection;74;0;76;0
WireConnection;74;1;28;0
WireConnection;74;2;75;0
WireConnection;59;0;56;0
WireConnection;32;0;74;0
WireConnection;91;0;88;0
WireConnection;91;1;116;0
WireConnection;90;0;87;0
WireConnection;90;1;85;0
WireConnection;105;0;101;0
WireConnection;105;1;102;0
WireConnection;106;0;105;0
WireConnection;107;0;104;0
WireConnection;107;1;103;0
WireConnection;93;0;89;0
WireConnection;93;1;90;0
WireConnection;92;0;117;0
WireConnection;92;1;91;0
WireConnection;36;0;73;0
WireConnection;36;1;30;0
WireConnection;108;0;106;0
WireConnection;108;1;107;0
WireConnection;94;0;92;0
WireConnection;94;1;93;0
WireConnection;95;0;94;0
WireConnection;95;1;108;0
WireConnection;63;0;66;0
WireConnection;63;1;36;0
WireConnection;63;2;71;0
WireConnection;38;0;63;0
WireConnection;119;0;95;0
WireConnection;14;0;11;0
WireConnection;11;0;8;4
WireConnection;0;0;41;0
WireConnection;0;2;44;0
WireConnection;0;13;120;0
ASEEND*/
//CHKSM=3A4D126DD35AB433734C91EE64BE95F9AC2955A0