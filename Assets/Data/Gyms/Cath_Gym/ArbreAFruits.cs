﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArbreAFruits : MonoBehaviour
{
    //List<GameObject> nbFruits= new List<GameObject>();
    private Transform tr;
    private float maxX = 1f;
    private float maxY = 8f;
    private float minZ = 5f;
    private float maxZ = 1f;
    public float delayTilDrop = 1f;
    private int fruitCount = 0;
    private bool canDrop = true;
    private Vector3 spawnFruits;
    public GameObject fruitDeco;
    public GameObject[] fruitDecoTab;
    private GameObject fruit;
    [Range(0,3)]
    public int nbFruits;
    private Animator animator;

    public enum StateName
    {
        idle, isDashed
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        fruitDecoTab = new GameObject[nbFruits];
        tr = transform;
        for (int i = 0; i < nbFruits; i++)
        {  
            DecoFruit();
        }
    }

    void FruitPosition()
    {
        spawnFruits = new Vector3(tr.position.x,tr.position.y,tr.position.z);
    }

    void DropFruit()
    {
        //fruitDecoTab[fruitCount-1].gameObject.GetComponent<Rigidbody>().useGravity= true;
        fruit = fruitDecoTab[fruitCount - 1];
        fruit.SetActive(true);
        fruitCount--;
    }

    void DecoFruit()
    {
        //Faire spawner les fruits au bon endroit sur le prefab
        FruitPosition();
        GameObject fruit = Instantiate(fruitDeco, spawnFruits, Quaternion.identity);
        AddFruitToTab(fruit);
    }

    void AddFruitToTab(GameObject fruit)
    {
        fruitDecoTab[fruitCount] = fruit;
        fruitCount++;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && GameManager.instance.currentAvatar.isDashing && canDrop)
            if (fruitCount > 0)
            {
                animator.SetTrigger("isDashed");
                Invoke("DropFruit", delayTilDrop); 
                canDrop = false;
                Invoke("ResetDropFruit", delayTilDrop);
            }
    }
    void ResetDropFruit()
    {
        canDrop = true;
    }
}
