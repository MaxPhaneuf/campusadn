﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatePushPull : CharacterState
{
    public override void ToIdle()
    {
        myMaster.ChangeStates(Character.StateName.idle);
    }
    public override void ToLocomotion(){}
    public override void ToJump(){}
    public override void ToFalling(){}
    public override void ToDoubleJump(){}
    public override void ToGlide(){}
    public override void ToDash(){}
    public override void ToPushPull(){}
    public override void ToRolling(){}

    public override void RollInput(){}
    public override void DashInput(){}
    public override void CancelButton()
    {
        myMaster.isGrabbing = false;
        myMaster.animator.SetBool(ANIM_PARAM_GRABBING, false);
        ToIdle();
    }
    public override void InteractInput(){}
    public override void JumpInput(){}
    public override void MoveInput(){}
    public StatePushPull(Character myMaster) : base(myMaster) { }
}
