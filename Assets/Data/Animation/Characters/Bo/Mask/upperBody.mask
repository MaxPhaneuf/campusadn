%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: upperBody
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Arrow
    m_Weight: 0
  - m_Path: Line001
    m_Weight: 0
  - m_Path: Line001/Bip001
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Footsteps
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 L Thigh
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 L Thigh/Bip001 L Calf
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 L Thigh/Bip001 L Calf/Bip001 L Foot
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 L Thigh/Bip001 L Calf/Bip001 L Foot/Bip001
      L Toe0
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 R Thigh
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 R Thigh/Bip001 R Calf
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 R Thigh/Bip001 R Calf/Bip001 R Foot
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 R Thigh/Bip001 R Calf/Bip001 R Foot/Bip001
      R Toe0
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/Bip001 L Forearm
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/Bip001 L Forearm/Bip001 L Hand
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/Bip001 L Forearm/Bip001 L Hand/Bip001 L Finger0
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/Bip001 L Forearm/Bip001 L Hand/Bip001 L Finger0/Bip001 L Finger01
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/Bip001 L Forearm/Bip001 L Hand/Bip001 L Finger0/Bip001 L Finger01/Bip001
      L Finger02
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/Bip001 L Forearm/Bip001 L Hand/Bip001 L Finger1
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/Bip001 L Forearm/Bip001 L Hand/Bip001 L Finger1/Bip001 L Finger11
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/Bip001 L Forearm/Bip001 L Hand/Bip001 L Finger1/Bip001 L Finger11/Bip001
      L Finger12
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/Bip001 L Forearm/L_Wrist_Twistbone
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001
      L UpperArm/L_Shoulder_Twistbone
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001
      Head
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001
      Head/Hat_Root
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001
      Head/Hat_Root/Bip001 Xtra09
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001
      Head/Hat_Root/Bip001 Xtra09/Bip001 Xtra10
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001
      Head/Hat_Root/Hat_Back
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001
      Head/Hat_Root/Hat_Front
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001
      Head/Hat_Root/Hat_Left
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001
      Head/Hat_Root/Hat_Right
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Scarf_01
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Scarf_01/Scarf_02
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Scarf_01/Scarf_02/Scarf_03
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Scarf_01/Scarf_02/Scarf_03/Scarf_04
    m_Weight: 0
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand/Arrow_Root
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand/Arrow_Root/Arrow_Bottom
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand/Arrow_Root/Arrow_Up
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand/Bip001 R Finger0
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand/Bip001 R Finger0/Bip001 R Finger01
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand/Bip001 R Finger0/Bip001 R Finger01/Bip001
      R Finger02
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand/Bip001 R Finger1
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand/Bip001 R Finger1/Bip001 R Finger11
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/Bip001 R Hand/Bip001 R Finger1/Bip001 R Finger11/Bip001
      R Finger12
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/Bip001 R Forearm/R_Wrist_Twistbone
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001
      R UpperArm/R_Shoulder_Twistbone
    m_Weight: 1
  - m_Path: Line001/Bip001/Bip001 Prop1
    m_Weight: 0
  - m_Path: Mesh
    m_Weight: 0
