﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMEOursPound : NMEState
{
    NMEOursSM sm;
    float hitRadius = 5;
    public override void Start()
    {
        base.Start();
        sm = (NMEOursSM)master;
    }

    public override void Update()
    {
        base.Update();
        if (timeInState >= sm.timeOfPoundAnim)
            master.ChangeState(NMEStateMachine.State.Alert);
    }

    public override void End()
    {
        base.End();
        if (Vector3.Distance(master.transform.position, GameManager.instance.currentAvatar.tr.position) <= hitRadius)
            GameManager.instance.currentAvatar.GettingKnocked();
    }
}
