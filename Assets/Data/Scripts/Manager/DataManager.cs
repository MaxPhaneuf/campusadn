﻿// Création du script : Jonathan G. Mann
// Date : 03-04-2019 11:30 
// Modification : Catherine Demeule
// Date de modification : 12-04 16:00 
//Inspiration: https://unity3d.com/learn/tutorials/topics/scripting/persistence-saving-and-loading-data

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
//Utiliser les Inputs et Outputs
using System.IO;
using UnityEngine.SceneManagement;

public class DataManager : MonoBehaviour
{
    #region Variables
    //Added le 4 avril par Catherine
    public static DataManager myself;
    public static int nbSaveSpaceMax = 3;
    public PlayerData[] playerData = new PlayerData[nbSaveSpaceMax];
    public List<PlayerData> listTemp = new List<PlayerData>();
    PlayerData tempCheckpoint;
    PlayerData tempToTransfer;
    DateTime dateGame;
    public GameObject[] collectibles;
    public GameObject[] archives;
    public int patates;
    public int saveSpaceNb;
    public int[] skills;
    public int health;
    private int copy1;
    private int copy2;
    public string[] narration;
    string nbPatates = System.String.Empty;
    string nbPatates2 = System.String.Empty;
    string nbPatates3 = System.String.Empty;
    const string DIRECTORY = "/Data/SaveGame/";
    const string EXTENSION = ".sg";
    //public GameManager gameManager;
    public bool updateVariables = false;
    public bool loadingGame;
    private bool toggleErase;
    private bool toggleCopy1;
    private bool toggleCopy2;
    private bool toggleCancel;
    private bool privateCount = true;
    #endregion
    private void Awake()
    {
        if (!myself) myself = this;
        else Destroy(gameObject);
        RefreshGameInfo();
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if (privateCount)
        {
            if (listTemp.Count > 0)
            {
                nbPatates = listTemp[0].patates.ToString();
                nbPatates2 = listTemp[1].patates.ToString();
                nbPatates3 = listTemp[2].patates.ToString();
                dateGame = listTemp[0].date;
                dateGame = listTemp[1].date;
                dateGame = listTemp[2].date;
                collectibles = listTemp[0].collectibles;
                collectibles = listTemp[1].collectibles;
                collectibles = listTemp[2].collectibles;
                privateCount = false;
            }
        }
    }
    /// <summary>
    /// Refresh les infos affichees en partie sauvegardee
    /// </summary>
    void RefreshGameInfo()
    {
        listTemp = new List<PlayerData>();
        for (int i = 0; i < 3; i++)
        {
            LoadGame(i + 1,false);
        }
        privateCount = true;
    }
    #region SaveFunctions
    /// <summary>
    /// Permet d'utiliser la fonction pour sauvegarder
    /// </summary>
    /// <param name="saveSpaceNb"></param>
    public void SaveGame(int saveSpaceNb, PlayerData data)
    {
        SaveGameManager.Save(saveSpaceNb.ToString(), data);
        TransferSaveData(data, saveSpaceNb);
        RefreshGameInfo();
    }
    /// <summary>
    /// Transfere les donnees a saver
    /// </summary>
    /// <param name="plata"></param>
    public void TransferSaveData(PlayerData plata, int saveSpaceNb)
    {
        playerData[saveSpaceNb - 1] = plata;
    }
    /// <summary>
    /// transfere le data pour la sauvegarde
    /// </summary>
    public PlayerData TransferData()
    {
       
        PlayerData temp = new PlayerData();
        temp.patates = patates;
        //temp.position = gameManager.currentAvatar.transform.position;
        //temp.avatarTransform = gameManager.currentAvatar.transform;
        temp.saveSpaceNb = saveSpaceNb;
        temp.health = health;
        temp.collectibles = collectibles;
        temp.archives = archives;
        temp.narration = narration;
        temp.skills = skills;
        //temp.currentLevel = gameManager.levelManager.current;
        temp.date = dateGame;
        return temp;
    }

    public void SaveGame(int saveSpaceNb)
    {
        SaveGame(saveSpaceNb, TransferData());
    }
    #endregion
    #region LoadFunctions
    /// <summary>
    /// Permet d'utiliser la fonction pour charger une partie
    /// </summary>
    /// <param name="saveSpaceNb"></param>
    public void LoadGame(int saveSpaceNb, bool transfer)
    {
        PlayerData temp = new PlayerData();
        SaveGameManager.Load(saveSpaceNb.ToString(), temp);
        if (temp != null)
            listTemp.Add(temp);
        tempToTransfer = temp;
        if(transfer)
            TransferLoadData(temp);
    }
    /// <summary>
    /// Transfere les donnees a loader
    /// </summary>
    public void TransferLoadData(PlayerData saveGameSpace)
    {
        PlayerData temp = saveGameSpace;
        patates = temp.patates;
        //gameManager.currentAvatar.transform.position = temp.position;
        //gameManager.currentAvatar.transform = temp.avatarTransform; //NEED CONSTRUCTEUR ASAP
        saveSpaceNb = temp.saveSpaceNb;
        health = temp.health;
        collectibles = temp.collectibles;
        archives = temp.archives;
        narration = temp.narration;
        skills = temp.skills;
        //gameManager.levelManager.current = temp.currentLevel;
        dateGame = temp.date;
    }

    public void LoadGame(int saveSpaceNb)
    {
        LoadGame(saveSpaceNb, true);
    }
    #endregion
    #region EraseFunctions
    /// <summary>
    /// Efface une partie sauvegardee
    /// </summary>
    /// <param name="saveSpaceNb"></param>
    public void EraseGame(int saveSpaceNb)
    {
        File.Delete(Application.dataPath + DIRECTORY + saveSpaceNb.ToString() + EXTENSION);
    }
    /// <summary>
    /// Permet d'effacer une partie
    /// </summary>
    private void CheckErase()
    {
        GUI.Label(new Rect(150, 340, 100, 30), "Which save?");
        if (GUI.Button(new Rect(150, 380, 100, 30), "Save Game 1"))
        {
            EraseGame(1);
            toggleErase = false;
        }
        if (GUI.Button(new Rect(150, 420, 100, 30), "Save Game 2"))
        {
            EraseGame(2);
            toggleErase = false;
        }
        if (GUI.Button(new Rect(150, 460, 100, 30), "Save Game 3"))
        {
            EraseGame(3);
            toggleErase = false;
        }
    }
    #endregion
    #region CopyFunctions
    /// <summary>
    /// Copie de partie A a partie B
    /// </summary>
    /// <param name="saveSpaceNbCopyFrom"></param>
    /// <param name="saveSpaceNbCopyTo"></param>
    public void CopyGame(int saveSpaceNbCopyFrom, int saveSpaceNbCopyTo)
    {
        File.Copy(Application.dataPath + DIRECTORY + saveSpaceNbCopyFrom.ToString() + EXTENSION, Application.dataPath + DIRECTORY + saveSpaceNbCopyTo.ToString() + EXTENSION, true);
    }
    /// <summary>
    /// Copie les donnees du premier fichier
    /// </summary>
    private void CheckCopyOne()
    {
        GUI.Label(new Rect(150, 340, 100, 30), "Copy From?");
        if (GUI.Button(new Rect(150, 380, 100, 30), "Save Game 1"))
        {
            CheckCopyTwo(1);
            TogglesCopyOne(1);
        }
        if (GUI.Button(new Rect(150, 420, 100, 30), "Save Game 2"))
        {
            CheckCopyTwo(2);
            TogglesCopyOne(2);
        }
        if (GUI.Button(new Rect(150, 460, 100, 30), "Save Game 3"))
        {
            CheckCopyTwo(3);
            TogglesCopyOne(3);
        }
    }
    private void TogglesCopyOne(int saveSpaceNb)
    {
        toggleCopy1 = false;
        copy1 = saveSpaceNb;
        toggleCopy2 = true;
    }
    /// <summary>
    /// Copie les donnes du deuxieme fichier
    /// </summary>
    /// <param name="one"></param>
    private void CheckCopyTwo(int one)
    {
        GUI.Label(new Rect(150, 340, 100, 30), "Copy To?");
        if (GUI.Button(new Rect(150, 380, 100, 30), "Save Game 1"))
        {
            TogglesCopyTwo(1);
            CopyGame(copy1, copy2);
        }
        if (GUI.Button(new Rect(150, 420, 100, 30), "Save Game 2"))
        {
            TogglesCopyTwo(2);
            CopyGame(copy1, copy2);
        }
        if (GUI.Button(new Rect(150, 460, 100, 30), "Save Game 3"))
        {
            TogglesCopyTwo(3);
            CopyGame(copy1, copy2);
        }
    }
    private void TogglesCopyTwo(int saveSpaceNb)
    {
        copy2 = saveSpaceNb;
        toggleCopy2 = false;
    }
    #endregion
    #region Checkpoint
    /// <summary>
    /// Donne au checkpoint les valeurs qu'il a besoin
    /// </summary>
    public void TransferTemp()
    {
        tempCheckpoint.patates = patates;
        //tempCheckpoint.position = gameManager.currentAvatar.transform.position;
        //tempCheckpoint.avatarTransform = gameManager.currentAvatar.transform;
        tempCheckpoint.collectibles = collectibles;
        tempCheckpoint.narration = narration;
        tempCheckpoint.skills = skills;
    }
    /// <summary>
    /// Transfere les donnees acquises par le checkpoint a l'avatar
    /// </summary>
    public void TransferTempLoad()
    {
        patates = tempCheckpoint.patates;
        //gameManager.currentAvatar.transform.position = tempCheckpoint.position;
        //gameManager.currentAvatar.transform = tempCheckpoint.avatarTransform;//GET CONSTRUCTEUR ASAP
        collectibles = tempCheckpoint.collectibles;
        narration = tempCheckpoint.narration;
        skills = tempCheckpoint.skills;
    }
    #endregion

    #region GUI
    /// <summary>
    /// Fonction servant a afficher les boutons save et load en appelant les fonctions respectives
    /// </summary>

   
   private void OnGUI()
   {/*
       GUI.Label(new Rect(10, 10, 100, 30), "Patates :" + patates);
       if (GUI.Button(new Rect(10, 50, 100, 30), "Ajouter patates"))
       {
           patates += 5;
           TransferData();
       }
       if (GUI.Button(new Rect(10, 100, 100, 40), "Save Game 1\n" + "Patates " + nbPatates))
           SaveGame(1, TransferData());
       if (GUI.Button(new Rect(10, 140, 100, 40), "Save Game 2\n" + "Patates " + nbPatates2))
           SaveGame(2, TransferData());
       if (GUI.Button(new Rect(10, 180, 100, 40), "Save Game 3\n" + "Patates " + nbPatates3))
           SaveGame(3, TransferData());
       if (GUI.Button(new Rect(10, 220, 100, 40), "Load Game 1"))
           LoadGame(1,true);
       if (GUI.Button(new Rect(10, 260, 100, 40), "Load Game 2"))
           LoadGame(2,true);
       if (GUI.Button(new Rect(10, 300, 100, 40), "Load Game 3"))
           LoadGame(3,true);
       if (GUI.Button(new Rect(10, 390, 100, 30), "Erase"))
       {
           toggleErase = true;
           toggleCancel = true;
       }
       if (GUI.Button(new Rect(10, 430, 100, 30), "Copy"))
       {
           toggleCopy1 = true;
           toggleCancel = true;
       }
       if (toggleErase)
           CheckErase();
       if (toggleCopy1)
           CheckCopyOne();
       if (toggleCopy2)
           CheckCopyTwo(copy1);
       if (toggleCancel)
           CancelButton();
   */
   }
   /// <summary>
   /// Bouton pour annuler une action du GUI
   /// </summary>
   private void CancelButton()
   {
       if (GUI.Button(new Rect(280, 380, 100, 30), "Cancel"))
       {
           toggleErase = false;
           toggleCopy1 = false;
           toggleCopy2 = false;
           toggleCancel = false;
       }
   }
}

    #endregion

    //Serialize : utilise pour la façon dont comment le systeme utilise le data, autorisation de sauvegarde sur une file
    //Cree par Catherine le 4 avril
    [Serializable]
public class PlayerData
{
    public int patates;
    public Vector3 position;
    public Transform avatarTransform;
    public int saveSpaceNb;
    public int health;
    public GameObject[] collectibles;
    public GameObject[] archives;
    public string[] narration;
    public int[] skills;
    public Scene currentLevel;
    public DateTime date = DateTime.Now;
}