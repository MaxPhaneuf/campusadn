﻿using UnityEngine;

public class NMEAlert : NMEState
{
    float timeAlerted = 2f;
    
    public override void Update()
    {
        base.Update();

        if (!isPlayerInRange)
        {
            master.ChangeState(NMEStateMachine.State.Patrol);
            return;
        }
        else 
        {
            master.ChangeState(NMEStateMachine.State.Pursuit);
            return;
        }
    }

    public override void End()
    {
        base.End();
        master.agent.updateRotation = true;
    }
}
