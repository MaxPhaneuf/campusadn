﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HedgehogTunnel : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
            GameManager.instance.currentAvatar.inTunnel = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            GameManager.instance.currentAvatar.inTunnel = false;
    }
}
