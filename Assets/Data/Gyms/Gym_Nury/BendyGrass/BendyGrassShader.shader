// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "BendyGrass"
{
	Properties
	{
		_OffsetGradient("OffsetGradient", 2D) = "white" {}
		_EffectRadius("EffectRadius", Range( 0 , 10)) = 1
		_EffectClampMax("EffectClampMax", Range( 0 , 10)) = 1
		_EffectTopOffset("EffectTopOffset", Float) = 2
		_OffsetGradientStrength("OffsetGradientStrength", Range( 0 , 1)) = 0.7
		_OffsetFixedRoots("OffsetFixedRoots", Range( 0 , 1)) = 1
		_OffsetMultiplier("OffsetMultiplier", Range( 0 , 10)) = 1
		_Albedo("Albedo", 2D) = "white" {}
		_Metallic("Metallic", 2D) = "white" {}
		_Roughness("Roughness", 2D) = "white" {}
		_Normal("Normal", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Transparent+0" }
		Cull Off
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
		};

		uniform sampler2D _OffsetGradient;
		uniform float4 _OffsetGradient_ST;
		uniform float _OffsetGradientStrength;
		uniform float _OffsetFixedRoots;
		uniform float _EffectRadius;
		uniform float _EffectTopOffset;
		uniform float _EffectClampMax;
		uniform float _OffsetMultiplier;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _Metallic;
		uniform float4 _Metallic_ST;
		uniform sampler2D _Roughness;
		uniform float4 _Roughness_ST;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 uv_OffsetGradient = v.texcoord * _OffsetGradient_ST.xy + _OffsetGradient_ST.zw;
			float lerpResult39 = lerp( tex2Dlod( _OffsetGradient, float4( uv_OffsetGradient, 0, 0.0) ).r , 1.0 , ( 1.0 - _OffsetGradientStrength ));
			float blendOpSrc45 = v.texcoord.xy.y;
			float blendOpDest45 = lerpResult39;
			float lerpResult46 = lerp( ( saturate( ( 1.0 - ( ( 1.0 - blendOpDest45) / blendOpSrc45) ) )) , 1.0 , ( 1.0 - _OffsetFixedRoots ));
			float XZOffset50 = lerpResult46;
			float3 ase_vertex3Pos = v.vertex.xyz;
			float3 _Player_Position = float3(0,0,0);
			float3 break2 = _Player_Position;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float WorldY17 = ase_worldPos.y;
			float PlayerY5 = break2.y;
			float PlayerTopY15 = ( PlayerY5 + _EffectTopOffset );
			float PlayerBottomY12 = ( PlayerY5 + -1.0 );
			float VertexY6 = ase_vertex3Pos.y;
			float RecalculatedY24 = (( WorldY17 > PlayerTopY15 ) ? PlayerTopY15 :  (( WorldY17 < PlayerBottomY12 ) ? PlayerBottomY12 :  VertexY6 ) );
			float4 appendResult7 = (float4(break2.x , RecalculatedY24 , break2.z , 0.0));
			float clampResult30 = clamp( ( _EffectRadius - distance( float4( ase_vertex3Pos , 0.0 ) , appendResult7 ) ) , 0.0 , _EffectClampMax );
			float3 break3 = _Player_Position;
			float4 appendResult33 = (float4(break3.x , ase_vertex3Pos.y , break3.z , 0.0));
			float4 normalizeResult35 = normalize( ( float4( ase_vertex3Pos , 0.0 ) - appendResult33 ) );
			float XZMultiplier53 = _OffsetMultiplier;
			float3 normalizeResult71 = normalize( -float3(0,-1,0) );
			float3 YMultiplier74 = ( normalizeResult71 * 2.0 );
			float temp_output_62_0 = v.texcoord.xy.y;
			float blendOpSrc63 = temp_output_62_0;
			float blendOpDest63 = temp_output_62_0;
			float lerpResult67 = lerp( ( saturate( ( blendOpSrc63 * blendOpDest63 ) )) , 1.0 , ( 1.0 - 1.0 ));
			float YOffset68 = lerpResult67;
			v.vertex.xyz += ( ( XZOffset50 * ( clampResult30 * normalizeResult35 ) * XZMultiplier53 ) + float4( ( YMultiplier74 * -clampResult30 * YOffset68 ) , 0.0 ) ).xyz;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			o.Normal = tex2D( _Normal, uv_Normal ).rgb;
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode78 = tex2D( _Albedo, uv_Albedo );
			o.Albedo = tex2DNode78.rgb;
			float2 uv_Metallic = i.uv_texcoord * _Metallic_ST.xy + _Metallic_ST.zw;
			o.Metallic = tex2D( _Metallic, uv_Metallic ).r;
			float2 uv_Roughness = i.uv_texcoord * _Roughness_ST.xy + _Roughness_ST.zw;
			o.Smoothness = ( 1.0 - tex2D( _Roughness, uv_Roughness ) ).r;
			o.Alpha = tex2DNode78.a;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16301
7;7;1674;1004;-2467.105;1651.664;1.651628;True;False
Node;AmplifyShaderEditor.Vector3Node;1;-1531.796,189.8233;Float;False;Constant;_Player_Position;Player_Position;0;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.BreakToComponentsNode;2;-1044.852,12.05226;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RegisterLocalVarNode;5;-709.7949,-108.9946;Float;False;PlayerY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;8;-1235.981,-1200.095;Float;False;5;PlayerY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-1224.281,-1046.696;Float;False;Constant;_Float0;Float 0;0;0;Create;True;0;0;False;0;-1;0;-1;-1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;4;-1011.052,-158.2478;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;11;-998.0815,-1155.896;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;9;-1242.481,-916.6951;Float;False;5;PlayerY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;16;-1245.081,-694.3968;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;13;-1250.281,-807.4963;Float;False;Property;_EffectTopOffset;EffectTopOffset;3;0;Create;True;0;0;False;0;2;2;2;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;17;-795.9531,-657.688;Float;False;WorldY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;12;-777.0815,-1161.096;Float;False;PlayerBottomY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;6;-749.586,-282.1824;Float;False;VertexY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;14;-987.6814,-889.3964;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;20;-384.5268,-697.8203;Float;False;6;VertexY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19;-420.7773,-778.3775;Float;False;12;PlayerBottomY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;15;-777.0815,-897.1968;Float;False;PlayerTopY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;18;-459.532,-1026.368;Float;False;17;WorldY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareLower;21;-90.49297,-842.8233;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;23;-87.99799,-947.626;Float;False;15;PlayerTopY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareGreater;22;144.7723,-989.5325;Float;False;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24;388.6725,-920.1016;Float;False;RecalculatedY;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25;-458.578,-88.6084;Float;False;24;RecalculatedY;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;37;892.1129,-781.2053;Float;True;Property;_OffsetGradient;OffsetGradient;0;0;Create;True;0;0;False;0;f6d5eedac2bfd3a4791d9359ac27eee2;f6d5eedac2bfd3a4791d9359ac27eee2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;41;915.5323,-463.421;Float;False;Property;_OffsetGradientStrength;OffsetGradientStrength;5;0;Create;True;0;0;False;0;0.7;0.7;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;40;1286.372,-559.187;Float;False;Constant;_Float1;Float 1;1;0;Create;True;0;0;False;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;42;1210.17,-461.4531;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;43;1762.297,-857.7466;Float;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosVertexDataNode;32;-1031.522,204.6673;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;7;-165.0348,-2.238979;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;61;1934.711,736.0916;Float;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;38;1225.889,-777.1782;Float;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;69;1912.763,390.1838;Float;False;Constant;_Vector0;Vector 0;5;0;Create;True;0;0;False;0;0,-1,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.BreakToComponentsNode;3;-1040.952,382.5522;Float;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;65;2358.145,1181.786;Float;False;Constant;_GravityFixedRoots;GravityFixedRoots;5;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;44;1996.079,-847.7681;Float;False;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;33;-722.5216,379.6673;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.NegateNode;70;2152.761,422.1838;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;39;1588.084,-625.2297;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;26;-268.4805,-212.3678;Float;False;Property;_EffectRadius;EffectRadius;1;0;Create;True;0;0;False;0;1;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;2140.88,-537.993;Float;False;Property;_OffsetFixedRoots;OffsetFixedRoots;6;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;27;45.16916,-62.47097;Float;True;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;62;2231.842,851.5258;Float;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;394.8223,47.47458;Float;False;Property;_EffectClampMax;EffectClampMax;2;0;Create;True;0;0;False;0;1;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;64;2707.82,1073.797;Float;False;Constant;_Float4;Float 4;5;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;66;2709.535,1178.358;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;34;-509.5216,280.6673;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.BlendOpsNode;63;2592.976,814.9686;Float;True;Multiply;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;71;2328.76,390.1838;Float;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;73;2264.761,486.1838;Float;False;Constant;_GravityMultiplier;GravityMultiplier;5;0;Create;True;0;0;False;0;2;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;28;352.3043,-112.6811;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;45;2279.753,-826.3858;Float;False;ColorBurn;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;47;2315.88,-688.993;Float;False;Constant;_One;One;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;49;2434.88,-538.993;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;35;411.775,283.4606;Float;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;52;2573.761,-167.8464;Float;False;Property;_OffsetMultiplier;OffsetMultiplier;8;0;Create;True;0;0;False;0;1;1;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;67;2946.08,982.9503;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;46;2643.88,-704.993;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;72;2584.761,390.1838;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClampOpNode;30;718.2132,-102.2383;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;68;3240.905,1034.373;Float;False;YOffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;77;1587.665,547.7944;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;947.5526,15.40209;Float;True;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;50;2832.88,-538.993;Float;False;XZOffset;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;53;2887.638,-171.3176;Float;False;XZMultiplier;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;74;2872.761,406.1838;Float;False;YMultiplier;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;3154.058,-346.3542;Float;False;3;3;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;81;3650.894,-702.2999;Float;True;Property;_Roughness;Roughness;11;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;75;3517.271,607.3883;Float;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;78;3616.788,-1523.561;Float;True;Property;_Albedo;Albedo;9;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;83;3626.947,-1322.762;Float;True;Property;_Normal;Normal;12;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;57;1423.537,1077.272;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;58;1128.899,1075.304;Float;False;Property;_Float3;Float 3;7;0;Create;True;0;0;False;0;0.7;0.7;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;59;1439.256,761.5461;Float;True;True;True;True;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;60;1105.48,757.519;Float;True;Property;_GravityGradient;GravityGradient;4;0;Create;True;0;0;False;0;f6d5eedac2bfd3a4791d9359ac27eee2;f6d5eedac2bfd3a4791d9359ac27eee2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;82;4064.178,-520.1036;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;55;1499.739,979.5375;Float;False;Constant;_Float2;Float 2;1;0;Create;True;0;0;False;0;1;0;1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;79;3660.691,-1112.245;Float;True;Property;_Metallic;Metallic;10;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;54;3829.615,-202.519;Float;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;56;1767.451,885.4946;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;4593.245,-494.1074;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;BendyGrass;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Translucent;0.5;True;True;0;False;Opaque;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;2;0;1;0
WireConnection;5;0;2;1
WireConnection;11;0;8;0
WireConnection;11;1;10;0
WireConnection;17;0;16;2
WireConnection;12;0;11;0
WireConnection;6;0;4;2
WireConnection;14;0;9;0
WireConnection;14;1;13;0
WireConnection;15;0;14;0
WireConnection;21;0;18;0
WireConnection;21;1;19;0
WireConnection;21;2;19;0
WireConnection;21;3;20;0
WireConnection;22;0;18;0
WireConnection;22;1;23;0
WireConnection;22;2;23;0
WireConnection;22;3;21;0
WireConnection;24;0;22;0
WireConnection;42;0;41;0
WireConnection;7;0;2;0
WireConnection;7;1;25;0
WireConnection;7;2;2;2
WireConnection;38;0;37;1
WireConnection;3;0;1;0
WireConnection;44;0;43;2
WireConnection;33;0;3;0
WireConnection;33;1;32;2
WireConnection;33;2;3;2
WireConnection;70;0;69;0
WireConnection;39;0;38;0
WireConnection;39;1;40;0
WireConnection;39;2;42;0
WireConnection;27;0;4;0
WireConnection;27;1;7;0
WireConnection;62;0;61;2
WireConnection;66;0;65;0
WireConnection;34;0;32;0
WireConnection;34;1;33;0
WireConnection;63;0;62;0
WireConnection;63;1;62;0
WireConnection;71;0;70;0
WireConnection;28;0;26;0
WireConnection;28;1;27;0
WireConnection;45;0;44;0
WireConnection;45;1;39;0
WireConnection;49;0;48;0
WireConnection;35;0;34;0
WireConnection;67;0;63;0
WireConnection;67;1;64;0
WireConnection;67;2;66;0
WireConnection;46;0;45;0
WireConnection;46;1;47;0
WireConnection;46;2;49;0
WireConnection;72;0;71;0
WireConnection;72;1;73;0
WireConnection;30;0;28;0
WireConnection;30;2;31;0
WireConnection;68;0;67;0
WireConnection;77;0;30;0
WireConnection;36;0;30;0
WireConnection;36;1;35;0
WireConnection;50;0;46;0
WireConnection;53;0;52;0
WireConnection;74;0;72;0
WireConnection;51;0;50;0
WireConnection;51;1;36;0
WireConnection;51;2;53;0
WireConnection;75;0;74;0
WireConnection;75;1;77;0
WireConnection;75;2;68;0
WireConnection;57;0;58;0
WireConnection;59;0;60;1
WireConnection;82;0;81;0
WireConnection;54;0;51;0
WireConnection;54;1;75;0
WireConnection;56;0;59;0
WireConnection;56;1;55;0
WireConnection;56;2;57;0
WireConnection;0;0;78;0
WireConnection;0;1;83;0
WireConnection;0;3;79;0
WireConnection;0;4;82;0
WireConnection;0;9;78;4
WireConnection;0;11;54;0
ASEEND*/
//CHKSM=964738A37946744038E4C2AEBD81959D563FFC1F