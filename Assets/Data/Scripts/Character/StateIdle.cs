﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateIdle : CharacterState
{
    public override void ToIdle(){}
    public override void ToLocomotion()
    {
        myMaster.ChangeStates(Character.StateName.move);
    }
    public override void ToJump()
    {
        myMaster.ChangeStates(Character.StateName.jump);
    }
    public override void ToFalling()
    {
        myMaster.ChangeStates(Character.StateName.falling);
    }
    public override void ToDoubleJump(){}
    public override void ToGlide()
    {
        myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_RUN, false);
        myMaster.isGliding = true;
        myMaster.animator.SetBool("isGliding", true);
        myMaster.startGlideFXGO = PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_PollenStart_01, myMaster.tr.position, 1);
        myMaster.ChangeStates(Character.StateName.glide);
    }
    public override void ToDash()
    {
        myMaster.ChangeStates(Character.StateName.dash);
    }
    public override void ToPushPull()
    {
        myMaster.ChangeStates(Character.StateName.pushPull);
    }
    public override void ToRolling()
    {
        myMaster.ChangeStates(Character.StateName.roll);
    }

    public override void RollInput()
    {
        myMaster.isCleansing = false;
        myMaster.layerCleanse = myMaster.animator.GetLayerIndex("Cleanse");
        myMaster.animator.SetLayerWeight(myMaster.layerCleanse, 0f);
        myMaster.isRolling = true;
        myMaster.animator.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.controller.height = 1;
        myMaster.controller.center = myMaster.cutCenter;
        myMaster.collider.height = 1;
        myMaster.collider.center = myMaster.cutCenter;
        myMaster.ApplyRotation();
        ToRolling();
    }
    public override void DashInput()
    {
        if (!myMaster.lockDash && !myMaster.isDashing && myMaster.canDash)
        {
            myMaster.lastTimeDash = Time.frameCount;
            myMaster.animator.SetTrigger(ANIM_PARAM_DASH);
            myMaster.isDashing = true;
            ToDash();
        }
    }
    public override void CancelButton(){}
    public override void InteractInput()
    {
        myMaster.isGrabbing = true;
        myMaster.animator.SetBool(ANIM_PARAM_GRABBING, true);
        ToPushPull();
    }
    public override void JumpInput()
    {
        if (InputManager.instance.leftStick != Vector3.zero)
        {
            myMaster.isJumping = true;
            myMaster.animator.SetTrigger(ANIM_PARAM_JUMP);
            myMaster.onGround = false;
            myMaster.velocityY = myMaster.impulsionJump;
            myMaster.velocityZ = myMaster.impulsionForwardJump;
            myMaster.ApplyRotation();
            ToJump();
        }
        else
        {
            myMaster.isJumping = true;
            myMaster.animator.SetTrigger(ANIM_PARAM_JUMP);
            myMaster.onGround = false;
            myMaster.velocityY = myMaster.impulsionJump;
            myMaster.velocityZ = 0;
            myMaster.ApplyRotation();
            ToJump();
        }
    }
    public override void MoveInput()
    {
        if (InputManager.instance.rolling)
        {
            ToRolling();
        }
        else
        {
            if (InputManager.instance.leftStick != Vector3.zero)
            {
                myMaster.ApplyRotation();
                myMaster.animator.SetBool(ANIM_PARAM_RUN, true);
                ToLocomotion();
            }
            else
                myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
        }
    }
    public StateIdle(Character myMaster) : base(myMaster) { }
}
