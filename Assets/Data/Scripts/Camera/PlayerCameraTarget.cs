﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraTarget : MonoBehaviour
{
    [HideInInspector] public Transform tr;
    [HideInInspector] public PlayerCamera playerCamera;
    private float avatarHeight;
    [HideInInspector] public float verticalDolly;

    private float heightAdjustment = .8f;
    private Vector3 forwardOffset;
    private float forwardOffsetVal = 0.6f;
    private Vector3 yOffset;
    private Vector3 targetSV;

    private void Start()
    {
        tr = transform;
    }
   
    public void Init()
    {
        avatarHeight = GameManager.instance.currentAvatar.GetComponent<CharacterController>().height;
        tr.position = new Vector3(0.0f, avatarHeight, 0.0f);
    }

    public void LateUpdate()
    {
        if (GameManager.instance.currentAvatar.gameObject.activeInHierarchy)
        {
            tr.rotation = GameManager.instance.currentAvatar.tr.rotation;
            forwardOffset = tr.forward * forwardOffsetVal;
            yOffset.y = avatarHeight + heightAdjustment + verticalDolly;
            tr.position = Vector3.SmoothDamp
                (tr.position, GameManager.instance.currentAvatar.animator.rootPosition + forwardOffset + yOffset, ref targetSV, 0.05f);
        }
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, 0.05f);
        }
    }
#endif
}
