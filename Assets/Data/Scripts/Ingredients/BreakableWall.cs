﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableWall : MonoBehaviour
{
    Transform tr;
    //private float distance;
    //private Vector3 currentPosition;
    //private Vector3 wallPosition;
    private Vector3 offset = new Vector3 (-2,0,-2);
    
    private Animator anim;
    private float timer = 1.8f;
    private Collider coli;
    AudioSource sound;
    public GameObject boomFxGO;

    void Start()
    {
        tr = transform;
        anim = GetComponentInChildren<Animator>();
        coli = GetComponentInChildren<Collider>();
        sound = GetComponentInChildren<AudioSource>();
        boomFxGO = PoolingManager.instance.GetObject("FX_Wall_BreakEffect_01", tr.position);
        boomFxGO.SetActive(false);
        boomFxGO.transform.position = tr.position + offset;
    }
    void Update()
    {
        /*if(GameManager.instance.currentAvatar.isDashing == true)
        {
            distance = Vector3.Distance(currentPosition, wallPosition);
            currentPosition = GameManager.instance.currentAvatar.transform.position;
            wallPosition = tr.position;
        }*/

        if (GameManager.instance.currentAvatar && GameManager.instance.currentAvatar.isDashing == true && GameManager.instance.currentAvatar.hitForward.collider == coli)
        {
            // sound
            boomFxGO.SetActive(true);
            anim.SetTrigger("isDashed");
            coli.enabled = false;
            Invoke("clear", timer);

            if (!sound.isPlaying)
            {
                sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_BreakableWall"));
            }
        }
    }

    void clear()
    {
        gameObject.SetActive(false);
        boomFxGO.SetActive(false);
    }
}
