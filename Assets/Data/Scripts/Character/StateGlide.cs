﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateGlide : CharacterState
{
    private float rotSpeedX = 60f;
    private float gravity = -100f;
    private Vector3 moveVector;
    public GameObject idleFxGO;
    public GameObject endFxGO;

    public override void ToIdle()
    {
        if (myMaster.onGround && !myMaster.isGliding)
        {
            PoolingManager.instance.ReturnObject(PoolConst.FX_Env_PollenCycle_01, myMaster.idleGlideFxGO);
            PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_Pollen_Desintegrate_01, myMaster.tr.position, 1);
            myMaster.hasSpawnPollen = !myMaster.hasSpawnPollen;
            myMaster.isGliding = false;
            myMaster.animator.SetBool("isGliding", false);
            myMaster.ChangeStates(Character.StateName.idle);
        }
    }
    public override void ToLocomotion()
    {
        if (myMaster.onGround && !myMaster.isGliding)
        {
            PoolingManager.instance.ReturnObject(PoolConst.FX_Env_PollenCycle_01, myMaster.idleGlideFxGO);
            PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_Pollen_Desintegrate_01, myMaster.tr.position, 1);
            myMaster.hasSpawnPollen = !myMaster.hasSpawnPollen;
            myMaster.animator.SetBool("isGliding", false);
            myMaster.ChangeStates(Character.StateName.move);
        }
    }
    public override void ToJump() { }
    public override void ToFalling()
    {
        if (!myMaster.isGliding)
        {
            PoolingManager.instance.ReturnObject(PoolConst.FX_Env_PollenCycle_01, myMaster.idleGlideFxGO);
            PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_Pollen_Desintegrate_01, myMaster.tr.position, 1);
            myMaster.hasSpawnPollen = !myMaster.hasSpawnPollen;
            myMaster.animator.SetBool("isGliding", false);
            myMaster.ChangeStates(Character.StateName.falling);
        }
    }
    public override void ToDoubleJump()
    {
        myMaster.ChangeStates(Character.StateName.doubleJump);
    }
    public override void ToGlide()
    {
        myMaster.startGlideFXGO.transform.position = myMaster.tr.position;
        myMaster.idleGlideFxGO.transform.position= myMaster.tr.position;
        myMaster.animator.SetBool("isGliding", true);
    }
    public override void ToDash(){}
    public override void ToPushPull(){}
    public override void ToRolling()
    {
        myMaster.ChangeStates(Character.StateName.roll);
    }

    public override void RollInput()
    {
        PoolingManager.instance.ReturnObject(PoolConst.FX_Env_PollenCycle_01, myMaster.idleGlideFxGO);
        PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_Pollen_Desintegrate_01, myMaster.tr.position, 1);
        myMaster.hasSpawnPollen = !myMaster.hasSpawnPollen;
        myMaster.isGliding = false;
        myMaster.animator.SetBool("isGliding", false);
        myMaster.isCleansing = false;
        myMaster.layerCleanse = myMaster.animator.GetLayerIndex("Cleanse");
        myMaster.animator.SetLayerWeight(myMaster.layerCleanse, 0f);
        myMaster.isRolling = true;
        myMaster.animator.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.controller.height = 1;
        myMaster.controller.center = myMaster.cutCenter;
        myMaster.collider.height = 1;
        myMaster.collider.center = myMaster.cutCenter;
        myMaster.ApplyRotation();
        myMaster.ApplyGravity();
        ToRolling();
    }
    public override void DashInput(){}
    public override void CancelButton()
    {
        ToFalling();
    }
    public override void InteractInput() { }
    public override void JumpInput()
    {
        if (myMaster.canDoubleJump && !myMaster.lockDoubleJump)
        {
            PoolingManager.instance.ReturnObject(PoolConst.FX_Env_PollenCycle_01, myMaster.idleGlideFxGO);
            PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_Pollen_Desintegrate_01, myMaster.tr.position, 1);
            myMaster.hasSpawnPollen = !myMaster.hasSpawnPollen;
            myMaster.isGliding = false;
            myMaster.animator.SetBool("isGliding", false);
            myMaster.animator.SetTrigger(ANIM_PARAM_DOUBLEJUMP);
            myMaster.velocityY = 0;
            myMaster.velocityY = myMaster.impulsionDoubleJump;
            if (InputManager.instance.moving)
                myMaster.velocityZ = myMaster.impulsionForwardJump / 1.3f;
            myMaster.canDoubleJump = false;
            ToDoubleJump();
        }
    }
    public override void MoveInput()
    {
        if (InputManager.instance.leftStick != Vector3.zero && myMaster.isGliding)
        {
            Vector3 moveVector = GameManager.instance.currentAvatar.transform.forward;
            Vector3 inputs = InputManager.instance.leftStick;
            Vector3 yaw = inputs.x * GameManager.instance.currentAvatar.transform.right * rotSpeedX * Time.deltaTime * 2f;
            Vector3 dir = yaw;
            GameManager.instance.currentAvatar.transform.Rotate(0, inputs.x * rotSpeedX * Time.deltaTime, 0, Space.Self);
            float maxX = GameManager.instance.currentAvatar.angle + GameManager.instance.currentAvatar.currentCameraTr.eulerAngles.y;

            if (maxX < 90 && maxX > 70 || maxX > 270 && maxX < 290)
            { }
            else
            {
                moveVector.x += dir.x;
                //moveVector.z += dir.z;
            }

            GameManager.instance.currentAvatar.controller.Move(moveVector * Time.deltaTime);
        }
        if(!myMaster.isGliding)
            ToLocomotion();
    }
    public StateGlide(Character myMaster) : base(myMaster) { }

}
