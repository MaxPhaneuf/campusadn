﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Panel : MonoBehaviour
{    
    
    public bool selected = false;    
    public Button firstButton;
    
    public void FixedUpdate()
    {
        if (firstButton != null && gameObject.activeInHierarchy && !selected)
            SelectButton();        
    }  

    void SelectButton()
    {        
        firstButton.Select();
        selected = true;
    }

    public void UnSelectPanel()
    {
        selected = false;
    }
}
