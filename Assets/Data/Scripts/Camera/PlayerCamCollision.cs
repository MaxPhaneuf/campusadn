﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamCollision : IStates
{
    private PlayerCamera user;
    private float hitPointDist;
    private Vector3 collSV;

    public PlayerCamCollision(PlayerCamera user)
    {
        this.user = user;
    }

    public void Enter()
    {

    }

    public void Execute()
    {
        if (user.isColliding)
        {
            if (InputManager.instance.resetCam) user.stateMachine.ChangeState(user.playerCamResetState);

            collSV = Vector3.zero;
            hitPointDist = Vector3.Distance(user.hit.point, user.camTarget.tr.position);
            user.tr.position = Vector3.SmoothDamp(user.tr.position, user.camTarget.tr.position - user.tr.forward * hitPointDist, ref collSV, 0.025f);
            user.UpdateState();
        }
        else user.stateMachine.ChangeState(user.playerCamDefaultState);
    }

    public void Exit()
    {

    }
}
