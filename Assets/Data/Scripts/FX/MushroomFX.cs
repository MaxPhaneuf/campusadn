﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomFX : MonoBehaviour
{
    private Mushroom parentScript;
    private Transform parent;
    private GameObject currentFX;
    private Vector3 offset;

    private void Start()
    {
        parentScript = GetComponentInParent<Mushroom>();
        parent = GetComponentInParent<Transform>();
    }

    private void LateUpdate()
    {
       
        if (currentFX != null) currentFX.transform.position = parent.position + offset;
    }
    public void PlayFX(string name)
    {
        offset = new Vector3(0, 0.9f, 0);
        GameObject currentFX = PoolingManager.instance.GetObjectAutoReturn(name, transform.position + offset, 1.5f);
        parentScript.idleFxGO.SetActive(false);
        currentFX.transform.SetParent(parent);
    }

    public void ResetIdleFX()
    {
        parentScript.idleFxGO.SetActive(true);
    }
}
