﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class NMEStateMachine : MonoBehaviour
{
    public const float TIME_HURT = 3;
    public const string AGENT_NAME = "Agent";

    public float visionRadius = 5;
    public float attack1Range = 2;
    public float attack2Range = 2;
    public float startCorruption;
    float castIncrement = .05f;
    [HideInInspector]
    public NMEFollow follow;
    [HideInInspector]
    public float timeInLastState;
    [HideInInspector]
    public float currentCorruption;
    [HideInInspector]
    public NavMeshAgent agent;
    protected Animator anim;
    public enum State { Idle, Patrol, Alert, Pursuit, Attack1, Attack2, Hurt, Cleansing, WakeUp, Special1, Special2 }
    public State Current;
    
    protected NMEState current;
    Dictionary<State, NMEState> states = new Dictionary<State, NMEState>();
    [HideInInspector]
    public UnityEvent OnHurt = new UnityEvent();
    [HideInInspector]
    public UnityEvent OnCleansingStart = new UnityEvent();
    [HideInInspector]
    public UnityEvent OnWakeUp = new UnityEvent();
    [HideInInspector]
    public bool isHurt;

    protected void Init()
    {
        InitStates();
        InitComponents();
        InitEvents();

        SetState(State.Idle);
    }

    protected virtual void InitComponents()
    {
        GetComponent<NMEStateListener>().Init(this);
        follow = GetComponent<NMEFollow>();
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    protected void InitEvents()
    {
        OnHurt.AddListener(() => ChangeState(State.Hurt));
        OnCleansingStart.AddListener(() => ChangeState(State.Cleansing));
        currentCorruption = startCorruption;
    }
       
    protected virtual void InitStates()
    {
        InitState(State.Idle, new NMEIdle());
        InitState(State.Patrol, new NMEPatrol());
        InitState(State.Alert, new NMEAlert());
        InitState(State.Hurt, new NMEHurt());
        InitState(State.Cleansing, new NMECleansing());
    }

    protected void InitState(State current, NMEState state)
    {
        state?.Init(this);
        states.Add(current, state);
    }

    protected void SetState(State state)
    {
        current = states[state];
        current.Start();
        Current = state;
    }

    public void ChangeState(State state)
    {
        timeInLastState = current.timeInState;
        current?.End();
        SetState(state);
    }

    public NMEState GetState(State state) { return states[state]; }

    public void RemoveCorruption(float value)
    {
        currentCorruption -= value;
    }
       
    public void SetAgentSpeedWithAnim()
    {
        if (agent && anim)
            agent.speed = anim.deltaPosition.magnitude / Time.deltaTime;
    }

    public class UnityFloatEvent : UnityEvent<float> { }

}
