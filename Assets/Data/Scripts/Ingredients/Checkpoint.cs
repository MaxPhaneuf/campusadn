﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private Collider collider;

    private void Start()
    {
        collider = GetComponentInChildren<Collider>();
    }
    public void ActivateCheckpoint()
    {
        //mettre FX et autres trucs a sauvegarder avec le checkpoint
        GameManager.instance.currentAvatar.startingPosition = GameManager.instance.currentAvatar.tr.position;
    }
}
