﻿using UnityEngine;

public class NMEOursPursuit : NMEPursuit
{

    public override void Update()
    {
        base.Update();

        if (Vector3.Distance(master.transform.position, GameManager.instance.currentAvatar.tr.position) <= master.attack2Range)
        {
            if (Vector3.Distance(master.transform.position, GameManager.instance.currentAvatar.tr.position) <= master.attack1Range)
                master.ChangeState(NMEStateMachine.State.Attack1);
            else 
                master.ChangeState(NMEStateMachine.State.Attack2);
        }

    }
}
