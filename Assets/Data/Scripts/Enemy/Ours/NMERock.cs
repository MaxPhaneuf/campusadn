﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMERock : MonoBehaviour
{
    public float returnTimer = 5f;
    float timer;

    public void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            timer += Time.deltaTime;
            if(timer >= returnTimer)
                PoolingManager.instance.ReturnObject(PoolConst.OBJ_NMEvOur_Rock, gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player" || collision.transform.tag == "Ground")
        {
            if (collision.transform.tag == "Player")
                GameManager.instance.currentAvatar.GettingKnocked();
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            PoolingManager.instance.ReturnObject(PoolConst.OBJ_NMEvOur_Rock, gameObject);
            timer = 0;
        }
    }
}
