﻿// Création du script : Felix Desrosiers-Dorval
// Date : 02-04-2019 16:15

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public IStates currentState;
    public IStates previousState;

    public void ChangeState(IStates newState)
    {
        if (currentState != null)
        {
            currentState.Exit();
            previousState = currentState;
        }

        currentState = newState;
        currentState.Enter();
    }

    public void CurrentStateUpdate()
    {
        if (currentState != null)
            currentState.Execute();
    }

    public void SwitchToPreviousState()
    {
        currentState.Exit();
        currentState = previousState;
        currentState.Enter();
    }
}
