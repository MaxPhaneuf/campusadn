﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamDefault : IStates
{
    private PlayerCamera user;
    private Vector3 dollySV;

    public PlayerCamDefault(PlayerCamera user)
    {
        this.user = user;
    }

    public void Enter()
    {

    }

    public void Execute()
    {
        if (user && InputManager.instance && InputManager.instance.resetCam) user.stateMachine.ChangeState(user.playerCamResetState);

        dollySV = Vector3.zero;
        user.tr.position = Vector3.SmoothDamp(user.tr.position, user.camTarget.tr.position - user.tr.forward * user.desiredDollyDst, ref dollySV, 0.015f);
        user.UpdateState();

        if (user.isColliding) user.stateMachine.ChangeState(user.playerCamCollisionState);
    }

    public void Exit()
    {

    }
}