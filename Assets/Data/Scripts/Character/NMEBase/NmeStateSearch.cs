﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NmeStateSearch : NmeStateMachine
{

    public override void Idle()
    {

    }
    public override void Chase()
    {
        myMaster.anim.SetBool("move", true);
        myMaster.ChangeState(NmeController.NmeStates.CHASE);
    }
    public override void Search()
    {
        myMaster.ChangeState(NmeController.NmeStates.SEARCH);
    }
    public override void Defend()
    {
        myMaster.ChangeState(NmeController.NmeStates.DEFEND);
    }
    public override void Patrol()
    {
        myMaster.anim.SetBool("move", true);
        myMaster.ChangeState(NmeController.NmeStates.PATROL);
    }
    public override void Alert()
    {
        throw new System.NotImplementedException();
    }
    public override void Hurt()
    {
        throw new System.NotImplementedException();
    }



    public override void UpdateState()
    {
        throw new System.NotImplementedException();
    }

    public NmeStateSearch(NmeController myMaster) : base(myMaster) { }


}
