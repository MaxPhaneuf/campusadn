﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateDash : CharacterState
{
    public override void ToIdle()
    {
        myMaster.ChangeStates(Character.StateName.idle);
    }
    public override void ToLocomotion()
    {
        myMaster.ChangeStates(Character.StateName.move);
    }
    public override void ToJump()
    {
        myMaster.ChangeStates(Character.StateName.jump);
    }
    public override void ToFalling()
    {
        myMaster.ChangeStates(Character.StateName.falling);
    }
    public override void ToDoubleJump()
    {
        myMaster.ChangeStates(Character.StateName.doubleJump);
    }
    public override void ToGlide()
    {
        myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_RUN, false);
        myMaster.isGliding = true;
        myMaster.animator.SetBool("isGliding", true);
        myMaster.startGlideFXGO = PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_PollenStart_01, myMaster.tr.position, 1);
        myMaster.ChangeStates(Character.StateName.glide);
    }
    public override void ToDash(){}
    public override void ToPushPull(){}
    public override void ToRolling(){}

    public override void RollInput(){}
    public override void DashInput(){}
    public override void CancelButton(){}
    public override void InteractInput(){}
    public override void JumpInput()
    {
        if (myMaster.onGround && InputManager.instance.leftStick != Vector3.zero)
        {
            myMaster.isJumping = true;
            myMaster.onGround = false;
            myMaster.velocityY = myMaster.impulsionJump;
            myMaster.velocityZ = myMaster.impulsionForwardJump;
            myMaster.animator.ResetTrigger(ANIM_PARAM_JUMP);
            ToJump();
        }
        if (myMaster.onGround && InputManager.instance.leftStick == Vector3.zero)
        {
            myMaster.isJumping = true;
            myMaster.onGround = false;
            myMaster.velocityY = myMaster.impulsionJump;
            myMaster.animator.ResetTrigger(ANIM_PARAM_JUMP);
            ToJump();
        }

        if (!myMaster.isDashing && myMaster.canDoubleJump && !myMaster.onGround && !myMaster.lockDoubleJump)
        {
            myMaster.animator.SetTrigger(ANIM_PARAM_DOUBLEJUMP);
            myMaster.velocityY = myMaster.impulsionDoubleJump;
            if (InputManager.instance.moving)
                myMaster.velocityZ = myMaster.impulsionForwardJump / 1.3f;
            myMaster.canDoubleJump = false;
            myMaster.animator.ResetTrigger(ANIM_PARAM_DOUBLEJUMP);
            ToDoubleJump();
        }
    }
    public override void MoveInput()
    {
        if (!myMaster.isDashing && InputManager.instance.leftStick != Vector3.zero)
        {
            myMaster.ApplyRotation();
            myMaster.animator.SetBool(ANIM_PARAM_RUN, true);
            ToLocomotion();
        }
    }
    public StateDash(Character myMaster) : base(myMaster) { }
}
