﻿// Création du script : Catherine Demeule
// Date : 03-04-2019 16:30 
// Modification : Catherine Demeule
// Date de modification : 03-04 16:31 


//using System.Collections;
//using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class DataSave : MonoBehaviour
{
    //TODO mettre les variables à sauvegarder


    /// <summary>
    /// Constructeur qui permet d'utiliser les valeurs de sauvegarde
    /// </summary>
    public DataSave() { }

    /// <summary>
    /// Create Folder for Save Game, to put in awake/start
    /// </summary>
    static void CreateFolder()
    {
        // Specifier un chemin pour envoyer le dossier
        string incompPath = Application.dataPath + "/Data";
        // Creer un string avec un nom quelconque qui sera envoye dans le chemin
        string pathString = System.IO.Path.Combine(incompPath, "SaveGame");
        //creer l'entierete du path avec le nouveau folder
        System.IO.Directory.CreateDirectory(pathString);
        //S'assure que le dossier existe pas encore avant de le creer
        if (!System.IO.File.Exists(pathString))
        {
            using (System.IO.FileStream fs = System.IO.File.Create(pathString))
            {
                for (byte i = 0; i < 100; i++)
                {
                    fs.WriteByte(i);
                }
            }
        }
        else
        {
            return;
        }
    }
}
