﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class modulesLD : MonoBehaviour
{
    public Renderer rd;
    
    // Start is called before the first frame update
    void Start()
    {
        rd = GetComponentInChildren<Renderer>();
        GameManager.instance.DeactivateRD.AddListener(DeactivateRD);
    }

    void DeactivateRD()
    {
        rd.enabled = !rd.enabled;
    }
}
