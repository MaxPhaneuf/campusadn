﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleLore : MonoBehaviour
{
    public bool hasTaken = false;
    Collider coli;
    Transform tr;
    AudioSource sound;

    // Start is called before the first frame update
    void Start()
    {
        tr = transform;
        sound = GetComponentInChildren<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //GameManager.instance.levelManager.loreCount--;
            gameObject.SetActive(false);
            sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_CollectibleLoreGet"));
        }
    }
}
