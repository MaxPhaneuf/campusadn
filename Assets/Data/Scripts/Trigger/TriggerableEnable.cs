﻿/*Script cree le : 11-04-2019
 * Par: Steven Judge 
 * Derniere modification:22-04-2019
 * Par:Steven Judge 
 * Description: trigger pour faire disparaitre ou apparaitre un Renderer et un Collider
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerableEnable : Triggerable
{
    private Renderer render;
    private Collider coli;

    void Start()
    {
        render = GetComponent<Renderer>();
        coli = GetComponent<Collider>();
    }

    public override void Activate()
    {
        if (state != 1)
        {
            render.enabled = !render.enabled;
            coli.enabled = !coli.enabled;
        }
        state = 1;
    }

    public override void Deactivate()
    {
        if (state != 2)
        {
            render.enabled = !render.enabled;
            coli.enabled = !coli.enabled;
        }
        state = 2;
    }

    public override void Toggle()
    {
        startActivated = !startActivated;
        if (startActivated) Activate();
        else Deactivate();
    }




}