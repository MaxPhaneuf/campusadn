﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFalling : CharacterState
{
    public override void ToIdle()
    {
        myMaster.ChangeStates(Character.StateName.idle);
    }
    public override void ToLocomotion()
    {
        myMaster.ChangeStates(Character.StateName.move);
    }
    public override void ToJump(){}
    public override void ToFalling(){}
    public override void ToDoubleJump()
    {
        if (!InputManager.instance.rolling)
            myMaster.ChangeStates(Character.StateName.doubleJump);
    }
    public override void ToGlide()
    {
        myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_RUN, false);
        myMaster.isGliding = true;
        myMaster.animator.SetBool("isGliding", true);
        myMaster.startGlideFXGO = PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_PollenStart_01, myMaster.tr.position, 1);
        myMaster.ChangeStates(Character.StateName.glide);
    }
    public override void ToDash()
    {
        myMaster.ChangeStates(Character.StateName.dash);
    }
    public override void ToPushPull(){}
    public override void ToRolling()
    {
        myMaster.ChangeStates(Character.StateName.roll);
    }

    public override void RollInput()
    {
        myMaster.animator.SetBool(ANIM_PARAM_FALL, false);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_FALL, true);
        myMaster.isCleansing = false;
        myMaster.layerCleanse = myMaster.animator.GetLayerIndex("Cleanse");
        myMaster.animator.SetLayerWeight(myMaster.layerCleanse, 0f);
        myMaster.isRolling = true;
        myMaster.animator.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.animatorRoll.SetBool(ANIM_PARAM_ROLL, true);
        myMaster.controller.height = 1;
        myMaster.controller.center = myMaster.cutCenter;
        myMaster.collider.height = 1;
        myMaster.collider.center = myMaster.cutCenter;
        myMaster.ApplyRotation();
        ToRolling();
    }
    public override void DashInput()
    {
        if (!myMaster.lockDash && !myMaster.isDashing && myMaster.canDash)
        {
            myMaster.animatorRoll.SetBool(ANIM_PARAM_RUN, false);
            myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
            myMaster.lastTimeDash = Time.frameCount;
            myMaster.animator.SetTrigger(ANIM_PARAM_DASH);
            myMaster.isDashing = true;
            ToDash();
        }
    }
    public override void CancelButton(){}
    public override void InteractInput(){}
    public override void JumpInput()
    {
        if (myMaster.canDoubleJump && !myMaster.onGround && !myMaster.lockDoubleJump && InputManager.instance.leftStick != Vector3.zero)
        {
            myMaster.animator.SetTrigger(ANIM_PARAM_DOUBLEJUMP);
            myMaster.velocityY = 0;
            myMaster.velocityY = myMaster.impulsionDoubleJump;
            if (InputManager.instance.moving)
                myMaster.velocityZ = myMaster.impulsionForwardJump / 1.3f;
            myMaster.canDoubleJump = false;
            ToDoubleJump();
        }
        else
        {
            if (myMaster.canDoubleJump && !myMaster.onGround && !myMaster.lockDoubleJump && InputManager.instance.leftStick == Vector3.zero)
            {
                myMaster.animator.SetTrigger(ANIM_PARAM_DOUBLEJUMP);
                myMaster.velocityY = 0;
                myMaster.velocityY = myMaster.impulsionDoubleJump;
                if (InputManager.instance.moving)
                    myMaster.velocityZ = 0;
                myMaster.canDoubleJump = false;
                ToDoubleJump();
            }
        }
    }
    public override void MoveInput()
    {
        if (!myMaster.onGround)
        {
            if (InputManager.instance.leftStick != Vector3.zero)
            {
                myMaster.velocityZ = myMaster.impulsionForwardJump;
                myMaster.ApplyRotation();
            }
        }
        else
        {
            if (InputManager.instance.leftStick != Vector3.zero)
            {
                myMaster.ApplyRotation();
                myMaster.animator.SetBool(ANIM_PARAM_RUN, true);
                ToLocomotion();
            }
            else
                myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
        }
        if (InputManager.instance.leftStick == Vector3.zero)
            myMaster.animator.SetBool(ANIM_PARAM_RUN, false);
    }
    public StateFalling(Character myMaster) : base(myMaster) { }
}
