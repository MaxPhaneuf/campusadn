﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class NMEState
{
    public UnityEvent OnStart = new UnityEvent();
    public UnityEvent OnUpdate = new UnityEvent();
    public UnityEvent OnEnd = new UnityEvent();
    public UnityBoolEvent IsInRange = new UnityBoolEvent();

    protected NMEStateMachine master;
    public float timeInState;
    protected bool isPlayerInRange;

    public void Init(NMEStateMachine master) { this.master = master; }

    public virtual void Start()
    {
        timeInState = 0;
        OnStart.Invoke();
    }

    public virtual void Update()
    {
        timeInState += Time.deltaTime;
        OnUpdate.Invoke();
        isPlayerInRange = IsPlayerInRange();
        IsInRange.Invoke(isPlayerInRange);
    }

    public virtual void End() { OnEnd.Invoke(); }

    public bool IsPlayerInRange()
    {
        return Vector3.Distance(master.transform.position, GameManager.instance.currentAvatar.tr.position) <= master.visionRadius;
    }
        
    public class UnityBoolEvent : UnityEvent<bool> { }
}
