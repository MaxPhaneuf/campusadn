﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Fruits : MonoBehaviour
{
    public Transform tr;
    public Rigidbody rb;
    public float mass = 1f;
    // Start is called before the first frame update
    void Start()
    {
        tr = transform;
        rb = GetComponent<Rigidbody>();
        rb.mass = mass;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
