﻿/*Script cree le : 11-04-2019
 * Par: Steven Judge
 * Derniere modification:11-04-2019
 * Par:Steven Judge 
 * Description: 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TrigCam : Triggerable
{
    public enum TriggerFunction { Entree, Sortie }
    public TriggerFunction function;
    delegate void Function();
    Function currentFunction;

    public bool doOnce = true;
    public bool repeat = false;
    public bool playerAsControl = false;
    [HideInInspector]
    public CameraTriggerManager.ActiveCam SwitchTo, Default;
    public UnityEvent CameraEvent;

    public override void Activate()
    {
        if (doOnce == true)
        {
            //GameManager.instance.GetComponent<CameraTriggerManager>().ChangeCurrent(SwitchTo);
            CameraEvent.Invoke();
            doOnce = false;
            Repeat();
        }


    }

    public override void Deactivate()
    {
        if (doOnce == true)
        {
            //GameManager.instance.GetComponent<CameraTriggerManager>().ChangeCurrent(SwitchTo);
            CameraEvent.Invoke();
            doOnce = false;
            Repeat();
        }
    }

    public override void Toggle()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        TriggerEvent();
    }

    void TriggerEvent()
    {
        if (function == TriggerFunction.Entree)
            currentFunction = Activate;
        else if (function == TriggerFunction.Sortie)
            currentFunction = Deactivate;

    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Fonction qui vérifie si on voulait le trigger actif une seul fois ou plusieurs fois.
    /// </summary>
    public void Repeat()
    {
        if (repeat == true)
        {
            doOnce = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //GameManager.instance.GetComponent<CameraTriggerManager>().ChangeCurrent(Default);
    }

}