﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableWindPit : Triggerable
{
    // variables pour fonctionement
    public enum Depart { Open, Close };
    public Depart Status;
    private float force = 9f;
    private float forceGlide = 1.7f;
    private Transform tr;
    Vector3 currentPos;
    Vector3 pitPosotion;
    private bool activated = false;
    public bool open;

    RaycastHit posdown;
    RaycastHit posup;
    RaycastHit posleft;
    RaycastHit posright;

    // variable pour le deplacer

    Vector3 direction;
    public float distance;
    public float pushForce = 5f;
    RaycastHit ground;
    RaycastHit front;
    RaycastHit back;
    public bool grabbed = false;
    public bool release = false;
    public float cast = 2f;
    Collider coli;
    public float pos;
    Animator anim;
    Vector3 baseMotion;
    [HideInInspector]
    public GameObject idleFxGO;
    [HideInInspector]
    public GameObject idleFxGOH;
    public Vector3 offset;
    CapsuleCollider collCaps;
    BoxCollider collBox;
    AudioSource sound;

    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        tr = transform;
        pitPosotion = tr.position;
        collBox = GetComponentInChildren<BoxCollider>();
        collCaps = GetComponentInChildren<CapsuleCollider>();
        coli = GetComponent<Collider>();
        switch (Status)
        {
            case Depart.Open:
               activated = true;
                break;

            case Depart.Close:
                activated = false;
                break;
        }
        offset = new Vector3(0, 0.9f, 0);
        idleFxGO = PoolingManager.instance.GetObject("FX_Prop_PuitVent_5m_01", tr.position + offset);
        idleFxGOH = PoolingManager.instance.GetObject("FX_Prop_PuitVent_10m_01", tr.position + offset);
        if(open)
        idleFxGO.SetActive(true);
        else
            idleFxGO.SetActive(false);
        idleFxGOH.SetActive(false);

        sound = GetComponentInChildren<AudioSource>();
    }

    void Update()
    {
        Physics.Raycast(tr.position, tr.forward, out posright, 2f);
        Physics.Raycast(tr.position, -tr.forward, out posleft, 2f);
        Physics.Raycast(tr.position, -tr.right, out posup, 2f);
        Physics.Raycast(tr.position, tr.right, out posdown, 2f);

        if (posright.transform == GameManager.instance.currentAvatar.transform)
        {
            pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
            pos = 180;
        }

        if (posleft.transform == GameManager.instance.currentAvatar.transform)
        {
            pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
            pos = 0;
        }

        if (posup.transform == GameManager.instance.currentAvatar.transform)
        {
            pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
            pos = 90;
        }

        if (posdown.transform == GameManager.instance.currentAvatar.transform)
        {
            pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
            pos = 270;
        }

        if (open == true) anim.SetBool("OnEntered", true);
        if (open == false) anim.SetBool("OnEntered", false);
        distance = Vector3.Distance(GameManager.instance.currentAvatar.transform.position, tr.position);
        direction = GameManager.instance.currentAvatar.transform.forward;

        Physics.Raycast(tr.position, direction, out front, 1f);
        Physics.Raycast(tr.position + GameManager.instance.currentAvatar.transform.up, direction + Vector3.down, out ground, cast); //  LA VALEUR DU CAST DOIT ETRE MODIFIER QUAND ON VA AVOIR LE VRAI MODEL , POUR LE MOMENT AVEC LE CUBE ON LA MET A 2

        // system de grab

        if (GameManager.instance.currentAvatar.hitForward.collider == coli && distance <= 2f && InputManager.instance.interaction && InputManager.instance.leftStick.z == 0)
        {
            pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
            grabbed = true;
        }

        if (grabbed == true)
        {
            GameManager.instance.currentAvatar.transform.eulerAngles = new Vector3(0, pos, 0);
            GameManager.instance.currentAvatar.animator.SetBool("isGrabbing", true);

            if (InputManager.instance.leftStick.z == 0)
            {
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
            }

            // pousser

            if (InputManager.instance.leftStick.z > 0)
            {
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", true);

                if (ground.transform && !front.transform)
                {
                    tr.position = GameManager.instance.currentAvatar.transform.position + GameManager.instance.currentAvatar.transform.forward * 2f;  // A MODIFIER LE FLOAT POUR LE OFFSET EN AVANT DE L AVATAR
                }
            }

            // tirer
            if (InputManager.instance.leftStick.z < 0)
            {
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", true);
                tr.position = GameManager.instance.currentAvatar.transform.position + GameManager.instance.currentAvatar.transform.forward * 2f;
            }

            if (grabbed == false)
            {
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isGrabbing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
            }

            if (InputManager.instance.jumping)
            {
                grabbed = false;
                tr.position = tr.position;
                GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isGrabbing", false);
                GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
            }
        }
        if (open && !GameManager.instance.currentAvatar.isRolling)
        {
            collCaps.enabled = false;
            collBox.enabled = true;
            idleFxGO.SetActive(true);
        }
        else
        {
            if (open && GameManager.instance.currentAvatar.isRolling || open && GameManager.instance.currentAvatar.isGliding)
            {
                collCaps.enabled = true;
                collBox.enabled = false;
            }
        }
        if(!open)
        {
            idleFxGO.SetActive(false);
            idleFxGOH.SetActive(false);
        }
    }

    private void LateUpdate()
    {
        idleFxGO.transform.position = tr.position;
        idleFxGOH.transform.position = tr.position;
    }

    public override void Activate()
    {
        open = true;
    }

    public override void Deactivate()
    {
        open = false;
    }

    public override void Toggle()
    {
        open = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(activated == true)
        {
            if (other.transform.tag == "Platform")
            {
                Debug.Log("other object in range");
                activated = false;
                return;
            }
            currentPos = GameManager.instance.transform.position;

        }
        if (other.tag == "Player")
        {
            baseMotion = GameManager.instance.currentAvatar.velocity * Time.deltaTime;
            GameManager.instance.currentAvatar.velocityZ = 0;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(open == true)
        {
            if (other.transform.tag == "Platform")
            {
                activated = false;
                return;
            }

            if (other.transform.tag == "Player")
            {
                activated = true;
            }

            if (other.transform.tag == "Player" && activated)
            {
               /* if (GameManager.instance.currentAvatar.isGliding == true)
                {
                    this.GetComponent<CapsuleCollider>().enabled = true;
                }*/

                if (GameManager.instance.currentAvatar.isRolling == true)
                {
                  //  this.GetComponent<CapsuleCollider>().enabled = true;
                    GameManager.instance.currentAvatar.velocityY = force;
                    idleFxGOH.SetActive(true);
                    idleFxGO.SetActive(false);
                }
                else
                {
                   // this.GetComponent<CapsuleCollider>().enabled = false;
                    idleFxGO.SetActive(true);
                    idleFxGOH.SetActive(false);
                }

                if(!GameManager.instance.currentAvatar.isGliding)
                    GameManager.instance.currentAvatar.velocityY = force;
                else
                    GameManager.instance.currentAvatar.velocityY = forceGlide;
            }
        }

        if (!sound.isPlaying)
        {
            sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_WindPit"));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Platform")
        {
            activated = true;
            return;
        }

        /*if (GameManager.instance.currentAvatar.onGround == true)
        {
            this.GetComponent<BoxCollider>().enabled = true;
            this.GetComponent<CapsuleCollider>().enabled = false;
        }*/
        if(other.tag == "Player")
            GameManager.instance.currentAvatar.velocity = baseMotion;
    }
}
