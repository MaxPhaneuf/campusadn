﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMEHerRoll : NMEState
{
    NMEHerSM sm;
    float rollSpeed = 10f;
    NMEStateMachine.State nextState;
    public override void Start()
    {
        base.Start();
        sm = (NMEHerSM)master;
        sm.basicModel.SetActive(false);
        sm.ballModel.SetActive(true);
        master.agent.speed = rollSpeed;
        master.agent.enabled = true;
        master.agent.SetDestination(GameManager.instance.currentAvatar.tr.position);

    }

    public override void Update()
    {
        base.Update();
        if (Vector3.Distance(master.transform.position, GameManager.instance.currentAvatar.transform.position) <= master.attack1Range)
        {
            nextState = NMEStateMachine.State.Attack1;
            master.ChangeState(NMEStateMachine.State.Attack1);
        }

        if (!isPlayerInRange)
            master.ChangeState(NMEStateMachine.State.Alert);
        else
            master.agent.SetDestination(GameManager.instance.currentAvatar.tr.position);
    }

    public override void End()
    {
        base.End();
        if (nextState != NMEStateMachine.State.Attack1)
        {
            sm.basicModel.SetActive(true);
            sm.ballModel.SetActive(false);
        }
    }
}
