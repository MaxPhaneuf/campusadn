﻿// Création du script : Maxime Arcierie
// Date : 03-04-2019 11:00 
// Modification : Maxime
// Date de modification : 03-05-2019 11:00 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public int currentLevel;
    public CollectibleArt[] collectArt = new CollectibleArt[4];
    public CollectibleLore[] collectLore = new CollectibleLore[1];
    [HideInInspector]
    public int artCount = 4;
    [HideInInspector]
    public int loreCount = 1;
 
    private void Update()
    {
        if (artCount == 0)
            ArtUnlocked();
        if (loreCount == 0)
            LoreUnlocked();
    }

    void ChangeLevel()
    {

    }
    void ArtUnlocked()
    {

    }
    void LoreUnlocked()
    {

    }

}


