﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamReset : IStates
{
    private PlayerCamera user;
    private Vector3 resetSV;
    private int camResetTimer;
    private float valueToDesiredPitch;
    private float valueToDesiredYaw;

    public PlayerCamReset(PlayerCamera user)
    {
        this.user = user;
    }

    public void Enter()
    {
        Debug.Log("CamResetState Enter");
        InputManager.instance.gameplayInputsAreDisabled = true;
    }

    public void Execute()
    {
        camResetTimer--;

        user.pitch = Mathf.Lerp(user.pitch, 15, 0.10f);
        user.yaw = Mathf.LerpAngle(user.yaw, 0 + (user.camTarget.tr.rotation.eulerAngles.y), 0.10f);
        user.tr.position = Vector3.SmoothDamp(user.tr.position, user.camTarget.tr.position - user.tr.forward * user.desiredDollyDst, ref resetSV, 0.025f);
        user.UpdateState();
        if (camResetTimer <= 0) user.stateMachine.ChangeState(user.playerCamDefaultState);
    }

    public void Exit()
    {
        camResetTimer = 45;
        InputManager.instance.gameplayInputsAreDisabled = false;
        Debug.Log("CamResetState Exit");
    }
}
