﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableWindPitFX : MonoBehaviour
{
    private MovableWindPit parentScriptM;
    private Transform parent;
    private GameObject currentFX;
    private Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        parentScriptM = GetComponentInParent<MovableWindPit>();
        parent = GetComponentInParent<Transform>();
    }

    private void LateUpdate()
    {
        if (currentFX != null) currentFX.transform.position = parent.position + offset;
    }
    public void PlayFX(string name)
    {
        offset = new Vector3(0, 0.9f, 0);
        GameObject currentFX = PoolingManager.instance.GetObjectAutoReturn(name, transform.position + offset, 5f);
        parentScriptM.idleFxGO.SetActive(false);
        parentScriptM.idleFxGOH.SetActive(false);
        currentFX.transform.SetParent(parent);
    }

    public void ResetIdleFX()
    {
        parentScriptM.idleFxGO.SetActive(true);
        parentScriptM.idleFxGOH.SetActive(true);
    }
}
