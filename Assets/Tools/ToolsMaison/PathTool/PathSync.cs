﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class PathSync : MonoBehaviour
{
    public enum Speed { UltraFast, SuperFast, VeryFast, Fast, Medium, Slow, VerySlow, SuperSlow, UltraSlow }
    [SerializeField] Speed speed = (Speed)5;
    [SerializeField] [Range(1, 100)] float weight;
    [SerializeField] bool isSyncing;
    public PathObject leader;

    void Awake()
    {
        if (isSyncing)
        {
            float leaderDist = leader.GetTotalDistance();
            float totalDuration = (((float)speed + 1) * weight) + .1f;

            leader.SetLocalDuration(leaderDist, totalDuration);
            Transform tr = transform;
            foreach (Transform child in tr)
            {
                PathObject p = child.GetComponent<PathObject>();
                if (p)
                {
                    if (p != leader)
                    {
                        p.GetTotalDistance();
                        p.SetLocalDuration(leaderDist, totalDuration);
                    }

                }
            }
        }
    }
}

