﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;

public class PauseMenu : Panel
{
    //[valentin 2019-04-05] Jeu en pause ?
    public static bool gameIsPaused = false;
    //[valentin 2019-04-05] Canvas du menu pause.
    public GameObject pauseMenuUI;
    public MenuBouton pauseMenu;

   

          
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("pause"))
        {
            if (!gameIsPaused)
                Pause();
            else
                Resume();
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    public void Pause()
    {        
        Time.timeScale = 0f;
        gameIsPaused = true;        
    }

    public void LoadMainMenu(string nomMainMenu)
    {        
        SceneManager.LoadScene(nomMainMenu);
    }

}
