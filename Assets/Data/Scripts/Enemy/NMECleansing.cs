﻿using UnityEngine.Events;

public class NMECleansing : NMEState
{
    public UnityEvent OnCleasingComplete = new UnityEvent();
    public override void Update()
    {
        base.Update();
        if (master.currentCorruption <= 0)
        {
            OnCleasingComplete.Invoke();
            master.ChangeState(NMEStateMachine.State.Idle);
        }
        else if (timeInState + master.timeInLastState >= NMEStateMachine.TIME_HURT)
        {
            master.OnWakeUp.Invoke();
            master.ChangeState(NMEStateMachine.State.Idle);
        }
    }
}
