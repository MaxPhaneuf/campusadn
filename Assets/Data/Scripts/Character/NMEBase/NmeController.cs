﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[RequireComponent(typeof(NavMeshAgent),typeof(Rigidbody))]

public class NmeController : MonoBehaviour
{

    [HideInInspector]
    public NmeCerf cerf;
    [HideInInspector]
    public NmeOurs ours;
    /*[HideInInspector]
    public NmeCerf herisson;
    [HideInInspector]
    public NmeCerf chouette;*/

    public enum Type { Cerf, Ours, Herisson, Chouette }
    public Type type;

    #region Variable State Machine
    public enum NmeStates { IDLE, CHASE, SEARCH, DEFEND, PATROL, ALERT, HURT }
    private NmeStateMachine currentState;
    private NmeStateChase chaseState;
    private NmeStateIdle idleState;
    private NmeStateSearch searchState;
    private NmeStateDefend defendState;
    private NmeStatePatrol patrolState;
    private NmeStateAlert alertState;
    private NmeStateHurt hurtState;
    #endregion

    #region Variable Standard
    [Header("Drag the GameManager Here")]
    [Header("")]
    protected Transform tr;
    protected Transform target;
    protected NavMeshAgent agent;
    [Header("Ajust the size of a enemy detection range")]
    [Header("")]
    [Range(1.0f, 30f)]
    public float lookRadius = 10f;
    [Header("Ajust the speed of a enemy")]
    [Header("")]
    [Range(0.1f, 10f)]
    public float speed;
    [HideInInspector]
    public Animator anim;
    
    public PathFollow follow;

    //Animation

    protected int nmeHitAnim;
    protected int nmeMoveRangeAnim;
    protected int nmeDetecRangeAnim;
    protected int nmeMoveAnim;

    // Cone Detection

    protected float angleOfView = 90f;
    protected bool playerIsDetected = false;
    protected Vector3 targetCurrentPos;
    protected Vector3 targetLastPos;



    #endregion

    #region Variable WayPoints ( TEMPORAIRE )
    [Header("Set number of Waypoint, Drag Empty object to set points")]
    [Header("")]
    public Transform[] wayPoints;
    private int destPoint = 0;
    public enum PointColor { Red, Blue, Cyan, Green, Fuckchia, White, Yellow };
    [Header("Select a color of the path")]
    public PointColor ColorPoint;
    #endregion

    #region Start : Get,Target Avatar, StateMachine

    protected void Start()
    {
        
        nmeHitAnim = Animator.StringToHash("hit");
        nmeMoveAnim = Animator.StringToHash("move");
        nmeDetecRangeAnim = Animator.StringToHash("detecRange");
        nmeMoveRangeAnim = Animator.StringToHash("moveRange");

        if (type == Type.Cerf)
            cerf = gameObject.GetComponent<NmeCerf>();
        if (type == Type.Ours)
            ours = gameObject.GetComponent<NmeOurs>();
        /*if (type == Type.Herisson)
            herisson = gameObject.GetComponent<NmeCerf>();
        if (type == Type.Chouette)
            chouette = gameObject.GetComponent<NmeCerf>();*/

        tr = transform;
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        target = GameManager.instance.currentAvatar.transform;
        follow = GetComponent<PathFollow>();

        // State Machine
        chaseState = new NmeStateChase(this);
        idleState = new NmeStateIdle(this);
        searchState = new NmeStateSearch(this);
        defendState = new NmeStateDefend(this);
        patrolState = new NmeStatePatrol(this);
        alertState = new NmeStateAlert(this);
        hurtState = new NmeStateHurt(this);
        currentState = idleState;
        agent.speed = speed;

        
        
    }
    #endregion

    protected void Update()
    {

        
        currentState?.UpdateState();
    }


    #region Fonction FaceTarget ( Nme will face avatar )
    public void FaceTarget()
    {
        Vector3 direction = (target.position - tr.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        tr.rotation = Quaternion.Slerp(tr.rotation, lookRotation, Time.deltaTime * 5f);
    }
    #endregion

    #region Draw Gizmo Nme Radius Detection, Waypoint , Waypoint Path
    private void OnDrawGizmosSelected()
    {
        int i = 0;
        int j = 0;
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);

        switch (ColorPoint)
        {
            case PointColor.Red:
                Gizmos.color = Color.red;
                break;
            case PointColor.Blue:
                Gizmos.color = Color.blue;
                break;
            case PointColor.Cyan:
                Gizmos.color = Color.cyan;
                break;
            case PointColor.Green:
                Gizmos.color = Color.green;
                break;
            case PointColor.Fuckchia:
                Gizmos.color = Color.magenta;
                break;
            case PointColor.White:
                Gizmos.color = Color.white;
                break;
            case PointColor.Yellow:
                Gizmos.color = Color.yellow;
                break;
        }

        for (i = 0; i < wayPoints.Length - 1; i++)
        {
             j = i + 1;
            Gizmos.DrawSphere(wayPoints[i].position, 0.2f);
            Gizmos.DrawSphere(wayPoints[wayPoints.Length-1].position, 0.2f);
            Gizmos.DrawLine(wayPoints[i].position, wayPoints[j].position);
        }
        if (i < wayPoints.Length)
        {
            Gizmos.DrawLine(wayPoints[j].position, wayPoints[0].position);
        }
    }
    #endregion

    

    #region StateMachine
    public void ChangeState(NmeStates nextState)
    {
        switch (nextState)
        {
            case NmeStates.IDLE:
                currentState = idleState;
                break;
            case NmeStates.CHASE:
                currentState = chaseState;
                break;
            case NmeStates.SEARCH:
                currentState = searchState;
                break;
            case NmeStates.DEFEND:
                currentState = defendState;
                break;
            case NmeStates.PATROL:
                currentState = patrolState;
                break;
            case NmeStates.ALERT:
                currentState = alertState;
                break;
            case NmeStates.HURT:
                currentState = hurtState;
                break;
        }
    }
    #endregion

    private void LateUpdate()
    {
       
        if (follow != null && follow.target != null && agent != null)
        {
            /*agent.SetDestination(follow.target.position);
            tr.LookAt(follow.target);*/
        }
    }

    public void Chase()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance < lookRadius - 5)
        {
            agent.speed = 0;
            anim.SetBool("moveRange", false);
        }
        if (distance > lookRadius - 4)
        {
            agent.speed = speed;
            anim.SetBool("moveRange", true);
        }
        agent.speed = speed;
        agent.SetDestination(target.position);
        FaceTarget();
    }

    public void Patrol()
    {
        agent.SetDestination(follow.target.position);
        tr.LookAt(follow.target);

    }
}
