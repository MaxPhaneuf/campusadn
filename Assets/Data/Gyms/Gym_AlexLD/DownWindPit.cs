﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownWindPit: MonoBehaviour
{
    // variables pour fonctionement
    public float force = 1.5f;
    private Transform tr;
    Vector3 currentPos;
    Vector3 pitPosotion;

    //// variable pour le deplacer

    //Vector3 direction;
    //public float distance;
    //public float pushForce = 5f;
    //RaycastHit ground;
    //RaycastHit front;
    //RaycastHit back;
    //public bool grabbed = false;
    //public bool release = false;
    //public float cast = 0f;
    //Collider coli;
    //public float pos;

    //void Start()
    //{
    //    tr = transform;
    //    pitPosotion = tr.position;
    //    coli = GetComponent<Collider>();
    //}

    //void Update()
    //{
    //    Debug.DrawRay(tr.position + GameManager.instance.currentAvatar.transform.up, direction + Vector3.down, Color.yellow, 2);
    //    distance = Vector3.Distance(GameManager.instance.currentAvatar.transform.position, tr.position);
    //    direction = GameManager.instance.currentAvatar.transform.forward;

    //    Physics.Raycast(tr.position, direction, out front, 1f);
    //    Physics.Raycast(tr.position + GameManager.instance.currentAvatar.transform.up, direction + Vector3.down, out ground, cast); //  LA VALEUR DU CAST DOIT ETRE MODIFIER QUAND ON VA AVOIR LE VRAI MODEL , POUR LE MOMENT AVEC LE CUBE ON LA MET A 2

    //    if (GameManager.instance.currentAvatar.hitForward.collider == coli && distance <= 2f && GameManager.instance.currentAvatar.input.interaction && GameManager.instance.currentAvatar.input.leftStick.z == 0)
    //    {
    //        pos = GameManager.instance.currentAvatar.transform.eulerAngles.y;
    //        grabbed = true;
    //    }

    //    if (grabbed == true)
    //    {
            
    //        GameManager.instance.currentAvatar.transform.eulerAngles = new Vector3(0, pos, 0);
    //        GameManager.instance.currentAvatar.animator.SetBool("isGrabbing", true);

    //        if (GameManager.instance.currentAvatar.input.leftStick.z == 0)
    //        {
    //            GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
    //            GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
    //        }

    //        if (GameManager.instance.currentAvatar.input.leftStick.z >= 1) // pousser
    //        {
    //            GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
    //            GameManager.instance.currentAvatar.animator.SetBool("isPushing", true);

    //            if (ground.transform && !front.transform)
    //            {
    //                tr.position = GameManager.instance.currentAvatar.transform.position + GameManager.instance.currentAvatar.transform.forward * 1.5f;  // A MODIFIER LE FLOAT POUR LE OFFSET EN AVANT DE L AVATAR
    //            }
    //        }

    //        if (GameManager.instance.currentAvatar.input.leftStick.z <= -1) // tirer
    //        {

    //            GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
    //            GameManager.instance.currentAvatar.animator.SetBool("isPulling", true);
    //            tr.position = GameManager.instance.currentAvatar.transform.position + GameManager.instance.currentAvatar.transform.forward * 1.5f;
    //        }

    //        if (grabbed == false)
    //        {
    //            GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
    //            GameManager.instance.currentAvatar.animator.SetBool("isGrabbing", false);
    //            GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
    //        }

    //        if (GameManager.instance.currentAvatar.input.jumping)
    //        {
    //            grabbed = false;
    //            tr.position = tr.position;
    //            GameManager.instance.currentAvatar.animator.SetBool("isPushing", false);
    //            GameManager.instance.currentAvatar.animator.SetBool("isGrabbing", false);
    //            GameManager.instance.currentAvatar.animator.SetBool("isPulling", false);
    //        }
    //    }
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag != "Player")
            return;
        currentPos = GameManager.instance.transform.position;
        GameManager.instance.currentAvatar.velocityZ -= 2f;
        if (GameManager.instance.currentAvatar.velocityZ <= 0)
        {
            GameManager.instance.currentAvatar.velocityZ += 2f;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag != "Player")
            return;
        //GameManager.instance.currentAvatar.velocityY += force;
        GameManager.instance.currentAvatar.velocityY = currentPos.y * -force;
        if (GameManager.instance.currentAvatar.velocityZ <= 0)
        {
            GameManager.instance.currentAvatar.velocityZ += 2f;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        //GameManager.instance.currentAvatar.velocityY = 0;
    }

}
