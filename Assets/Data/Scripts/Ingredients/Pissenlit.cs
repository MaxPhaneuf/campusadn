﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pissenlit : MonoBehaviour
{
    Transform tr;
    Collider colision;
    Animator anim;
    private bool hasDashed = false;
    private float respawn = 1.8f;
    public GameObject pollen;
    private float baseSpeed = 5f;
    private float rotSpeedX = 60f;
    public bool allowGlide = false;
    [HideInInspector]
    public static Pissenlit instance;
    private float gravity = -100f;
    private bool haveActivate;
    AudioSource sound;
    GameObject explosionFX;
    //TODO integrer FX quand on dash dans pissenlit, FX_Prop_PissenlitExplosion_01 apres ajout

    void Start()
    {
        tr = transform;
        colision = GetComponent<Collider>();
        anim = GetComponentInChildren<Animator>();
        Character.instanceGlide = this;
        sound = GetComponentInChildren<AudioSource>();
        //explosionFX = PoolingManager.instance.GetObject("FX_Prop_PissenlitExplosion_01", tr.position);
        //explosionFX.SetActive(false);
    }


    void Update()
    {
        if (GameManager.instance.currentAvatar.hitForward.collider == this.colision && GameManager.instance.currentAvatar.isDashing && !hasDashed)
        {
            GameManager.instance.currentAvatar.onGround = false;
            GameManager.instance.currentAvatar.isGliding = true;
            GameManager.instance.currentAvatar.velocityY = GameManager.instance.currentAvatar.impulsionJump;
            allowGlide = true;
            anim.SetTrigger("triggerDashed");
            sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_TreeIn"));
            Invoke("respawnTimer", respawn);
            hasDashed = true;
        }

        if (InputManager.instance.bButton && GameManager.instance.currentAvatar.isGliding == true)
        {
            allowGlide = false;
            GameManager.instance.currentAvatar.isGliding = false;
            hasDashed = false;
            GameManager.instance.currentAvatar.velocityY = 0; // pour faire retomber l avatar sans ajouter une impulsion
        }
        if (InputManager.instance.jumping && GameManager.instance.currentAvatar.isGliding)
        {
            allowGlide = false;
            GameManager.instance.currentAvatar.isGliding = false;
            hasDashed = false;
        }
    }
    private void LateUpdate()
    {

        if (allowGlide)
        {
            GameManager.instance.currentAvatar.currentState.ToGlide();
            allowGlide = false;
        }

        if (GameManager.instance.currentAvatar.onGround && !hasDashed)
        {
            allowGlide = false;
            if (GameManager.instance.currentAvatar.isGliding)
            {
                GameManager.instance.currentAvatar.isGliding = false;
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (GameManager.instance.currentAvatar.isDashing && !hasDashed)
            {
                GameManager.instance.currentAvatar.animator.SetBool("isRunning", false);
                GameManager.instance.currentAvatar.onGround = false;
                GameManager.instance.currentAvatar.isGliding = true;
                GameManager.instance.currentAvatar.velocityY = GameManager.instance.currentAvatar.impulsionJump;
                allowGlide = true;
                anim.SetTrigger("triggerDashed");
               // PoolingManager.instance.GetObjectAutoReturn(name, transform.position, 5f);
                sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_TreeIn"));
                Invoke("respawnTimer", respawn);
                hasDashed = true;
            }
        }
    }
    void respawnTimer()
    {
        hasDashed = false;
    }
}
