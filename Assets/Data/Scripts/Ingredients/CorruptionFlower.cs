﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CorruptionFlower : MonoBehaviour
{
    [HideInInspector]
    public UnityEvent Uncorrupt;
    private Animator anim;
    float timePressed;
    [Range(1.5f, 3)]public float corruptionLvl = 2f;
    GameObject currentIdleFx;
    bool absorbFXActive;
    float absorbFXTime = 1.5f;
    bool isCorrupted = true;
    public Transform absorbFXOffset;
    public Corruption[] myCorruptedIngredients;
    private Corruption[] myCorrupted;

    // Start is called before the first frame update
    private void Awake()
    {
        
    }

    void Start()
    {
        myCorrupted = GetComponentsInChildren<Corruption>();
        anim = GetComponentInChildren<Animator>();
        currentIdleFx = PoolingManager.instance.GetObject(PoolConst.FX_Env_Flower_Corruption_01, transform.position);
        Uncorrupt.AddListener(EndCorruptFX);
        if (myCorrupted != null)
        {
            for (int i = 0; i < myCorrupted.Length; i++)
            {
                if (myCorrupted[i] != null)
                Uncorrupt.AddListener(myCorrupted[i].Uncorrupt);
            }
        }
        if (myCorruptedIngredients != null)
        {
            for (int i = 0; i < myCorruptedIngredients.Length; i++)
            {
                if (myCorruptedIngredients[i] != null)
                myCorruptedIngredients[i].Corrupt();
                Uncorrupt.AddListener(myCorruptedIngredients[i].Uncorrupt);
            }
        }
    }

    void EndCorruptFX()
    {
        if (PoolingManager.instance)
        {
            PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_Flower_Corruption_01, absorbFXOffset.position, absorbFXTime);
            PoolingManager.instance.ReturnObject(PoolConst.FX_Env_Flower_Corruption_01, currentIdleFx);
        }
    }

    private void Update()
    {

        if (timePressed >= corruptionLvl && isCorrupted)
        {
            anim.SetTrigger("triggerCleansedFinish");
            Uncorrupt.Invoke();
            Invoke("DeactivateObject", 2.33f);
            isCorrupted = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        anim.SetBool("isBeingCleansed", false);
        timePressed = 0;
    }

    private void DeactivateObject()
    {
        gameObject.SetActive(false);
    }

    IEnumerator AbsorbFX(float time)
    {
        absorbFXActive = true;
        if(isCorrupted)
            PoolingManager.instance.GetObjectAutoReturn(PoolConst.FX_Env_Flower_Corruption_Absorb_0, transform.position, time);
        yield return new WaitForSeconds(time);
        absorbFXActive = false;
    }
    public void RemoveCorruption()
    {
        if (InputManager.instance.aspiring)
        {
            anim.SetBool("isBeingCleansed", true);
            timePressed += Time.deltaTime;
            if (!absorbFXActive && PoolingManager.instance)
                StartCoroutine(AbsorbFX(absorbFXTime));
        }
        else
        {
            anim.SetBool("isBeingCleansed", false);
            timePressed = 0;
        }
    }
}