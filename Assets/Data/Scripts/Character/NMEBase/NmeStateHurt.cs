﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NmeStateHurt : NmeStateMachine
{
    public override void Idle()
    {
        myMaster.anim.SetBool("move", false);
        myMaster.ChangeState(NmeController.NmeStates.IDLE);
    }
    public override void Chase()
    {
        myMaster.anim.SetBool("detecRange", true);
        myMaster.anim.SetBool("moveRange", true);
        myMaster.ChangeState(NmeController.NmeStates.CHASE);
    }
    public override void Search()
    {
        myMaster.ChangeState(NmeController.NmeStates.SEARCH);
    }
    public override void Defend()
    {
        myMaster.ChangeState(NmeController.NmeStates.DEFEND);
    }
    public override void Patrol()
    {
        myMaster.anim.SetBool("detecRange", false);
        myMaster.anim.SetBool("moveRange", false);
        myMaster.anim.SetBool("move", true);
        myMaster.ChangeState(NmeController.NmeStates.PATROL);
    }
    public override void Alert()
    {
        throw new System.NotImplementedException();
    }
    public override void Hurt()
    {
        
    }


    public override void UpdateState()
    {

        Debug.Log("Hurt");

    }

    public NmeStateHurt(NmeController myMaster) : base(myMaster) { }
        
}
