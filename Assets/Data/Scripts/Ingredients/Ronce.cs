﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ronce : MonoBehaviour
{
    Transform tr;
    Collider coli;
    Collider col;
    AudioSource sound;

    // Start is called before the first frame update
    void Start()
    {
        sound = GetComponentInChildren<AudioSource>();
        col = GetComponent<Collider>();
        coli = GetComponentInChildren<Collider>();
        tr = transform;
    }
    private void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (!GameManager.instance.currentAvatar.isRolling && other.gameObject.CompareTag("Player"))
        {
            GameManager.instance.currentAvatar.GettingHurt();
            if (!sound.isPlaying)
            {
                sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_RonceIn"));
            }
        }
    }
}
