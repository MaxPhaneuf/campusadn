﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePanel : Panel
{
    public DataManager dataManager;
            
    public void SaveGame(int saveSpaceNb)
    {
        dataManager.SaveGame(saveSpaceNb, dataManager.TransferData());
    }
    

  
}
