﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PollenFX : MonoBehaviour
{
    private Pollen parentScript;
    private Transform parent;
    private GameObject currentFX;
    private Vector3 offset;

    private void Start()
    {
        parentScript = GetComponentInParent<Pollen>();
        parent = GetComponentInParent<Transform>();
    }

    private void LateUpdate()
    {
        if (currentFX != null) currentFX.transform.position = parent.position + offset;
    }
    public void PlayFX(string name)
    {
        offset = new Vector3(0, 0.9f, 0);
        GameObject currentFX = PoolingManager.instance.GetObjectAutoReturn(name, transform.position + offset, 1.5f);
        parentScript.startFxGO.SetActive(false);
        parentScript.idleFxGO.SetActive(false);
        parentScript.endFxGO.SetActive(false);
        currentFX.transform.SetParent(parent);
    }

    public void ResetIdleFX()
    {
        parentScript.startFxGO.SetActive(true);
        parentScript.idleFxGO.SetActive(true);
        parentScript.endFxGO.SetActive(true);
    }
}
