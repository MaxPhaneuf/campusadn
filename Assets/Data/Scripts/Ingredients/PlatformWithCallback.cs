﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformWithCallback : PathFollow
{
    private void Start()
    {
        lerpable = GetComponent<Lerpable>();
        lerpable.active = true;
        lerpable.isAutoStop = false;
        if (target)
            SetTarget();
        else if (transform.GetComponent<PathObject>() == null)
            target = transform;
        if (path != null)
        {
            if (startPoint > path.wayPoints.Count)
                startPoint = path.wayPoints.Count;
            if (path.wayPoints[startPoint - 1])
                Init();
        }
        active = startActive;
        Activate.AddListener(() => SetActive(true));
        Deactivate.AddListener(() => SetActive(false));
    }

    void Update()
    {
        UpdateLerp();
    }

    protected override void UpdateLerp()
    {
        lerpable.forward = active;
        lerpable.Lerp();
        UpdateTarget();
        if (path != null && lerpable != null && target)
            Lerp();
    }

    public override void NextWayPoint()
    {
        InitDefaultLerp();
    }

    protected override void LerpDefault()
    {
        if (target)
            UpdateTargetPosition(lerpable.Lerp(start, end));
    }
}
