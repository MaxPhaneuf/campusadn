﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMEHerSL : NMEStateListener
{
    readonly int isRollingAtTarget = Animator.StringToHash("isRollingAtTarget");
    readonly int isRolling = Animator.StringToHash("isRolling");
    readonly int triggerAttack = Animator.StringToHash("triggerAttack");
    readonly int triggerBallHit = Animator.StringToHash("triggerBallHit");

    Animator AnimBaseModel, AnimBallModel;
    NMEHerSM sm;

    public override void Init(NMEStateMachine machine)
    {
        this.machine = machine;
        sm = (NMEHerSM)machine;
        AnimBaseModel = GetComponent<Animator>();
        AnimBallModel = sm.ballModel.GetComponent<Animator>();
        InitStates();
    }

    protected override void InitStates()
    {
        InitIdle(AnimBaseModel);
        InitHurt(AnimBaseModel);
        InitPatrol(AnimBaseModel);
        InitPursuit(AnimBaseModel);
         InitRollBall(AnimBallModel);
        InitCannonBall();
    }

    protected override void InitPursuit(Animator anim)
    {
        NMEHerRoll pursuit = (NMEHerRoll)machine.GetState(NMEStateMachine.State.Pursuit);
        pursuit.OnStart.AddListener(() => anim.SetBool(isRollingAtTarget, true));
        pursuit.OnEnd.AddListener(() => anim.SetBool(isRollingAtTarget, false));
        pursuit.IsInRange.AddListener((isInRange) => anim.SetBool(isTargetInRange, isInRange));
    }

    void InitRollBall(Animator anim)
    {
        NMEHerRoll roll = (NMEHerRoll)machine.GetState(NMEStateMachine.State.Pursuit);
        roll.OnStart.AddListener(() => anim.SetBool(isRolling, true));
        roll.OnEnd.AddListener(() => anim.SetBool(isRolling, false));
    }

    void InitCannonBall()
    {
        NMEHerCannon roll = (NMEHerCannon)machine.GetState(NMEStateMachine.State.Attack1);
        roll.OnStart.AddListener(() => AnimBaseModel.SetTrigger(triggerAttack));
        sm.OnBallHit.AddListener(() => AnimBaseModel.SetTrigger(triggerBallHit));
        sm.OnBallHit.AddListener(() => AnimBallModel.SetTrigger(triggerBallHit));
    }
}
