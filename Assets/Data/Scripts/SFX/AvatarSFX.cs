﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AvatarSFX : MonoBehaviour
{
    AudioSource sound;


    void Start()
    {
        sound = GetComponentInChildren<AudioSource>();
    }


    void Update()
    {
        
    }

    public void PlaySFXdash()
    {
        int dashId = UnityEngine.Random.Range(0, 3);
        if (dashId == 0) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_AvatarDash1"));
        if (dashId == 1) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_AvatarDash2"));
        if (dashId == 2) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_AvatarDash3"));
    }

    public void PlaySFXjump()
    {
        int jumpId = UnityEngine.Random.Range(0, 3);
        if (jumpId == 0) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_AvatarJump"));
        if (jumpId == 1) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_AvatarJump2"));
        if (jumpId == 2) sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_AvatarJump3"));
    }


    public void PlaySFXrolling()
    {
        sound.PlayOneShot(GameManager.instance.SoundManager.GetClip("SFX_AvatarRolling"));
    }

    public void PlaySFX(string Name)
    {
        sound.PlayOneShot(GameManager.instance.SoundManager.GetClip(Name));
    }


    public void StopSFX()
    {
        sound.Stop();
    }
}
