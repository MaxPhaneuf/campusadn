﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NMECerfSL : NMEStateListener
{
    readonly int triggerAttack = Animator.StringToHash("triggerAttack");
    readonly int triggerDashNoHit = Animator.StringToHash("triggerDashNoHit");
    readonly int triggerDashHit = Animator.StringToHash("triggerDashHit");

    protected override void InitStates()
    {
        base.InitStates();
        InitDash();
    }

    void InitDash()
    {
        NMECerfDash dash = (NMECerfDash)machine.GetState(NMEStateMachine.State.Attack1);
        dash.OnStart.AddListener(() => Anim.SetTrigger(triggerAttack));
        
        NMECerfSM sm = (NMECerfSM)machine;
        sm.OnDashNoHit.AddListener(() => Anim.SetTrigger(triggerDashNoHit));
        sm.OnDashHit.AddListener(() => Anim.SetTrigger(triggerDashHit));
        dash.IsInRange.AddListener((isInRange) => Anim.SetBool(isTargetInRange, isInRange));

    }
       
}
