﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindpitFX : MonoBehaviour
{
    private WindPit parentScript;
    private Transform parent;
    private GameObject currentFX;
    private Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        parentScript = GetComponentInParent<WindPit>();
        parent = GetComponentInParent<Transform>();
    }

    private void LateUpdate()
    {
        if (currentFX != null) currentFX.transform.position = parent.position + offset;
    }
    public void PlayFX(string name)
    {
        offset = new Vector3(0, 0.9f, 0);
        GameObject currentFX = PoolingManager.instance.GetObjectAutoReturn(name, transform.position + offset, 5f);
        parentScript.idleFxGO.SetActive(false);
        parentScript.idleFxGOH.SetActive(false);
        currentFX.transform.SetParent(parent);
    }

    public void ResetIdleFX()
    {
        parentScript.idleFxGO.SetActive(true);
        parentScript.idleFxGOH.SetActive(true);
    }
}
