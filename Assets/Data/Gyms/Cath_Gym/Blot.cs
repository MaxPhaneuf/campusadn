﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blot : MonoBehaviour
{
    private Vector3 rayOffSet = new Vector3(0,0.01f,0);
    Transform tr;
    [HideInInspector]
    public Vector2 startSize, endSize;
    public float maxDistance = 2;

    float currentDistance;
    float distanceRatio;
    Vector3 avatarPosition;

    public bool active = true;

    // Start is called before the first frame update
    void Start()
    {
        tr = transform;
    }

    private void Update()
    {
        if (active)
        {
            avatarPosition = GameManager.instance.currentAvatar.tr.position;
            currentDistance = Vector3.Distance(avatarPosition, tr.position);

            if (currentDistance >= maxDistance)
                distanceRatio = 1;
            else
                distanceRatio = currentDistance / maxDistance;
            tr.localScale = Vector2.Lerp(startSize, endSize, distanceRatio);
        }
    }
    // Update is called once per frame
    void LateUpdate()
    {
        //tr.position = GameManager.instance.currentAvatar.animator.rootPosition;
        tr.position = GameManager.instance.currentAvatar.positionBlot.point + rayOffSet;
    }
}
