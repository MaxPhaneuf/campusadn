﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NmeCerf : NmeController
{
    private bool dashOk = true;
    private float rofDash = 5.0f;
    private float dashTime = 2.0f;
    private Vector3 targetPos;
    private RaycastHit hitOk;
    


    public void NmeCerfBehavior()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance < lookRadius - 1)
        {
            
            if (dashOk == true)
            {
                Physics.Raycast(tr.position, tr.forward, out hitOk);
                if(hitOk.transform == target.transform)
                {
                    anim.SetTrigger("attackRange1");
                    anim.SetBool("moveRange", false);
                    dashOk = false;
                    Invoke("DashTime", dashTime);
                    Invoke("RofDashReset", rofDash);
                }
            }
        }
    }
    private void OnTriggerEnter(Collider target)
    {
        Debug.Log("hit");
        anim.SetInteger("hitAvatar", -1);
    }
    private void RofDashReset()
    {
        dashOk = true;
        anim.SetInteger("hitAvatar", 0);
    }
    private void DashTime()
    {
        Debug.Log("miss");
        anim.SetInteger("hitAvatar", 1);
    }
}
