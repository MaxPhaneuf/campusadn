﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BendyGrass : MonoBehaviour
{
    public Material mat;
    public Renderer rd;
    public string Pos = "Player_Position";
    public Transform player;

    private void Update()
    {
        if (GameManager.instance != null)
        {
            player = GameManager.instance.currentAvatar.GetComponent<Transform>();
        }
        else Debug.Log("No Manager");
}
    private void LateUpdate()
    {
        if (player != null)
        {
            
            if (mat == null)
            {
                rd = GetComponent<Renderer>();
                mat = rd.material;
            }
            else
            {
                mat.SetVector(Pos, player.position);
            }
        }
        else
        {
            Debug.Log("No Player");
        }
    }
}
