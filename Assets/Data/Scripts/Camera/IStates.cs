﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStates
{
    void Enter();

    void Execute();

    void Exit();
}
