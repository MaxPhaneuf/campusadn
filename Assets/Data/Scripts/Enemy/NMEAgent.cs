﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class NMEAgent : MonoBehaviour
{
    public float speed = 3;
    public NMEFollow path;
    NavMeshAgent nav;
    
    public void Init()
    {
        nav = gameObject.AddComponent<NavMeshAgent>();
        nav.speed = speed;
    }

    public void SetDestination(Vector3 destination)
    {
        nav.SetDestination(destination);
    }
}
