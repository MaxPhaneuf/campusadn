﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Rewired;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public GameObject Dialogue, Pause, Cheats, Save, Load, Settings;
    Panel dialoguePanel, pausePanel, cheatsPanel, savePanel, loadPanel, settingsPanel;
    public bool isUIActive, isGamePaused;
    public GameObject current, last;
    private Panel lastPanel, currentPanel;
    float currentFrame;

    // Rewired 
    private Player player;
    private int playerId = 0;

    public void Start()
    {
        SetBoolPaused();
        current = Pause;
        last = Pause;
    }

    #region toggle panel fonctions
    public void ToggleDialogue()
    {
        AssignCurrent(Dialogue);
        Dialogue.SetActive(!Dialogue.activeInHierarchy);
    }

    public void TogglePause()
    {
        AssignCurrent(Pause);
        Pause.SetActive(!Pause.activeInHierarchy);
       
    }

    public void ToggleCheats()
    {
        AssignCurrent(Cheats);
        Cheats.SetActive(!Cheats.activeInHierarchy);
        TogglePause();
    }

    public void ToggleSave()
    {
        AssignCurrent(Save);
        Save.SetActive(!Save.activeInHierarchy);
        TogglePause();
    }

    public void ToggleLoad()
    {
        AssignCurrent(Load);
        Load.SetActive(!Load.activeInHierarchy);
        TogglePause();
    }

    public void ToggleSettings()
    {
        AssignCurrent(Settings);
        Settings.SetActive(!Settings.activeInHierarchy);
        TogglePause();
    }

    public void TogglePanel(GameObject panelToToggle)
    {
        AssignCurrent(panelToToggle);
        panelToToggle.SetActive(!panelToToggle.activeInHierarchy);
    }
    #endregion

    public void AssignCurrent(GameObject o)
    {
        if (last != current)
        {
            last = current;
            lastPanel = last.GetComponent<Panel>();
            lastPanel.selected = false;
            last.SetActive(false);
        }
        current = o;
    }

    public void BackToLast()
    {

       if (last != null)
            last.SetActive(true);
        Time.timeScale = (current == Pause ? 1 : 0);
        currentPanel = current.GetComponent<Panel>();
        currentPanel.selected = false;
        current.SetActive(false);
        current = last;
       /* if (last != null && last != current)
        {
            TogglePanel(last);
            
            currentPanel = current.GetComponent<Panel>();
            currentPanel.UnSelectPanel();
            TogglePanel(current);

            
        }*/
    }

    #region Pause  


    public void Paused()
    {
        //GameManager.instance.currentAvatar.DisableInput(true);   trouver le bon positionnement
        isGamePaused = true;
        isUIActive = true;
        TogglePause();               
        Time.timeScale = 0f;
        

    }

    public void Resume()
    {
        Pause.SetActive(false);
        Dialogue.SetActive(false);
        Cheats.SetActive(false);
        Save.SetActive(false);
        Load.SetActive(false);
        Settings.SetActive(false);
        isGamePaused = false;
        isUIActive = false;
        Time.timeScale = 1f;
        //GameManager.instance.currentAvatar.DisableInput(false);   strouver le bon positionnement


    }       
    
    public void SetBoolPaused()
    {
        if (!Pause.activeInHierarchy && !Dialogue.activeInHierarchy)
        {
            isGamePaused = false;
            isUIActive = false;
        }
        else
        {
            isGamePaused = true;
            isUIActive = true;
        }
    }
    #endregion

    public void LoadMainMenu()
    {
        GameManager.instance.LoadLevel(0);        
    }

    public void QuitGame()
    {
        Application.Quit();
    }   
}
