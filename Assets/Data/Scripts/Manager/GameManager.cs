﻿// Créé par: Jonathan G. Mann
// En date du: 03-04-19
// Modifié par: David Babin
// En date du: 2019-04-10


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public UnityEvent DeactivateRD;
    public const string SPAWN_POINT_NAME = "SpawnPoint";
    public const string LEVEL_MANAGER = "LevelManager";
    public const string PLAYER_CAMERA = "PlayerCamera";
    public const string SOUND_MANAGER = "SoundManager";
    public const string PLAYER_TAG = "Player";

    #region VARIABLES SCRIPTS
    public static GameManager instance;
    public LevelManager levelManager;
    public CameraManager cameraManager;
    public DataManager saveData;
    public PoolManagerSound SoundManager;
    public EventManager eventManager;
    public UIManager uiManager;
    public Skills skillsManager;
    public Image fadePanel;
    public bool busy;
    public GameObject blotObject;
    [HideInInspector]
    public GameObject blot;

    public Character modeleAvatar;
    public Character currentAvatar;
    //public Skills skills;
    public static Player player;
    public int playerId;
    [HideInInspector]
    public bool doorUnlocker = false;
    public bool SpawnAtStart = true;
    public List<Level> niveaux = new List<Level>();
    #endregion

    private void Awake()
    {
        SingletonSetup();
        player = ReInput.players.GetPlayer(playerId);
    }

    void Start()
    {
        StartCoroutine(FadeEffect(new Color(fadePanel.color.r, fadePanel.color.g, fadePanel.color.b, 0), .5f, fadePanel));
        SpawnAvatar();
        SetGameManagerLinks();
        if (SpawnAtStart)
        {
            currentAvatar.gameObject.SetActive(true);
            RepositionAvatar(GetSpawnPoint());
            //blot = Instantiate(blotObject);
        }
    }

    private void Update()
    {
        if (currentAvatar.hasRespawn)
        {
            currentAvatar.gameObject.SetActive(false);
            currentAvatar.tr.position = currentAvatar.startingPosition;
            currentAvatar.gameObject.SetActive(true);
        }
        /*if (!currentAvatar.onGround && currentAvatar != null)
            blot.SetActive(true);
        else if (currentAvatar != null)
            blot.SetActive(false);*/
    }

    #region Load asynchrone des scenes LD et LA
    public IEnumerator SceneLoaded(bool isAvatarSpawningAtStart, Scene sceneLA, Scene sceneLD)
    {
        Image i = fadePanel;
        StartCoroutine(FadeEffect(new Color(i.color.r, i.color.g, i.color.b, 1), .5f, i));
        AsyncOperation asyncScene = SceneManager.LoadSceneAsync(sceneLA.handle, LoadSceneMode.Single);
        asyncScene = SceneManager.LoadSceneAsync(sceneLD.handle, LoadSceneMode.Additive);
        while (!asyncScene.isDone)
            yield return new WaitForEndOfFrame();
        Scene activeScene = SceneManager.GetActiveScene();
        LoadAvatar(isAvatarSpawningAtStart, activeScene);
        SetGameManagerLinks();
        PlayerCamera.instance.camTarget.Init();
        CheatMenu.instance.levelToLoad = levelManager.currentLevel;
        StartCoroutine(FadeEffect(new Color(i.color.r, i.color.g, i.color.b, 0), .5f, i));

    }
    #endregion

    #region Load l'avatar si il doit spawn at start dans le niveau
    public void LoadAvatar(bool startActive, Scene scene)
    {
        if (/*!currentAvatar.isActiveAndEnabled &&*/ startActive)
        {
            Debug.Log("load avatar");
            currentAvatar.gameObject.SetActive(true);
            RepositionAvatar(GetSpawnPoint());
        }
    }
    #endregion

    #region Récupere le spawn point de la scene
    public Vector3 GetSpawnPoint()
    {
        GameObject o = GameObject.FindGameObjectWithTag(SPAWN_POINT_NAME);
        if (o != null)
            return o.transform.position;
        else
            return new Vector3(0, 0, 0);
    }
    #endregion

    #region Spawn l'avatar en SetActive false
    public void SpawnAvatar()
    {
        currentAvatar = Instantiate(modeleAvatar, modeleAvatar.transform.position, modeleAvatar.transform.rotation);
        DontDestroyOnLoad(currentAvatar);
        currentAvatar.gameObject.SetActive(false);
    }
    #endregion

    #region Clear l'avatar
    void ClearAvatar(Scene scene)
    {
        currentAvatar = null;
    }
    #endregion

    #region Repositionne l'avatar à la position choisi
    public void RepositionAvatar(Vector3 position)
    {
        currentAvatar.transform.position = position;
    }
    #endregion

    #region Class Level
    [System.Serializable]
    public class Level
    {
        public Scene sceneLD;
        public Scene sceneLA;
        public bool isAvatarSpawningAtStart = true;
    }
    #endregion

    #region Fonction LoadLevel
    public void LoadLevel(int levelToLoad)
    {
        if (niveaux.Count > 0)
        {
            Image i = fadePanel;
            bool start = niveaux[levelToLoad].isAvatarSpawningAtStart;
            Scene sceneLA = niveaux[levelToLoad].sceneLA;
            Scene sceneLD = niveaux[levelToLoad].sceneLD;
            StartCoroutine(SceneLoaded(start, sceneLA, sceneLD));
        }
    }
    #endregion

    #region Fade Effect
    public IEnumerator FadeEffect(Color target, float duration, Image image)
    {
        busy = true;
        Color startColor = image.color;
        float startTime = Time.time;
        float t = 0;
        while (t < 1)
        {
            t = (Time.time - startTime) / duration;
            image.color = Color.Lerp(startColor, target, t);
            yield return new WaitForEndOfFrame();
        }
        busy = false;
    }
    #endregion

    #region Singleton
    private void SingletonSetup()
    {
        if (instance == null) instance = this;
        else Destroy(this.gameObject);
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    private void SetGameManagerLinks()
    {
        GameObject o = GameObject.Find(LEVEL_MANAGER);
        if (o != null)
            levelManager = o.GetComponent<LevelManager>();
        else
            Debug.LogError("LevelManager object missing");
        
        o = GameObject.Find(SOUND_MANAGER);
        if (o != null)
            SoundManager = o.GetComponent<PoolManagerSound>();

    }

}