﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NmeStateAlert : NmeStateMachine
{

    public override void Idle()
    {

        myMaster.ChangeState(NmeController.NmeStates.IDLE);
    }
    public override void Chase()
    {

        
        myMaster.ChangeState(NmeController.NmeStates.CHASE);
    }
    public override void Search()
    {
        myMaster.ChangeState(NmeController.NmeStates.SEARCH);
    }
    public override void Defend()
    {
        myMaster.ChangeState(NmeController.NmeStates.DEFEND);
    }
    public override void Patrol()
    {
        myMaster.anim.SetBool("detecRange", false);
        myMaster.anim.SetBool("moveRange", false);
        myMaster.ChangeState(NmeController.NmeStates.PATROL);

    }
    public override void Alert()
    {

    }
    public override void Hurt()
    {
        throw new System.NotImplementedException();
    }


    public override void UpdateState()
    {
        Debug.Log("Alert");
        float distance = Vector3.Distance(GameManager.instance.currentAvatar.tr.position, myMaster.transform.position);
        
        if (distance < myMaster.lookRadius)
        {
            Chase();
        }
        /*if (distance > myMaster.lookRadius)
        {
            Patrol();
        }*/

        



    }

    public NmeStateAlert(NmeController myMaster) : base(myMaster) { }
}
