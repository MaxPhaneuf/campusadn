﻿// Création du script : Maxime Arcierie
// Date : 03-04-2019 11:00 
// Modification : Maxime
// Date de modification : 03-05-2019 11:00 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader : Triggerable
{
    #region Variable And Enum
    public enum LevelName { Main_Menu, Le_calme_avant_la_tempete, De_retour_a_la_maison, Nouveaux_horizons, La_caverne_desolee, Reve_premonitoire, Jusqu_aux_nuages, Revelations, Un_nouvel_espoir, L_union_fait_la_force, Chaos, };
    [Header("Select a level to load on trigger")]
    public LevelName Level;
    [HideInInspector]
    public int levelSelector;
    #endregion

    #region Switch Level Name
    private void Update()
    {
        switch (Level)
        {
            case LevelName.Main_Menu:
                levelSelector = 0;
                break;
            case LevelName.Le_calme_avant_la_tempete:
                levelSelector = 1;
                break;
            case LevelName.De_retour_a_la_maison:
                levelSelector = 2;
                break;
            case LevelName.Nouveaux_horizons:
                levelSelector = 3;
                break;
            case LevelName.La_caverne_desolee:
                levelSelector = 4;
                break;
            case LevelName.Reve_premonitoire:
                levelSelector = 5;
                break;
            case LevelName.Jusqu_aux_nuages:
                levelSelector = 6;
                break;
            case LevelName.Revelations:
                levelSelector = 7;
                break;
            case LevelName.Un_nouvel_espoir:
                levelSelector = 8;
                break;
            case LevelName.L_union_fait_la_force:
                levelSelector = 9;
                break;
            case LevelName.Chaos:
                levelSelector = 10;
                break;
        }
    }
    #endregion

    #region Fonction Inutilisable du trigger
    public override void Activate()
    {
        GameManager.instance.LoadLevel(levelSelector);
    }

    public override void Deactivate()
    { 
    }

    public override void Toggle()
    { 
    }
    #endregion
}
