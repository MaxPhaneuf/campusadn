﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NmeStateDefend : NmeStateMachine
{

    public override void Idle()
    {
        myMaster.anim.SetBool("move", false);
        myMaster.ChangeState(NmeController.NmeStates.IDLE);
    }
    public override void Chase()
    {
        myMaster.anim.SetBool("detecRange", true);
        myMaster.anim.SetBool("moveRange", true);
        myMaster.ChangeState(NmeController.NmeStates.CHASE);
    }
    public override void Search()
    {
        myMaster.ChangeState(NmeController.NmeStates.SEARCH);
    }
    public override void Defend()
    {
        
    }
    public override void Patrol()
    {
        myMaster.anim.SetBool("move", true);
        myMaster.ChangeState(NmeController.NmeStates.PATROL);
    }
    public override void Alert()
    {
        throw new System.NotImplementedException();
    }
    public override void Hurt()
    {
        myMaster.ChangeState(NmeController.NmeStates.HURT);
        myMaster.anim.SetBool("hit", true);
    }



    public override void UpdateState()
    {
        throw new System.NotImplementedException();
    }

    public NmeStateDefend(NmeController myMaster) : base(myMaster) { }
}
