﻿//Creer par Valentin et Maxime P.
//Date de création [2019-04-08]

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;
public class CameraTriggerManager : MonoBehaviour
{
    //[2019-04-04]Valentin et Maxime P: Type of camera to set in the inspector
    public enum ActiveCam { Player, Cutscene } 
    public ActiveCam currentCam;
    bool cutActive;
    //[2019-04-04]Valentin et Maxime P: List of virtual camera to enter in the inspector
    public List<VirtualCamera> cameras = new List<VirtualCamera>(); 

    ActiveCam c;
    UnityCamEvent OnSwitchCam = new UnityCamEvent();

    void Awake()
    {       
        RegisterCameras();
        c = currentCam;
    }

    public void Update()
    {
        if (currentCam != c && currentCam != ActiveCam.Cutscene)
            ChangeCurrent(currentCam);   
    }

    /// <summary>
    /// Change priority of all camera putting the selected camera at highest priority in cinemachine component.
    /// </summary>
    public void ChangeCurrent(ActiveCam changeTo)
    {
        OnSwitchCam.Invoke(changeTo);
        currentCam = changeTo;
        c = currentCam;
    }

    void RegisterCameras()
    {
        foreach (VirtualCamera c in cameras)
            OnSwitchCam.AddListener((camType) => c.ChangeCamPriority(camType));
    }
    
    public class UnityCamEvent : UnityEvent<ActiveCam> { }   
}
